CREATE DATABASE IF NOT EXISTS dmsswe_db;
USE dmsswe_db;

CREATE TABLE IF NOT EXISTS tbl_positions (
	pos_id			INT(8)			UNSIGNED	NOT NULL    AUTO_INCREMENT,
	pos_name		VARCHAR(30)		NOT NULL,
	pos_require		TEXT,
	pos_active		ENUM('0', '1') 	NOT NULL 	DEFAULT 	'1',
	pos_date		DATETIME		NOT NULL 	DEFAULT		CURRENT_TIMESTAMP,
	
	PRIMARY KEY (pos_id)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS tbl_applications (
	app_id			INT(8)			UNSIGNED	NOT NULL    AUTO_INCREMENT,
	pos_id			INT(8)			UNSIGNED	NOT NULL,
    first_name      VARCHAR(30)     NOT NULL,
	last_name		VARCHAR(40)     NOT NULL,
	mobile_number	VARCHAR(10)		NOT NULL,
	email			VARCHAR(60)		NOT NULL,
	file_name		VARCHAR(20)		NOT NULL,
	date_created	DATETIME 		NOT NULL 	DEFAULT CURRENT_TIMESTAMP,
	reviewed		ENUM('0','1', '2', '3')		NOT NULL	DEFAULT '0',/* 0 - Not Revied, 1 - Reviewed, 2 - Email Sent, 3- Recruited */
    linked_in      	VARCHAR(35),
	
	PRIMARY KEY (app_id),
	FOREIGN KEY (pos_id)	REFERENCES tbl_positions (pos_id)
)ENGINE=InnoDB;

/*
	Permissions
		Resumes
			- Add 			RA
			- Update 		RU
			- Remove		RD
			- Review		RR
			- Send Mail		RSM
			- View			RV
		Users
			- Add			UA
			- Update		UU
			- Remove		UD
			- View			UV
		User Activity
			- View 			UAV
		Positions
			- Add			PA
			- Close			PC
			- Open			PO
		Permissions			
			- Add			PMA
			- View			PMV
			- Update		PMU
		Interviews
			- Schedule		IS
			- Change State	ICS
			- View			IV
		Roles
			- Add			ROA
			- View			ROV
			- Update		ROU
			- Remove		ROR
*/

CREATE TABLE IF NOT EXISTS tbl_permissions (
	permission_id		INT(3)			UNSIGNED	NOT NULL,
	module_name			VARCHAR(30)		NOT NULL,
	perm_short_name		VARCHAR(5)		NOT NULL,
	perm_name			VARCHAR(40)		NOT NULL,
	
	PRIMARY KEY (permission_id)
)ENGINE=InnoDB;
INSERT INTO tbl_permissions	VALUES 
(1, 'Applicants', 'RA', 'Add Application'),
(2, 'Applicants', 'RU', 'Update Application'),
(3, 'Applicants', 'RD', 'Remove Application'),
(4, 'Applicants', 'RR', 'Review Application'),
(5, 'Applicants', 'RSM', 'Schedule Interview'),
(6, 'Applicants', 'RV', 'View Application'),

(7, 'Users', 'UA', 'Add User'),
(8, 'Users', 'UU', 'Update User'),
(9, 'Users', 'UD', 'Remove User'),
(10, 'Users', 'UV', 'View User'),

(11, 'Users','UAV', 'View User Activity'),

(12, 'Positions','PA', 'Add Position'),
(13, 'Positions','PC', 'Close Position'),
(14, 'Positions','PO', 'Open Position'),

(15, 'Permissions','PMA', 'Add Permission'),
(16, 'Permissions','PMV', 'View Permissions'),
(17, 'Permissions','PMU', 'Update Permissions'),

(18, 'Interview','IS', 'Schedule Interview'),
(19, 'Interview','ICS', 'Change Interview State'),
(20, 'Interview','IV', 'View Interview'),

(21, 'Roles','ROA', 'Add Role'),
(22, 'Roles','ROV', 'View Role'),
(23, 'Roles','ROU', 'Update Role'),
(24, 'Roles','ROR', 'Remove Role');

CREATE TABLE IF NOT EXISTS tbl_roles (
	role_id				INT(2)			UNSIGNED	NOT NULL    AUTO_INCREMENT,
	role_name			VARCHAR(40)		NOT NULL,
	role_permission		VARCHAR(200)	NOT NULL,
	
	PRIMARY KEY (role_id)
)ENGINE=InnoDB;
INSERT INTO tbl_roles VALUES
(NULL, 'Administrator', '[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]'),
(NULL, 'Standard User',	'[5,6,10]');

CREATE TABLE IF NOT EXISTS tbl_user_data (
	user_id			INT(8)			UNSIGNED	NOT NULL    AUTO_INCREMENT,
	role_id			INT(2)			UNSIGNED	NOT NULL,
	first_name		VARCHAR(30)		NOT NULL,
	last_name		VARCHAR(50)		NOT NULL,
	time_created	DATETIME		NOT NULL DEFAULT CURRENT_TIMESTAMP,
	
	PRIMARY KEY (user_id),
	FOREIGN KEY (role_id) REFERENCES tbl_roles (role_id)
)ENGINE=InnoDB;
INSERT INTO tbl_user_data VALUES
(NULL, 1, 'System', 'Administrator', CURRENT_TIMESTAMP);

CREATE TABLE IF NOT EXISTS tbl_user_login (
	email_address		VARCHAR(200)		NOT NULL,
	user_id				INT(8)				UNSIGNED	NOT NULL,
	password			VARCHAR(60)			NOT NULL,
	first_time			ENUM('0', '1')		NOT NULL	DEFAULT '1',
	
	UNIQUE KEY (email_address),
	PRIMARY KEY (email_address, user_id),
	FOREIGN KEY (user_id)	REFERENCES tbl_user_data (user_id)	
)ENGINE=InnoDB;
INSERT INTO tbl_user_login VALUES
('basura@dmsswe.com', 1, MD5('mywins13'), '1');

CREATE TABLE IF NOT EXISTS tbl_user_activity (
	activity_id			INT(8)			UNSIGNED	NOT NULL    AUTO_INCREMENT,
	user_id				INT(8)			UNSIGNED	NOT NULL,
	activity			VARCHAR(200)	NOT NULL,
	ip_address      	VARCHAR(45)     NOT NULL,
	time_created		DATETIME		NOT NULL DEFAULT CURRENT_TIMESTAMP,
	
	PRIMARY KEY (activity_id),
	FOREIGN KEY (user_id)	REFERENCES tbl_user_data (user_id)	
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS tbl_position_owner (
	pos_id			INT(8)			UNSIGNED	NOT NULL,
	user_id			INT(8)			UNSIGNED	NOT NULL,
	changed			DATETIME		NOT NULL	DEFAULT CURRENT_TIMESTAMP,
	
	PRIMARY KEY (pos_id, user_id),
	FOREIGN KEY (pos_id)	REFERENCES tbl_positions (pos_id),
	FOREIGN KEY (user_id)	REFERENCES tbl_user_data (user_id)	
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS tbl_interview (
	interview_id		INT(8)					UNSIGNED	NOT NULL    AUTO_INCREMENT,
	app_id				INT(8)					UNSIGNED	NOT NULL,
	user_id				INT(8)					UNSIGNED	NOT NULL,
	interview_dt		DATETIME				NOT NULL	DEFAULT		CURRENT_TIMESTAMP,
	pos_manager			INT(8)					UNSIGNED	NOT NULL,
	person_con			INT(10)					NOT NULL,
	interview_remarks	TEXT					NOT NULL,
	interview_status	ENUM('N','S','C','O')	NOT NULL	DEFAULT 'S',/* N - Cancelled, S - Scheduled, C - Completed, O - Ongoining */
	
	PRIMARY KEY (interview_id),
	FOREIGN KEY (app_id)	REFERENCES tbl_applications (app_id),
	FOREIGN KEY (user_id)	REFERENCES tbl_user_data (user_id),
	FOREIGN KEY (pos_manager)	REFERENCES tbl_user_data (user_id)	
)ENGINE=InnoDB;
