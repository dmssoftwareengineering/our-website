<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Homepage';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['sitemap'] = 'Homepage/sitemap';
$route['sitemap.xml'] = 'Homepage/sitemap';

$route['reach-us'] = 'Contact/index';

$route['who-we-are'] = 'About/index';

$route['partners'] = 'Homepage/partners';

$route['what-we-do'] = 'Services/index';
$route['what-we-do/payroll-and-bpo-services'] = 'Services/goToPayRoll';
$route['what-we-do/tailored-application-software-development'] = 'Services/goToSoftwareDev';
$route['what-we-do/creative-web-design-and-development'] = 'Services/goToWebDev';
$route['what-we-do/data-capturing-and-digitizing'] = 'Services/goToCaptureData';
$route['what-we-do/mobile-development'] = 'Services/goToMobileDev';
$route['what-we-do/cloud-solutions'] = 'Services/goToCloudSolutions';
$route['what-we-do/security-solutions'] = 'Services/goToSecuritySolutions';
$route['what-we-do/managed-services'] = 'Services/goToManagedServices';

/* Product Pages - Start */
$route['products/deposit-information-system'] = 'Products/goToDIS';
$route['products/dms-payadmin'] = 'Products/goToPayadmin';

$route['products/crbsl-regulatory-reporting-system'] = 'Products/goToCRRS';
$route['products/customs-remittance-ms'] = 'Products/goToCRMS';
$route['products/electronic-fund-transfer'] = 'Products/goToEFTN';
$route['products/acuire'] = 'Products/goToAcuire';
$route['products/human-resource-information-system'] = 'Products/goToHRIS';
$route['products/leave-and-time-attendance'] = 'Products/goToLeaveTas';
$route['products/electronic-payslip-on-cloud'] = 'Products/goToEpaySlip';
$route['products/milk-collection-centre-ms'] = 'Products/goToMCCMS';
$route['products/secure-file-transfer'] = 'Products/goToSFT';
$route['products/education-management-system'] = 'Products/goToEMS';

$route['products/teamics'] = 'Products/goToTeamics';
$route['products/sophos-firewall'] = 'Products/goToSophos';
$route['products/amlock'] = 'Products/goToAmlock';
$route['products/premia'] = 'Products/goToPremia';
$route['products/mutual-fund'] = 'Products/goToMfund';
$route['products/bit-defender'] = 'Products/goToBitDefender';
$route['products/bank-sys'] = 'Products/goToBankSys';
$route['products/crib-information-management-system'] = 'Products/goToCRIMS';
$route['products/data-scan-online'] = 'Products/goToDatascan';
$route['products/aura-portal'] = 'Products/goToAuraPortal';
/* Product Pages - End */

/* Dashboard - Start */
$route['dashboard/applicant/view/(:num)'] = 'Dashboard/applicantView/$1';

$route['dashboard/position/view/(:num)'] = 'Dashboard/positionView/$1';
$route['dashboard/position/add'] = 'Dashboard/positionAdd';

$route['dashboard/user/add'] = 'Dashboard/userAdd';
$route['dashboard/user/view/(:num)'] = 'Dashboard/userView/$1';
$route['dashboard/user/change-password'] = 'Dashboard/changePasswordView';

$route['dashboard/interview/(:num)'] = 'Dashboard/interView/$1';

$route['dashboard/role/add'] = 'Dashboard/roleAdd';
/* Dashboard - End */