<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	
	public function __construct(){
    	parent::__construct();		
		$this->load->model("Model_core");   
    }
	
	public function index() {
		$data = array(
			"status" => $this->Model_core->setActive("about"),
			"seo" => $this->Model_core->getSeoTags("about")
		);
	
		$this->Model_core->checkBrowser("about_us", $data);
	}
}
