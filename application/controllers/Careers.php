<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careers extends CI_Controller {
	
	public function __construct() {
    	parent::__construct();
		$this->load->model("Model_core");
		$this->load->model("Model_careers");
		$this->load->model("Model_contact"); 
		$this->load->model("Model_dashboard");
    }
	
	public function index() {
		$data = array(
			"status" => $this->Model_core->setActive("careers"),
			"seo" => $this->Model_core->getSeoTags("careers"),
			"positions" => $this->Model_careers->getPositions()
		);
		
		$this->Model_core->checkBrowser('careers/index', $data);
	}
	
	public function view($pos_id) {
		$data = array(
			"status" => $this->Model_core->setActive("careers"),
			"seo" => $this->Model_core->getSeoTags("careers"),
			"pos_data" => $this->Model_dashboard->getPositionData($pos_id),
		);
		
		$this->Model_core->checkBrowser('careers/single_career', $data);
	}
	
	public function apply() {
		$data = array(
			"status" => $this->Model_core->setActive('careers'),
			"positions" => $this->Model_careers->getPositions(),
			"csrf_token" => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("careers", "apply")
		);
		
		$this->Model_core->checkBrowser('careers/apply', $data);
	}
	
	public function sendResume() {		
		if ($this->Model_core->checkForPost()) {
			$data["first_name"] = $this->Model_core->setRule("first_name");	
			$data["last_name"] = $this->Model_core->setRule("last_name");	
			$data["mobile_number"] = $this->Model_core->setRule("mobile_number");	
			$data["email"] = $this->Model_core->setRule("email");	
			$data["position"] = $this->Model_core->setRule("position");	
			$data["linkedin"] = $this->Model_core->setRule("linkedin");	
			$data["position_select"] = $this->Model_core->setRule("position_select");	
			
			if ($this->form_validation->run()) {	
				if ($this->Model_core->checkForPost()) {				
					$this->Model_careers->sendResume($data);				
				}				
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "Bad Request", array($this->form_validation->error_array()));
		
				$this->load->view("json_out", $data);
			}
		}
	}
	
	public function getApplicationResume($application_id) {
		$this->Model_careers->getApplicationResume($application_id);
	}
}