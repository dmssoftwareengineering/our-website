<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Contact extends CI_Controller {
	
	private $csrf_token = "";
	
	public function __construct() {
    	parent::__construct();		
		$this->load->model("Model_core");   
		$this->load->model("Model_contact"); 
    }

	public function index() {	
		$data = array(
			"status" => $this->Model_core->setActive("contact"),
			"csrf_token" => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("home")
		);
	
		$this->Model_core->checkBrowser("contact", $data);
	}
	
	public function requestService() {
		if ($this->Model_core->checkForPost()) {
			
			$data["contact_number"] = $this->Model_core->setRule("contact_number");	
			$data["email"] = $this->Model_core->setRule("email");	
			$data["message"] = $this->Model_core->setRule("message");	
			$data["person_name"] = $this->Model_core->setRule("person_name");	
			$data["com_name"] = $this->Model_core->setRule("com_name");	
			$data["token"] = $this->Model_core->setRule("token");	
			$data["service_type"] = $this->Model_core->setRule("service_type");	
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->Model_contact->validateCSRF_token($data["token"])) {				
				if ($this->form_validation->run()) {
					$data["output"] = $this->Model_contact->requestService($data);
				} else {
					$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
				}
			} else 
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "Please don\"t try to do that.");
			
			$this->load->view("json_out", $data);
		}
	}
}