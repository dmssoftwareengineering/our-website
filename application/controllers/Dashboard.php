<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	private $user_in = false;
	
	public function __construct() {
    	parent::__construct();		
		$this->load->model("Model_core");   
		$this->load->model("Model_dashboard");
		$this->load->model("Model_careers");
		$this->load->model("Model_contact"); 
		$this->load->model("cdash/Model_positions"); 		
		$this->load->model("cdash/Model_permission"); 		
		$this->load->model("cdash/Model_interviews"); 		
		
		$this->user_in = $this->session->has_userdata('email') ? "1" : "0";
    }

	public function index() {	
		$data = array(
			"user_in" => $this->user_in,
			"interviews" => $this->Model_interviews->interviewStatistic(),
			"overall_recruit" => $this->Model_positions->overall_resume_recruitment(),
			"resumes_position" => $this->Model_positions->resumes_open_position(),
			"user_data" => $this->Model_dashboard->getSignedUserData(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Applicants' => array(
					'View Application'
				), 
				'Interview' => array(
					'View Interview'
				)
			))
		);
		
		$this->has_permission($data['has_permission'] , "dashboard/index", $data);
	}	
	private function has_permission($status, $view, $data = array()){	
		if (!$status) {
			if ($this->user_in == '0') {
				redirect(base_url('dashboard/login'));
			}
			$this->load->view("dashboard/error_403", $data);
		} else {
			$this->load->view($view, $data);			
		}
	}	
	
	public function login() {		
		$data = array(
			"user_in" => $this->user_in
		);
		
		$this->load->view("dashboard/login", $data);
	}
	
	public function loginUser() {
		if ($this->Model_core->checkForPost()) {
			$data["email"] = $this->Model_core->setRule("email");
			$data["password"] = $this->Model_dashboard->setRule("password");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$data["output"] = $this->Model_dashboard->loginUser($data);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}
	
	public function logout() {
		$this->Model_dashboard->log_out();
	}
	
	public function applicants() {
		$data = array(
			"user_in" => $this->user_in,
			"resumes" => $this->Model_dashboard->getAllResumes(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Applicants' => array(
					'View Application'
				)
			))
		);
		
		$this->has_permission($data['has_permission'] , "dashboard/resumes", $data);
	}
	public function applicantView($app_id) {
		$data = array(
			"user_in" => $this->user_in,
			"user_data" => $this->Model_dashboard->getApplicantData($app_id),
			"csrf_token" => $this->Model_contact->getCSRF_token(),
			"interviews" => $this->Model_interviews->getApplicationInterviews($app_id),
			"users" => $this->Model_dashboard->getAllUsers(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Applicants' => array(
					'View Application', 'Review Application'
				)
			))
		);
	
		$this->has_permission($data['has_permission'] , "dashboard/single_resume", $data);
	}
	
	public function changeAppState() {
		if ($this->Model_core->checkForPost()) {
			$data["app_id"] = $this->Model_dashboard->setRule("pos-id");
			$data["app_status"] = $this->Model_dashboard->setRule("pos-status");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$data["output"] = $this->Model_dashboard->changeAppState($data);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}
	
	public function positions() {
		$data = array(
			"user_in" => $this->user_in,
			"positions" => $this->Model_dashboard->getAllPositions()
		);
	
		$this->load->view("dashboard/positions", $data);
	}
	public function positionView($pos_id){
		$data = array(
			"user_in" => $this->user_in,
			"pos_data" => $this->Model_dashboard->getPositionData($pos_id),
			"csrf_token" => $this->Model_contact->getCSRF_token()
		);
	
		$this->load->view("dashboard/single_position", $data);
	}
	public function positionAdd(){
		$data = array(
			"user_in" => $this->user_in,
			"csrf_token" => $this->Model_contact->getCSRF_token(),
			"users" => $this->Model_dashboard->getAllUsers(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Users' => array(
					'View User'
				),
				'Positions' => array(
					'Add Position'
				)
			))			
		);
	
		$this->has_permission($data['has_permission'] , "dashboard/add_position", $data);
	}
	public function addPosition(){
		if ($this->Model_core->checkForPost()) {
			$data["pos-name"] = $this->Model_dashboard->setRule("pos-name");
			$data["pos-req"] = $this->Model_dashboard->setRule("pos-req");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$data["output"] = $this->Model_dashboard->addPosition($data);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}
	public function changePositionStatus(){
		if ($this->Model_core->checkForPost()) {
			$data["pos-id"] = $this->Model_dashboard->setRule("pos-id");
			$data["pos-status"] = $this->Model_dashboard->setRule("pos-status");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$data["output"] = $this->Model_dashboard->changePositionStatus($data);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}
	
	public function permissions() {
		$data = array(
			"user_in" => $this->user_in,
			"permissions" => $this->Model_dashboard->getAllPermissions(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Permissions' => array(
					'View Permissions'
				)
			))
		);
		
		$this->has_permission($data['has_permission'] , "dashboard/permissions", $data);
	}
	
	public function roles() {
		$data = array(
			"user_in" => $this->user_in,
			"roles" => $this->Model_dashboard->getAllRoles(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Roles' => array(
					'View Role'
				),
				'Permissions' => array(
					'View Permissions'
				)
			))
		);
		
		$this->has_permission($data['has_permission'] , "dashboard/roles", $data);
	}
	public function roleAdd() {
		$data = array(
			"user_in" => $this->user_in,
			"csrf_token" => $this->Model_contact->getCSRF_token(),
			"permissions" => $this->Model_dashboard->getAllPermissions(),
			"has_permission" => $this->Model_permission->checkAccess(array(				
				'Roles' => array(
					'Add Role'
				),
				'Permissions' => array(
					'View Permissions'
				)
			))
		);
		
		$this->has_permission($data['has_permission'] , "dashboard/add_role", $data);
	}
	public function addRole() {
		if ($this->Model_core->checkForPost()) {
			$data["role-name"] = $this->Model_dashboard->setRule("role-name");
			$data["role-perms"] = $this->Model_dashboard->setRule("role-perms");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$data["output"] = $this->Model_dashboard->addRole($data);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}
	
	public function users() {
		$data = array(
			"user_in" => $this->user_in,
			"users" => $this->Model_dashboard->getAllUsers(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Users' => array(
					'View User'
				),
				'Permissions' => array(
					'View Permissions'
				),
				'Roles' => array(
					'View Role'
				)
			))
		);
		
		$this->has_permission($data['has_permission'] , "dashboard/users", $data);
	}
	public function userAdd(){
		$data = array(
			"user_in" => $this->user_in,
			"csrf_token" => $this->Model_contact->getCSRF_token(),
			"roles" => $this->Model_dashboard->getAllRoles(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Users' => array(
					'Add User'
				),
				'Roles' => array(
					'View Role'
				)
			))
		);
		
		$this->has_permission($data['has_permission'] , "dashboard/add_user", $data);
	}
	public function addUser(){
		if ($this->Model_core->checkForPost()) {
			$data["first_name"] = $this->Model_core->setRule("first_name");
			$data["last_name"] = $this->Model_core->setRule("last_name");
			$data["email"] = $this->Model_dashboard->setRule("email");
			$data["password"] = $this->Model_dashboard->setRule("password");
			$data["role_id"] = $this->Model_dashboard->setRule("pos-id");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$data["output"] = $this->Model_dashboard->addUser($data);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}
	public function changePasswordView() {
		$data = array(
			"user_in" => $this->user_in,
			"user_data" => $this->Model_dashboard->getSignedUserData(),
			"csrf_token" => $this->Model_contact->getCSRF_token()
		);
	
		$this->load->view("dashboard/user_password_change", $data);
	}
	public function changePassword() {
		if ($this->Model_core->checkForPost()) {	
			$data["email"] = $this->Model_dashboard->setRule("email");
			$data["password"] = $this->Model_dashboard->setRule("password");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$data["output"] = $this->Model_dashboard->changePassword($data, true);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}
	public function userView($user_id){
		$this->load->model("cdash/Model_useractivity"); 		

		$data = array(
			"user_in" => $this->user_in,
			"user_data" => $this->Model_dashboard->getUserData($user_id),
			"user_activity" => $this->Model_useractivity->getUserActivity($user_id),
			"csrf_token" => $this->Model_contact->getCSRF_token(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Users' => array(
					'View User Activity',
					'View User'
				),
				'Permissions' => array(
					'View Permissions'
				),
				'Roles' => array(
					'View Role'
				)
			))
		);
	
		$this->has_permission($data['has_permission'] , "dashboard/single_user", $data);
	}
	
	public function setInterview() {
		if ($this->Model_core->checkForPost()) {			
			$data["int_date"] = $this->Model_dashboard->setRule("int_date");
			$data["int_time"] = $this->Model_dashboard->setRule("int_time");
			$data["pos-manager"] = $this->Model_dashboard->setRule("pos-manager");
			$data["mobile_number"] = $this->Model_core->setRule("mobile_number");
			$data["user_id"] = $this->Model_dashboard->setRule("user_id");
			$data["app_id"] = $this->Model_dashboard->setRule("app_id");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$status = true;
				if (!$this->Model_core->validateTime($data['int_time'])) {
					$data["output"] = $this->Model_core->jsonResponse("false", "400", "Bad Request", array("int_time" => "Incorrect Format must be in HH:MM"));		
					$status = false;
				}
				
				if (!$this->Model_core->validateDate($data['int_date'])) {
					$data["output"] = $this->Model_core->jsonResponse("false", "400", "Bad Request", array("int_date" => "Incorrect Format must be in YYYY-MM-DD"));	
					$status = false;		
				}
				
				if ($status)				
					$data["output"] = $this->Model_interviews->setInterview($data);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}	
	public function interviews(){
		$data = array(
			"user_in" => $this->user_in,
			"interviews" => $this->Model_interviews->getInterviews(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Interview' => array(
					'View Interview'
				)
			))
		);
		
		$this->has_permission($data['has_permission'] , "dashboard/interviews", $data);
	}	
	public function interView($int_id) {
		$data = array(
			"user_in" => $this->user_in,
			"int_data" => $this->Model_interviews->getInterview($int_id),
			"csrf_token" => $this->Model_contact->getCSRF_token(),
			"has_permission" => $this->Model_permission->checkAccess(array(
				'Interview' => array(
					'View Interview', 'Change Interview State'
				)
			))
		);
		$data['app_data'] = $this->Model_dashboard->getApplicantData($data['int_data']['app_id']);		
		
		$this->has_permission($data['has_permission'] , "dashboard/single_interview", $data);
	}	
	public function changeInterviewState() {
		if ($this->Model_core->checkForPost()) {			
			$data["state"] = $this->Model_dashboard->setRule("state");
			$data["int_id"] = $this->Model_dashboard->setRule("user_id");
			$data["remark"] = $this->Model_core->setRule("message");
			
			if (!$this->input->is_ajax_request()) {
				$data["output"] = $this->Model_core->jsonResponse("false", "400", "No direct script access allowed.");
				$this->load->view("json_out", $data);				
				return;
			}
			
			if ($this->form_validation->run()) {
				$data["output"] = $this->Model_interviews->changeInterviewState($data);
			} else {
				$data["output"] = $this->Model_core->jsonResponse("false", "404", "Bad Request", array($this->form_validation->error_array()));
			}
			
			$this->load->view("json_out", $data);
		}
	}
}