<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {
	
	public function __construct() {
    	parent::__construct();		
		$this->load->model("Model_core");   
    }

	public function index() {	
		$data = array(
			"status" => $this->Model_core->setActive("home"),
			"seo" => $this->Model_core->getSeoTags("home")
		);
	
		$this->Model_core->checkBrowser("index", $data);
	}
	
	public function partners(){
		$data = array(
			"status" => $this->Model_core->setActive("partners"),
			"seo" => $this->Model_core->getSeoTags("partners")
		);
		
		$this->Model_core->checkBrowser("partners", $data);
	}
	
	public function showAdnic(){
		$this->Model_core->checkBrowser("adnic/index");
	}
	
	public function sitemap() {
		$this->load->view('sitemap');
	}
}