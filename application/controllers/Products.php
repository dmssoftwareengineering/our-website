<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Products extends CI_Controller {
	
	public function __construct() {
    	parent::__construct();		
		$this->load->model("Model_core");   
		$this->load->model("Model_products");   
    }
	
	public function index() {
		$data = array(
			"status" => $this->Model_core->setActive("services"),
			"seo" => $this->Model_core->getSeoTags("products")
		);
	
		$this->Model_core->checkBrowser("products/index", $data);
	}
	
	public function goToProduct($prod_name, $prod_id) {		
		$prod_id = $prod_id-1;
		
		$prod_data = $this->Model_products->product_title[$prod_id];
		
		if($prod_id <= 6) {
			$prod_type = "Banking";
		} else if ($prod_id == 7 || $prod_id == 8) {
			$prod_type = "Manufacturing";
		} else if ($prod_id > 8) {
			$prod_type = "Diversified";
		} else {
		
		}
		
		$data = array(
			"status" => $this->Model_core->setActive("services"),
			"product_name" => $prod_data,
			"prod_type" => $prod_type,
			"prod_id" => $prod_id
		);
		
		$this->load->view("products/product_single", $data);
	}
	
	/* Product Pages - Start */
	public function goToDIS() {
		$this->load->view("products/dis", $this->Model_core->setActive("services"));
	}
	
	public function goToPayadmin(){
		$this->load->view("products/payadmin", $this->Model_core->setActive("services"));
	}
	
	public function goToTeamics(){
		$this->load->view("products/teamics", $this->Model_core->setActive("services"));
	}
	
	public function goToAcuire(){
		$this->load->view("products/acuire", $this->Model_core->setActive("services"));
	}
	/* Product Pages - End */
}
