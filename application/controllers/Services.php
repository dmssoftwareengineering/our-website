<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {
	
	public function __construct(){
    	parent::__construct();		
		$this->load->model("Model_core");   
		$this->load->model("Model_contact"); 
    }
	
	public function index() {		
		$data = array(
			"status" => $this->Model_core->setActive("services"),
			"seo" => $this->Model_core->getSeoTags("services")
		);
	
		$this->Model_core->checkBrowser("services/index", $data);
	}
	
	public function goToService($prod_name, $prod_id) {		
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token()
		);
		
		$this->load->view('services/service_single', $data);
	}
	
	public function goToPayRoll() {
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("services", "payroll")
		);
		
		$this->load->view('services/payroll_bpo', $data);
	}
	
	public function goToSoftwareDev() {
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("services", "software")
		);
		
		$this->load->view('services/software_development', $data);
	}
	
	public function goToWebDev() {
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("services", "web")
		);
		
		$this->load->view('services/web_development', $data);
	}
	
	public function goToCaptureData() {
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("services", "data-capture")
		);
		
		$this->load->view('services/data_capture', $data);
	}
	
	public function goToMobileDev(){
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("services", "mobile")
		);
		
		$this->load->view('services/mobile_development', $data);		
	}
	
	public function goToCloudSolutions(){
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("services", "cloud")
		);
		
		$this->load->view('services/cloud_solution', $data);		
	}
	
	public function goToSecuritySolutions(){
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("services", "security")
		);
		
		$this->load->view('services/security_solution', $data);		
	}
	
	public function goToManagedServices(){
		$data = array(
			'status' => $this->Model_core->setActive('services'),
			'csrf_token' => $this->Model_contact->getCSRF_token(),
			"seo" => $this->Model_core->getSeoTags("services", "managed")
		);
		
		$this->load->view('services/managed_services', $data);		
	}
}