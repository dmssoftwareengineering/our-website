<?php

class Model_careers extends CI_Model {
	public function __construct() {
    	parent::__construct();      
		$this->load->model("Model_core");
		$this->load->helper('download');
    }
	
	public function sendEmail() {
				
	}	
		
	public function generateResumeLink($application_id) {
		$application_id = substr(md5(rand()),0,10).$application_id."res".substr(md5(rand()),0,10);
		return base_url('careers/getapplicationresume/'.$application_id);
	}
	
	public function getApplicationResume($application_id) {
		if (strlen($application_id) < 20 || strlen($application_id) > 50) {
			$result['output'] = $this->Model_core->jsonResponse("false", "400", "Invalid Application Resume");
			$this->load->view('json_out', $result);
		}
		
		$old_app_id = $application_id;
		$application_id = substr($application_id, 10, strlen($application_id));		
		$application_id = explode("res", $application_id);
		
		if (sizeof($application_id) == 2) {
			$application_id = $application_id[0];
		} else {
			$this->load->view('json_out', array(
				"output" => $this->Model_core->jsonResponse("false", "404", "Invalid Application Resume")
			));
		}
		$db_query = "SELECT file_name FROM tbl_applications WHERE app_id = ?";
		$db_query = $this->db->query($db_query, array($application_id));
	
		if ($db_query->num_rows() == 0) {
			exit($this->Model_core->jsonResponse("false", "404", "No Such Application Resume Found"));
		}
		
		$file_name = $db_query->result_array()[0]['file_name'];
		$this->download($file_name);
	}	
	function download($filename) {
		$file_download = "resumes/".$filename;
	
		if (file_exists($file_download) == 1) {
			$data = file_get_contents(base_url($file_download));
			force_download($filename, $data);
		} else {//File Not Found
			exit($this->Model_core->jsonResponse("false", "404", "No Such Application Resume Found"));
		}
	}
	
	/* Frontend Functions - Start */	
	public function getPositions() {//Get all Positions that are active
		$query = "SELECT pos_id, pos_name FROM tbl_positions WHERE pos_active = '1'";
		
		$query = $this->db->query($query);		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$data[] = array(
					"pos_id" => $row['pos_id'],
					"pos_name" => $row['pos_name']
				);
			}
			
			return $data;
		}
		
		return array();		
	}
	
	public function sendResume($data) {
		$result = array(
			"status" => true,
			"response_code" => "200",
			"message" => "Resume Added Successfully",
			"output" => ""
		);
		
		$upload_dir = './resumes/';
		
		$status = true;
		
		$this->db->trans_begin();
		
		$config['upload_path'] = $upload_dir;
		$config['allowed_types'] = 'doc|docx|pdf';
		$config['file_name'] = substr(md5(rand()),0,10);
		$config['overwrite'] = false;
		$config['max_size'] = '2048';
		
		$file_ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
		$file_ext = $config['file_name'].".".$file_ext;
	
		if ($_FILES['file']['size'] != 0) {
			$this->load->library('upload', $config);
			
			if (!$this->upload->do_upload('file')) {
				$status = false;
				$result = array(
					"status" => false,
					"response_code" => "400",
					"message" => "Invalid Uploaded Resume"
				);
			}
		} else {
			$status = false;
			$result = array(
				"status" => false,
				"response_code" => "400",
				"message" => "No Resume Selected By User"
			);
		}
		
		$data["linkedin"] = str_replace("https://www.linkedin.com/in/", "", $data["linkedin"]);
		$data["linkedin"] = str_replace("/", "", $data["linkedin"]);
		
		if (strlen($data["linkedin"]) > 32) {
			$result['output'] = $this->Model_core->jsonResponse("false", "400", "Invalid LinkedIn URL");		
			$this->load->view('json_out', $result);
			return;
		}
		
        $query = "INSERT INTO tbl_applications values (NULL, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, '0', ?)";
        $query = $this->db->query($query, array($data["position_select"], $data["first_name"], $data["last_name"], $data["mobile_number"], $data["email"], $file_ext, $data["linkedin"]));
		
        if ($query != "1" || $query != 1) {
			$status = false;
			$result = array(
				"status" => false,
				"response_code" => "400",
				"message" => "Failed to Add Resume"
			);
        }
		
		if ($status) {
			$result = array(
				"status" => true,
				"response_code" => "200",
				"message" => "Resume Receieved."
			);
			
			$this->db->trans_commit();
			
			$application_id = $this->db->insert_id();
			/*$this->Model_core->sendEmailTemplate(array(
				"email" => $data["email"],
				"subject" => "We Recieved Your Application",
				"msg" => "Dear ".$data["first_name"].", <br/>We Received Your Resume, We'll Get Back to You As Soon As Possible.<br/> Best Regards, <br/>DMS Software Engineering"
			));*/
			
			$application_id = substr(md5(rand()), 0, 10).$application_id."res".substr(md5(rand()), 0, 10);
			
			/*$this->Model_core->sendEmailTemplate(array(
				"email" => $data["email"],
				"subject" => "New Resume Received From ".$data["first_name"],
				"msg" => $data["first_name"]." has applied for ".$data["position"]." on ".date("Y")."<br/> Please find the resume with this link ".base_url('careers/getApplicationResume/'.$application_id)
			));*/
		} else {
			$this->db->trans_rollback();
		}
		
		$result['output'] = $this->Model_core->jsonResponse($result['status'], $result['response_code'], $result['message']);		
		$this->load->view('json_out', $result);
	}	
	/* Frontend Functions - End */
	
	/* Backend Functions - Start */	
	/* Backend Functions - End */	
}