<?php

class Model_contact extends CI_Model {
	
	public function __construct() {
    	parent::__construct();      

		$this->generateCSRF_token();
    }    
	
	/* CSRF Token - Start */	
	private $csrf_token = "";	
	private $max_token_age = 5;//Minutes
	
	private function deadCSRF_token() {
		$csrf_token = $this->session->userdata('csrf_token');
		
		$token_time = new DateTime($csrf_token['created']);			
		$time_now = new DateTime(date("Y-m-d H:i:s"));
		
		$time_diff = $time_now->diff($token_time)->i;
		
		if ($time_diff > $this->max_token_age) return true;
		
		return false;
	}
	
	public function generateCSRF_token($create = false) {
		try {
			$this->csrf_token = $this->Model_core->generateString();
			
			if ($create) $this->session->unset_userdata('csrf_token');			
			
			if ($this->session->userdata('csrf_token') != false) {									
				if ($this->deadCSRF_token()) $this->generateCSRF_token(true);//Check Token Age
			} else {
				$this->session->set_userdata(array(
				   'csrf_token' => array(
						"token" => $this->csrf_token,
						"created" => date("Y-m-d H:i:s")
					)
				));
			}
			
			$this->csrf_token = $this->session->userdata('csrf_token');
		} catch(Exception $error) {
			$this->generateCSRF_token(true);
		}
	}
	
	public function validateCSRF_token($token) {
		$csrf_token = $this->session->userdata('csrf_token');
		
		if ($token == $csrf_token["token"]) {
			if ($this->deadCSRF_token()) return false;
			return true;
		}
		
		return false;
	}
	
	public function getCSRF_token() {
		return $this->csrf_token["token"];
	}		
	/* CSRF Token - End */
	
	/* Services - Start */
	public function requestService($data) {		
		$this->Model_contact->generateCSRF_token(true);
		
		//$response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Le5_GsUAAAAAKHnJ9fwtGeqlbpVX_Hnarij2I8o&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
		//if($response['success']) {
			return $this->Model_core->jsonResponse("true", "200", "Request Sent. We will contact you as soon as possible.");
		//}
		
		//return $this->Model_core->jsonResponse("false", "400", "Invalid Captcha");
	}
	/* Services - End */
}