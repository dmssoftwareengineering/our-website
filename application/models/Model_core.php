<?php

class Model_core extends CI_Model {
	
	public function __construct() {
    	parent::__construct();
		
		header("Access-Control-Allow-Origin: *"); 
        header("Access-Control-Allow-Credentials: true"); 
		
		//$this->initSecureHeaders();
    }
	
	private function initSecureHeaders() {     
		header("Access-Control-Allow-Methods: GET, POST");  
		header("Strict-Transport-Security: max-age=63072000; includeSubDomains; preload"); 
		header("Content-Security-Policy: default-src https:"); 
		header("X-XSS-Protection: 1; mode=block"); 
		header("X-Frame-Options: DENY"); 
		header("X-Content-Type-Options: nosniff"); 
		header("Referrer-Policy: strict-origin-when-cross-origin"); 
		
		header_remove("X-Powered-By");
		header_remove("Expires");	
		header_remove("Server");	
	}
	
	//Validation Rules - Start
	private $inputFields = array(
		"person_name" => array(
			"title" => 'Person Name',
			"regex" => 'required|trim|regex_match[/^[A-Za-z ]{5,20}$/]'
		),	
		"com_name" => array(
			"title" => 'Company Name',
			"regex" => 'required|trim|regex_match[/^[A-Za-z0-9 ]{3,20}$/]'
		),	
		"contact_number" => array(
			"title" => 'Contact Number',
			"regex" => 'required|trim|regex_match[/^([+][0-9]{2})[0-9]{8,10}$/]'
		),	
		"email" => array(
			"title" => 'Email',
			"regex" => 'required|valid_email'
		),		
		"token" => array(
			"title" => 'CSRF Token',
			"regex" => 'required|trim|regex_match[/^[A-Za-z0-9_]{32}$/]'
		),		
		"message" => array(
			"title" => 'User Message',
			"regex" => 'required|trim|regex_match[/^[A-Za-z0-9+_$# ]{10,250}$/]'
		),		
		"service_type" => array(
			"title" => 'Service Type',
			"regex" => 'required|trim|regex_match[/^Payroll|Manpower|Software|Other|Web|Job$/]'
		),
		"first_name" => array(
			"title" => 'First Name',
			"regex" => 'required|trim|regex_match[/^[A-Za-z ]{4,30}$/]'
		),
		"last_name" => array(
			"title" => 'Last Name',
			"regex" => 'required|trim|regex_match[/^[A-Za-z ]{5,40}$/]'
		),	
		"mobile_number" => array(
			"title" => 'Mobile Number',
			"regex" => 'required|trim|regex_match[/^[0-9]{10,11}$/]'
		),	
		"position" => array(
			"title" => 'Mobile Number',
			"regex" => 'required|trim|regex_match[/^[A-Za-z ]{10,30}$/]'
		),	
		"linkedin" => array(
			"title" => 'Mobile Number',
			"regex" => 'required|trim|regex_match[/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:\/?#[\]@!\$&\'\(\)\*\+,;=.]+$/]'
		),	
		"position_select" => array(
			"title" => 'Position Id',
			"regex" => 'required|trim|regex_match[/^[0-9]{1,3}$/]'
		)
	);
		
	public function setRule($field_name) {
		$this->form_validation->set_rules($field_name, $this->inputFields[$field_name]["title"], $this->inputFields[$field_name]["regex"]);		
		
		$data = $this->input->post($field_name, TRUE);
		$data = $this->security->xss_clean($data);
		
		return $data;
	}
	//Validation Rules - End
	
	public function getClientIP() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
	 
		return $ipaddress;
	}
	
	public function time_elapsed_string($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return implode(', ', $string);
	}
	
	public function getSeoTags($page_name, $sub = "") {//Get SEO Tags
		$seo_file = base_url("assets/core/json/seo_tags.json");		
		
		$seo_tags = file_get_contents($seo_file);
		$seo_tags = json_decode($seo_tags);
		
		$seo_tags = (array) $seo_tags;	

		$seo_tags[$page_name] = (array) $seo_tags[$page_name];
		$seo_tags[$page_name]['meta'] = (array) $seo_tags[$page_name]['meta'];
		
		$sub_data = array();
		
		if ($sub != "") {
			$seo_tags[$page_name]['sub'] = (array) $seo_tags[$page_name]['sub'];
			$seo_tags[$page_name]['sub'][$sub] = (array) $seo_tags[$page_name]['sub'][$sub];
			$seo_tags[$page_name]['sub'][$sub]['meta'] = (array) $seo_tags[$page_name]['sub'][$sub]['meta'];
			
			$sub_data = $seo_tags[$page_name]['sub'][$sub]['meta'];
		}
		
		return array(
			"name" => $seo_tags[$page_name],
			"desc" => $seo_tags[$page_name]['meta']['desc'],
			"keywords" => $seo_tags[$page_name]['meta']['keywords'],
			"sub_page" => $sub_data
		);
	}
		
	public function sendEmailTemplate($data) {//Send Email With Web Template
		$headers = "From: no-reply@dmsswe.azurewebsites.net\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		
		mail($data['email'], $data['subject'], $data['msg'], $headers);
	}		
	
	public function hashPassword($pass, $salt=FALSE) {
		if (!empty($salt)) $pass = $salt . implode($salt, str_split($pass, floor(strlen($pass)/2))) . $salt;
		return md5( $pass );
	}
		
	public function checkBrowser($view, $extra = array()) {		
		$this->load->library('user_agent');
		
		$browser = $this->agent->browser();
		$version = explode(".", $this->agent->version())[0];
		$version = (int)$version;
		
		$output = "";
		
		switch (strtolower($browser)) {
			case "safari":
				if ($version < 535) {
					$output = "This Browser Version is Not Supported. <br/> Please Use a Modern Browser Version";
				}
				break;
		}
		
		if ($this->agent->is_mobile()) {//Remove This After Development
			$data['output'] = "<strong>Dear Friend</strong>, <br/><br/>We appreciate your willingness to see our website but currently we are in the process of developing it. <br/>Till we are finished you will not be able to view through your mobile, So please use a computer to visit the website till then. <br/><br/> <strong>Thank You</strong>,<br/> DMS Software Engineering";
			//$this->load->view('json_out', $data);
			
			$this->load->view($view, $extra);
		} else {
			if ($output != "") {
				$data['output'] = $output;
				$this->load->view('json_out', $data);
			} else {
				$this->load->view($view, $extra);
			}
		}
	}
	
	public function checkForRecord($table, $column, $value, $msg = "Record") {
		$db_query = "SELECT * FROM ".$table." WHERE ".$column." = ?";
		$db_query = $this->db->query($db_query, array($value));
	
		if ($db_query->num_rows() != 1) {
			exit($this->jsonResponse("false", "404", "No Such $msg Found"));
		}
	}
	
	public function setActive($page) {
        $data = array('', '', '', '', '', '');
		
		switch ($page) {
			case 'home':
				$data[0] = 'active';
				break;
			case 'contact':
				$data[1] = 'active';
				break;
			case 'about':
				$data[2] = 'active';
				break;
			case 'careers':
				$data[3] = 'active';
				break;
			case 'services':
				$data[4] = 'active';
				break;
			case 'partners':
				$data[5] = 'active';
				break;
		}		
		
		$data['status'] = $data;
        return $data;
    }
	
	public function validateDateTime($date_time) {
		$d = DateTime::createFromFormat('Y-m-d H:i:s', $date_time);
		return $d && $d->format('Y-m-d H:i:s') === $date_time;
	}	
	public function validateDate($date) {
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') === $date;
	}	
	public function validateTime($time) {
		$d = DateTime::createFromFormat('H:i', $time);
		return $d && $d->format('H:i') === $time;
	}
	
	public function generateString($count = 32) {
        $alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
        $data = "";
        for ($i=0;$i<$count;$i++) {
            $data = $data.$alpha[rand(0,strlen($alpha)-1)];
        }
		
        return md5($data);
	}

	public function jsonResponse($status, $response_code, $message, $data = null) {
        http_response_code((int)$response_code);
		header("Content-Type: text/json;"); 
        $response = array(
            "status" => $status,
            "response_code" => $response_code,
            "message" => $message
        );
        if ($data != null) {
            $response["data"] = $data;
        }
        return json_encode($response);
    }
	
	public function checkForPost() {
		if($this->input->method() == "post") {
			if ($this->input->post()) {
				return true;
			}
		}
		
		$data['output'] = $this->jsonResponse("false", "400", "Bad Request");
		$this->load->view('json_out', $data);
	}
}