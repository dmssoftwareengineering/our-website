<?php

class Model_dashboard extends CI_Model {
	
	public function __construct() {
    	parent::__construct();		
		$this->load->model("Model_core");   
		$this->load->model("cdash/Model_useractivity"); 
    }
	
	//Validation Rules - Start
	private $inputFields = array(	
		"password" => array(
			"title" => 'Password',
			"regex" => 'required|trim|regex_match[/^[A-Za-z0-9\.@]{5,60}$/]'
		),
		"pos-name" => array(
			"title" => 'Position Name',
			"regex" => 'required|trim|regex_match[/^[A-Za-z0-9 ]{10,30}$/]'
		),
		"pos-req" => array(
			"title" => 'Position Requirement',
			"regex" => 'required|trim|regex_match[/^[\w\W]{30,}$/]'
		),
		"pos-id" => array(
			"title" => 'Position Id',
			"regex" => 'required|trim|regex_match[/^[0-9]{1,8}$/]'
		),
		"pos-status" => array(
			"title" => 'Position Status',
			"regex" => 'required|trim|regex_match[/^[0-9]{1,2}$/]'
		),
		"email" => array(
			"title" => 'DMSSWE Email Address',
			"regex" => 'required|trim|regex_match[/^[A-Za-z0-9_]+@dmsswe.com$/]'
		),
		"user_id" => array(
			"title" => 'User Id',
			"regex" => 'required|trim|regex_match[/^[0-9]{1,8}$/]'
		),
		"app_id" => array(
			"title" => 'Application Id',
			"regex" => 'required|trim|regex_match[/^[0-9]{1,8}$/]'
		),
		"int_date" => array(
			"title" => 'Interview Date',
			"regex" => 'required|trim'
		),
		"int_time" => array(
			"title" => 'Interview Time',
			"regex" => 'required|trim'
		),
		"state" => array(
			"title" => 'Interview State',
			"regex" => 'required|trim|regex_match[/^(C|N|O)$/]'
		),
		"role-name" => array(
			"title" => 'Role Name',
			"regex" => 'required|trim|regex_match[/^[A-Za-z0-9 ]{5,40}$/]'
		),
		"role-perms" => array(
			"title" => 'Role Name',
			"regex" => 'required|trim|regex_match[/^[0-9,]+$/]'
		),
		"pos-manager" => array(
			"title" => 'Position Manager',
			"regex" => 'required|trim|regex_match[/^[0-9]{1,8}$/]'
		)
	);
	
	public function setRule($field_name) {
		$this->form_validation->set_rules($field_name, $this->inputFields[$field_name]["title"], $this->inputFields[$field_name]["regex"]);		
		
		$data = $this->input->post($field_name, TRUE);
		$data = $this->security->xss_clean($data);
		
		return $data;
	}
	//Validation Rules - End
	
	//Authentication Functions - Start
	public function user_in() {//Check if user logged in
		if ($this->session->has_userdata('email') && $this->session->userdata('first_name')) {
			return true;
		}
		return false;
	}
	
	private function create_session_data($data) {///Create User session			
		$this->session->set_userdata(array(
			'email' => $data['email'],
			'user_id' => $data['user_id'],
			'role_id' => $data['role_id'],
			'last_name' => $data['last_name'],
			'role_name' => $data['role_name'],
			'first_name' => $data['first_name'],
			'role_permission' => $data['role_permission'],
			'first_time' => $data['first_time']
		));
		
		return $this->user_in();
	}
	
	public function getSignedUserData() {
		if (!$this->user_in()) {
			return array();
		}
		
		return array(
			'email' => $this->session->userdata('email'),
			'user_id' => $this->session->userdata('user_id'),
			'role_id' => $this->session->userdata('role_id'),
			'last_name' => $this->session->userdata('last_name'),
			'role_name' => $this->session->userdata('role_name'),
			'first_name' => $this->session->userdata('first_name'),
			'role_permission' => $this->session->userdata('role_permission'),
			'first_time' => $this->session->userdata('first_time')
		);
	}
			
	public function log_out() {
		$this->session->sess_destroy();
		
		if (!$this->user_in()) {
			$this->Model_useractivity->recordUseractivity("User Signed Out");
			return true;
		}
		
		redirect(base_url('dashboard/login'));
	}
	
	public function loginUser($data) {		
		$query = "SELECT ud.user_id, ud.role_id, ud.first_name, ud.last_name, r.role_name, r.role_permission, ul.first_time FROM tbl_user_data ud, tbl_user_login ul, tbl_roles r WHERE ud.user_id = ul.user_id AND ud.role_id = r.role_id AND ul.email_address = ? AND ul.password = ?";
		
		$data['password'] = $this->Model_core->hashPassword($data['password']);
		
        $query = $this->db->query($query, array($data['email'], $data['password']));

        if ($query->num_rows() > 0) {	
			$query = $query->result_array()[0];
			
			$perms = $query['role_permission'];
			$perms = json_decode($perms);
			
			$perm_length = sizeof($perms);
			
			$perm_data = array();
			
			for ($i=0;$i<$perm_length;$i++) {
				$perm_d = $this->db->query("SELECT module_name, perm_name FROM tbl_permissions WHERE permission_id = ?", array($perms[$i]))->result_array()[0];
				
				$perm_data[$perm_d['module_name']][] = $perm_d['perm_name'];
			}
			
			if ($this->create_session_data(array(
				'email' => $data['email'],
				'user_id' => $query['user_id'],
				'role_id' => $query['role_id'],
				'last_name' => $query['last_name'],
				'role_name' => $query['role_name'],
				'first_name' => $query['first_name'],
				'first_time' => $query['first_time'],
				'role_permission' => $perm_data
			))) {
				$this->Model_useractivity->recordUseractivity("User Signed In");
				
				$user_status = array(
					"first_time" => $query['first_time']
				);
				
				return $this->Model_core->jsonResponse("true", "200", "Authentication Successful", $user_status);
			}
			
			return $this->Model_core->jsonResponse("false", "500", "Unspecified Error Occurred.", array());
		}
		
		return $this->Model_core->jsonResponse("false", "404", "Email Address Not Verified", array(
			"email" => "Invalid Login Credentials",
			"password" => "Invalid Login Credentials"
		));
	}
	//Authentication Functions - End
	
	//Application Functions - Start
	public function getAllResumes() {
		$query = "SELECT a.app_id, a.first_name, a.last_name, a.mobile_number, a.reviewed, a.email, a.date_created, a.linked_in, p.pos_name, p.pos_id FROM tbl_applications a, tbl_positions p WHERE a.pos_id = p.pos_id ORDER BY a.date_created DESC";
		
		$query = $this->db->query($query);		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$reviewed_status  = "Not Reviewed" ;
				switch($row['reviewed']){
					case "1": $reviewed_status = 'Reviewed'; break;
					case "2": $reviewed_status = 'Email Sent'; break;
					case "3": $reviewed_status = 'Recruited'; break;						
				}
				
				$data[] = array(
					"email" => $row['email'],
					"app_id" => $row['app_id'],
					"pos_id" => $row['pos_id'],					
					"pos_name" => $row['pos_name'],
					"linked_in" => $row['linked_in'],
					"last_name" => $row['last_name'],
					"first_name" => $row['first_name'],
					"reviewed_id" => $row['reviewed'],
					"date_created" => explode(" ",$row['date_created'])[0],
					"mobile_number" => $row['mobile_number'],
					"reviewed_status" => $reviewed_status
				);
			}
			
			return $data;
		}
		
		return array();		
	}	
	public function getApplicantData($app_id) {
		$query = "SELECT a.app_id, a.first_name, a.last_name, a.mobile_number, a.reviewed, a.file_name, a.email, a.date_created, a.linked_in, p.pos_name, p.pos_id FROM tbl_applications a, tbl_positions p WHERE a.pos_id = p.pos_id AND a.app_id = ?";
		
		$query = $this->db->query($query, array($app_id));		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$reviewed_status  = "Not Reviewed" ;
				switch($row['reviewed']){
					case "1": $reviewed_status = 'Reviewed'; break;
					case "2": $reviewed_status = 'Email Sent'; break;
					case "3": $reviewed_status = 'Recruited'; break;						
				}
				
				$resume_file = base_url('resumes/'.$row['file_name']);
				
				$data[] = array(
					"email" => $row['email'],
					"app_id" => $row['app_id'],
					"pos_id" => $row['pos_id'],
					"pos_name" => $row['pos_name'],
					"last_name" => $row['last_name'],
					"linked_in" => $row['linked_in'],
					"first_name" => $row['first_name'],
					"reviewed_id" => (int)$row['reviewed'],
					"resume_file" => $resume_file,					
					"resume_link" => $this->Model_careers->generateResumeLink($row['app_id']),"date_created" => explode(" ",$row['date_created'])[0],
					"mobile_number" => $row['mobile_number'],		
					"reviewed_status" => $reviewed_status
				);
			}
			
			return $data[0];
		}
		
		return array();		
	}
	
	public function changeAppState($data) {
		$db_query = "SELECT * FROM tbl_applications WHERE app_id = ?";
		$db_query = $this->db->query($db_query, array($data['app_id']));
	
		if ($db_query->num_rows() != 1) {
			return $this->Model_core->jsonResponse("false", "404", "No Such Application Available");
		}
		
		$this->db->trans_begin();
		
		$this->db->set('reviewed', $data['app_status']);
		$this->db->where('app_id', $data['app_id']); 
		
		if ($this->db->update('tbl_applications')) {
			$this->db->trans_commit();			
			$this->Model_useractivity->recordUseractivity("Changed Application State of ".$data['app_id']." to ".$data['app_status']);
			return $this->Model_core->jsonResponse("true", "200", "Application Status Changed");
		}
		
		$this->db->trans_rollback();		
		return $this->Model_core->jsonResponse("false", "500", "Internal Server Error Occurred");
	}
	//Application Functions - End
	
	//Position Functions - Start
	public function addPosition($data) {
		$this->db->trans_begin();
		
		$query = "INSERT INTO tbl_positions VALUES (NULL, ?, ?, '1', CURRENT_TIMESTAMP)";
        $query = $this->db->query($query, array($data["pos-name"], $data["pos-req"]));
		
        if ($query == "1") {
			$pos_id = $this->db->insert_id();			
			$user_id = $this->session->userdata('user_id');
			
			$query = "INSERT INTO tbl_position_owner VALUES (?, ?, CURRENT_TIMESTAMP)";
			$query = $this->db->query($query, array($pos_id, $user_id));
			if ($query == "1") {
				$this->db->trans_commit();
				$this->Model_useractivity->recordUseractivity("Added New Position $pos_id");
				return $this->Model_core->jsonResponse("true", "200", "Position Added Successfully");
			}
		}
		
		$this->db->trans_rollback();		
		return $this->Model_core->jsonResponse("false", "500", "Internal Server Error Occurred");
	}
	public function changePositionStatus($data) {
		$db_query = "SELECT * FROM tbl_positions WHERE pos_id = ?";
		$db_query = $this->db->query($db_query, array($data['pos-id']));
	
		if ($db_query->num_rows() != 1) {
			return $this->Model_core->jsonResponse("false", "404", "No Such Position Available");
		}
		
		$this->db->trans_begin();
		
		$this->db->set('pos_active', $data['pos-status']);
		$this->db->where('pos_id', $data['pos-id']); 
		
		if ($this->db->update('tbl_positions')) {
			$this->db->trans_commit();			
			$this->Model_useractivity->recordUseractivity("Changed Position State of ".$data['pos-id']." to ".$data['pos-status']);
			return $this->Model_core->jsonResponse("true", "200", "Position Status Changed");
		}
		
		$this->db->trans_rollback();		
		return $this->Model_core->jsonResponse("false", "500", "Internal Server Error Occurred");
	}
	public function getAllPositions() {//Get all Positions that are active
		$query = "SELECT pos_id, pos_name,pos_active,pos_require FROM tbl_positions";
		
		$query = $this->db->query($query);		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$data[] = array(
					"pos_id" => $row['pos_id'],
					"pos_name" => $row['pos_name'],
					"pos_active" => $row['pos_active'],
					"pos_require" => str_replace("\n", "<br/>", substr($row['pos_require'], 0, 50))
				);
			}			
			
			return $data;
		}
		
		return array();		
	}	
	public function getPositionData($pos_id) {
		$query = "SELECT p.pos_name, p.pos_require, p.pos_active, p.pos_date, u.first_name, u.last_name, u.user_id FROM tbl_positions p, tbl_position_owner po, tbl_user_data u WHERE p.pos_id = po.pos_id AND po.user_id = u.user_id AND p.pos_id = ?";
		
		$query = $this->db->query($query, array($pos_id));		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				
				$data[] = array(
					"pos_id" => $pos_id,		
					"pos_date" => $row['pos_date'],
					"pos_name" => $row['pos_name'],
					"pos_active" => $row['pos_active'],			
					"pos_status" => $row['pos_active'] == '0' ? 'Position Closed' : 'Position Open',	
					"pos_manager" => $row['first_name']." ".$row['last_name'],
					"pos_require" => str_replace("\n", "<br/>", $row['pos_require']),	
					"pos_manager_id" => $row['user_id']
				);
			}
			
			return $data[0];
		}
		
		return array();	
	}
	//Position Functions - End
	
	//User Functions - Start
	public function getAllUsers() {
		$query = "SELECT ud.user_id, ud.first_name, ud.last_name, ul.email_address, r.role_name, r.role_id FROM tbl_user_data ud, tbl_user_login ul, tbl_roles r WHERE ud.user_id = ul.user_id AND ud.role_id = r.role_id";
		
		$query = $this->db->query($query);		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$data[] = array(
					"user_id" => $row['user_id'],
					"role_id" => $row['role_id'],
					"last_name" => $row['last_name'],
					"role_name" => $row['role_name'],
					"first_name" => $row['first_name'],
					"email_address" => $row['email_address']
				);
			}			
			
			return $data;
		}
		
		return array();	
	}
	public function addUser($data) {
		$db_query = "SELECT * FROM tbl_roles WHERE role_id = ?";
		$db_query = $this->db->query($db_query, array($data['role_id']));
	
		if ($db_query->num_rows() != 1) {
			return $this->Model_core->jsonResponse("false", "404", "No Such Role Available");
		}		
		
		$this->db->trans_begin();
		
		$data['password'] = $this->Model_core->hashPassword($data['password']);
		
		$query = "INSERT INTO tbl_user_data VALUES (NULL, ?, ?, ?, CURRENT_TIMESTAMP)";
        $query = $this->db->query($query, array($data["role_id"], $data["first_name"], $data["last_name"]));
		
        if ($query == "1") {
			$user_id = $this->db->insert_id();;
			
			$query = "INSERT INTO tbl_user_login VALUES (?, ?, ?, '1')";
			$query = $this->db->query($query, array($data["email"], $user_id, $data["password"]));
			
			if ($query == "1") {
				$this->db->trans_commit();				
				$this->Model_useractivity->recordUseractivity("Added New User ".$user_id);
				return $this->Model_core->jsonResponse("true", "200", "User Added Successfully");
			}
		}
		
		$this->db->trans_rollback();		
		return $this->Model_core->jsonResponse("false", "500", "Internal Server Error Occurred");
	}
	
	public function getUserData($user_id) {
		$query = "SELECT ud.first_name, ud.last_name, ud.role_id, ul.email_address, r.role_name, r.role_permission, ul.first_time FROM tbl_user_data ud, tbl_user_login ul, tbl_roles r WHERE ud.user_id = ul.user_id AND ud.role_id = r.role_id AND ud.user_id = ?";
		$query = $this->db->query($query, array($user_id));		

		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {			
				$role_permissions = array();

				$rp = json_decode($row['role_permission']);
				$rp_length = sizeof($rp);
				if ($rp_length > 0) {
					for ($i=0;$i<$rp_length;$i++) {
						$query_rp = "SELECT module_name, perm_name FROM tbl_permissions WHERE permission_id = ?";
						$query_rp = $this->db->query($query_rp, array($rp[$i]))->row();
						
						$role_permissions[] = array(
							"module" => $query_rp->module_name,
							"perm_name" => $query_rp->perm_name
						);
					}
				}					

				$data[] = array(
					"user_id" => $user_id,	
					"first_name" => $row['first_name'],
					"last_name" => $row['last_name'],
					"role_id" => $row['role_id'],
					"email_address" => $row['email_address'],			
					"role_name" => $row['role_name'],
					"role_permission" => $role_permissions,
					"first_time" => $row['first_time']
				);
			}
			
			return $data[0];
		}
		
		return array();	
	}
	
	public function changePassword($data, $first_time) {
		$db_query = "SELECT * FROM tbl_user_login WHERE email_address = ?";
		$db_query = $this->db->query($db_query, array($data['email']));
	
		if ($db_query->num_rows() != 1) {
			return $this->Model_core->jsonResponse("false", "404", "No Such User Available");
		}
		
		$db_query = $db_query->result_array()[0];
		
		$this->db->trans_begin();
		
		$data['password'] = $this->Model_core->hashPassword($data['password']);
		
		$this->db->set('password', $data['password']);
		$this->db->set('first_time', '0');
		$this->db->where('email_address', $data['email']); 
		
		if ($this->db->update('tbl_user_login')) {
			$this->db->trans_commit();			
			
			$this->session->sess_destroy();			
			
			$this->Model_useractivity->recordUseractivity($db_query['user_id']." Changed Password ".($first_time == 1 ? "First Time" : ""));
			return $this->Model_core->jsonResponse("true", "200", "Password Changed");
		}
		
		$this->db->trans_rollback();		
		return $this->Model_core->jsonResponse("false", "500", "Internal Server Error Occurred");
	}
	//User Functions - End
	
	//Permission Functions - Start
	public function getAllPermissions() {
		$query = "SELECT permission_id,perm_short_name,perm_name,module_name FROM tbl_permissions";
		
		$query = $this->db->query($query);		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$data[] = array(
					"perm_name" => $row['perm_name'],
					"module_name" => $row['module_name'],
					"permission_id" => $row['permission_id'],
					"perm_short_name" => $row['perm_short_name'],
				);
			}			
			
			return $data;
		}
		
		return array();		
	}
	//Permission Functions - End
	
	//Role Functions - Start
	public function getAllRoles() {
		$query = "SELECT role_id,role_name,role_permission FROM tbl_roles";
		
		$query = $this->db->query($query);		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$perm_json = json_decode($row['role_permission']);
				$permissions = "";
				
				for ($i=0;$i<sizeof($perm_json);$i++) {
					if ($i==5) break;
					
					$perm_online = "SELECT perm_name FROM tbl_permissions WHERE permission_id = ?";
					$perm_online = $this->db->query($perm_online, array($perm_json[$i]))->result_array()[0];
										
					$permissions = $permissions."<br/>".$perm_online['perm_name'];					
				}
				
				$permissions = rtrim($permissions, "<br/>");	
				
				$data[] = array(
					"role_id" => $row['role_id'],
					"role_name" => $row['role_name'],
					"permission" => $permissions
				);
			}
			
			return $data;
		}
		
		return array();		
	}
	public function addRole($data) {
		$this->db->trans_begin();
		
		$data["role-perms"] = "[".$data["role-perms"]."]";
		
		$query = "INSERT INTO tbl_roles VALUES (NULL, ?, ?)";
        $query = $this->db->query($query, array($data["role-name"], $data["role-perms"]));
		
        if ($query == "1") {
			$pos_id = $this->db->insert_id();
			$this->db->trans_commit();			
			$this->Model_useractivity->recordUseractivity("Added New Role ".$pos_id);
			return $this->Model_core->jsonResponse("true", "200", "Role Added Successfully");
		}
		
		$this->db->trans_rollback();		
		return $this->Model_core->jsonResponse("false", "500", "Internal Server Error Occurred");
	}
	//Role Functions - End
}