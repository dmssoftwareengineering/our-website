<?php

class Model_products extends CI_Model {
	
	public function __construct() {
    	parent::__construct();
    }   	
	
	public $product_title = array(
		"BANKSYS", "IMAGO", "CRIMS", "DIS", "CRRS", "CRMS", "EFTN",
		"TEAMICS", "ACUIRE", 
		"HIRIS", "Leave/TAS", "Payadmin","E-Payslip", "Data.Scan Online", "Auraportal", "AMLOCK", "KASPERSKY", "SOPHOS Firewall", "Microsoft Solutions", "MCCMS", "SFT", "EMS"
	);	
}