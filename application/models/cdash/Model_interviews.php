<?php

class Model_interviews extends CI_Model {
	
	public function __construct() {
    	parent::__construct();
		$this->load->model("../Model_core");   
		$this->load->model("cdash/Model_useractivity"); 
    }
	
	private function setInterviewMessage($user_data) {//Email Template
		return '<p><strong>Dear '.$user_data['first_name'].'</strong>,<br/><br/>Thank you for applying to DMS Software Engineering. <br/><br/>Your application for the '.$user_data['pos_name'].' position stood out to us and we would like to invite you for an interview at our office.</p><p>You will meet with <span class="person-name">'.$user_data['person_name'].'</span>. The interview will last about 60 minutes. You’ll have the chance to discuss the position’s responsibilities and learn more about our company.</p><p>We would like to conduct your interview <strong><span>'.$user_data['date_time'].'</span></strong>, please let me know your availability. I will schedule the interview once I receive your reply.</p><p>Our offices are located at <strong>380 Koswatta Road, Kalapaluwawa, Rajagiriya</strong> (<a href="https://goo.gl/maps/QdzUWTAMYj62">Location</a>). You can find an attached screenshot of our exact location. <br/>Please bring your NIC, so you can receive a visitor’s pass at the reception.</p>For furthur information please contact <span>'.$user_data['person_name'].'</span> on <span>'.$user_data['phone_number'].'</span>.<br/><br/><strong>Best Wishes</strong><br/>DMS Software Engineering';
	}	
	
	public function setInterview($data) {//Add Interview AJAX Method
		$db_query = "SELECT * FROM tbl_user_data WHERE user_id = ?";
		$db_query = $this->db->query($db_query, array($data['user_id']));
	
		if ($db_query->num_rows() != 1) {//Check for User
			return $this->Model_core->jsonResponse("false", "404", "No Such User Found");
		}		

		$db_query = "SELECT first_name FROM tbl_user_data WHERE user_id = ?";
		$db_query = $this->db->query($db_query, array($data['pos-manager']));
	
		if ($db_query->num_rows() != 1) {//Check for Position Manager
			return $this->Model_core->jsonResponse("false", "404", "No Such Position Manager Found");
		}		

		$db_query = $db_query->result_array()[0];
		
		$manager_name = $db_query['first_name'];
		
		$db_query = "SELECT p.pos_name, a.first_name FROM tbl_applications a, tbl_positions p WHERE a.pos_id = p.pos_id AND app_id = ?";
		$db_query = $this->db->query($db_query, array($data['app_id']));
	
		if ($db_query->num_rows() != 1) {//Check for Application
			return $this->Model_core->jsonResponse("false", "404", "No Such Application Found");
		}
		
		$db_query = $db_query->result_array()[0];
		
		$date_time = $data['int_date']." ".$data['int_time'].":00";
		
		$email_message = $this->setInterviewMessage(array(
			"pos_name" => $db_query['pos_name'],
			"date_time" => $date_time,
			"first_name" => $db_query['first_name'],
			"person_name" => $manager_name,
			"phone_number" => $data['mobile_number']
		));//Message for the Email
		
		$this->db->trans_begin();
		
		$db_query = "INSERT INTO tbl_interview VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)";
        $db_query = $this->db->query($db_query, array($data["app_id"], $data["user_id"], $date_time, $data['pos-manager'], $data["mobile_number"], 'Not Set', 'S'));
		
        if ($db_query == "1") {
			$pos_id = $this->db->insert_id();
			$this->db->set('reviewed', '1');
			$this->db->where('app_id', $data['app_id']); 
			
			if ($this->db->update('tbl_applications')) {			
				$this->db->trans_commit();				
				$this->Model_useractivity->recordUseractivity("Sent Interview Request to ".$data['app_id'].". Interview Id: ".$pos_id);
				return $this->Model_core->jsonResponse("true", "200", "Interview Request Sent to Applicant");	
			}
		}
		
		$this->db->trans_rollback();		
		return $this->Model_core->jsonResponse("false", "500", "Internal Server Error Occurred");
	}
	
	public function getApplicationInterviews($app_id) {//Get All Interviews for Applicant
		$query = "SELECT i.interview_id,u.first_name,u.user_id,i.interview_dt FROM tbl_interview i, tbl_applications a, tbl_user_data u WHERE i.app_id = a.app_id AND i.pos_manager = u.user_id AND a.app_id = ?";		
		$query = $this->db->query($query, array($app_id));		
		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$data[] = array(
					"person_name" => $row['first_name'],
					"interview_id" => $row['interview_id'],
					"interview_dt" => substr($row['interview_dt'], 0, strlen($row['interview_dt'])-3)
				);
			}		
			
			return $data;
		}
		
		return array();
	}
	
	public function getInterviews() {//Get All Applicants	
		$query = "SELECT i.interview_id, i.interview_dt, pm.first_name as person_name, i.interview_status, u.first_name, u.user_id, p.pos_name, p.pos_id FROM tbl_interview i, tbl_user_data u, tbl_user_data pm, tbl_positions p, tbl_applications a WHERE i.user_id = u.user_id AND pm.user_id = i.pos_manager AND a.app_id = i.app_id AND a.pos_id = p.pos_id ORDER BY i.interview_dt, i.interview_status DESC";
		
		$query = $this->db->query($query);		
		if ($query->num_rows() > 0) {
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$status = $row['interview_status'];
				switch($status){
					case "N":
						$status = "Cancelled";
						break;
					case "S":
						$status = "Scheduled";
						break;
					case "C":
						$status = "Completed";
						break;
					case "O":
						$status = "Ongoining";
						break;
				}
				
				$data[] = array(
					"pos_id" => $row['pos_id'],
					"user_id" => $row['user_id'],
					"position" => $row['pos_name'],
					"first_name" => $row['first_name'],
					"person_name" => $row['person_name'],
					"interview_id" => $row['interview_id'],
					"interview_dt" => $row['interview_dt'],
					"interview_status" => $status
				);
			}
			
			return $data;
		}
		
		return array();	
	}
	
	public function getInterview($int_id) {//Get Single Interview Data
		$query = "SELECT i.app_id, i.user_id, i.interview_dt, pm.first_name AS pos_man, pm.user_id AS pos_mid, i.person_con, i.interview_remarks, i.interview_status, u.first_name,  CONCAT(a.first_name, ' ', a.last_name ) AS app_name  FROM tbl_interview i, tbl_user_data u, tbl_user_data pm, tbl_applications a WHERE i.app_id = a.app_id AND i.user_id = u.user_id AND pm.user_id = i.pos_manager AND i.interview_id = ?";
		
		$query = $this->db->query($query, array($int_id));		
		if ($query->num_rows() > 0) {
			
			$position_manager = 
			
			$data = array();
			
			foreach ($query->result_array() as $row) {
				$status = $row['interview_status'];
				switch($status) {
					case "N":
						$status = "Cancelled";
						break;
					case "S":
						$status = "Scheduled";
						break;
					case "C":
						$status = "Completed";
						break;
					case "O":
						$status = "Ongoining";
						break;
				}
				
				$current_dt = date("Y-m-d H:i:s");
				$current_dt = explode(" ", $current_dt)[0]." 00:00:00";
				
				$interview_dt = explode(" ", $row['interview_dt'])[0]." 00:00:00";
				$interview_dt = date("Y-m-d H:i:s", strtotime($interview_dt ));
				
				$interview_today = '0';
				
				if ($interview_dt == $current_dt) {
					$interview_today = '1';
				} else if ($interview_dt < $current_dt) {
					$interview_today = '2';
				}
				
				$data[] = array(
					"int_id" => $int_id,
					"app_id" => $row['app_id'],
					"status" => $status,
					"user_id" => $row['user_id'],
					"app_name" => $row['app_name'],
					"first_name" => $row['first_name'],/* N - Cancelled, S - Scheduled, C - Completed, O - Ongoining */	
					"person_con" => $row['person_con'],		
					"person_name" => $row['pos_man'],	
					"interview_dt" => $row['interview_dt'],		
					"interview_today" => $interview_today,			
					"position_man_id" => $row['pos_mid'],	
					"interview_status" => $row['interview_status'],
					"interview_remarks" => $row['interview_remarks'],	
				);
			}
			
			return $data[0];
		}
		
		return array();	
	}
	
	public function changeInterviewState($data) {//Change Interview State
		$db_query = "SELECT * FROM tbl_interview WHERE interview_id = ?";
		$db_query = $this->db->query($db_query, array($data['int_id']));
	
		if ($db_query->num_rows() != 1) {
			return $this->Model_core->jsonResponse("false", "404", "No Such Interview Available");
		}
		
		$this->db->trans_begin();
		
		$this->db->set('interview_status', $data['state']);
		$this->db->set('interview_remarks', $data['remark']);
		$this->db->where('interview_id', $data['int_id']); 
		
		if ($this->db->update('tbl_interview')) {
			$this->db->trans_commit();
			$this->Model_useractivity->recordUseractivity("Changed Interview State of ".$data['int_id']." to ".$data['state']);
			return $this->Model_core->jsonResponse("true", "200", "Interview State Changed");
		}
		
		$this->db->trans_rollback();		
		return $this->Model_core->jsonResponse("false", "500", "Internal Server Error Occurred");
	}
	
	private function getEachStat($where, $today = true){//Retrieve Today and this week's interviews
		$db_query = "SELECT i.interview_id, i.app_id, CONCAT(a.first_name, ' ', a.last_name ) AS app_name, i.interview_dt, p.pos_name FROM tbl_interview i, tbl_user_data u, tbl_user_data pm, tbl_applications a, tbl_positions p WHERE i.app_id = a.app_id AND i.user_id = u.user_id AND pm.user_id = i.pos_manager AND a.pos_id = p.pos_id AND (i.interview_status = 'S' OR i.interview_status = 'O') AND i.interview_dt BETWEEN DATE(CURRENT_DATE()) AND $where ORDER BY i.interview_dt ASC";
		$db_query = $this->db->query($db_query);
		
		if ($db_query->num_rows() > 0) {	
			$data = array();
			foreach ($db_query->result_array() as $row) {
				$data[] = array(
					"int_id" => $row['interview_id'],
					"app_id" => $row['app_id'],
					"app_name" => $row['app_name'],
					"pos_name" => $row['pos_name'],
					"interview_dt" => "In ".$this->Model_core->time_elapsed_string($row['interview_dt'])."<br/>".date('g:i a', strtotime($row['interview_dt']))
				);
			}
			
			return $data;
		}
		
		return array();
	}
	public function interviewStatistic() {
		$today = $this->getEachStat('DATE(CURRENT_DATE()+1)');
		$week = $this->getEachStat('DATE_ADD(CURRENT_DATE(), INTERVAL 1 WEEK) AND i.interview_dt NOT BETWEEN DATE(CURRENT_DATE()) AND DATE(CURRENT_DATE()+1)', false);
		
		$db_query = "SELECT COUNT(*) as 'all', (SELECT COUNT(*) as 'month' FROM tbl_interview WHERE interview_dt BETWEEN DATE(CURRENT_DATE()) AND DATE_ADD(CURRENT_DATE(), INTERVAL 1 MONTH)) as 'month' FROM tbl_interview WHERE interview_status = 'S' OR interview_status = 'O'";
		$db_query = $this->db->query($db_query)->result_array()[0];//Total Interviews and This Months
		
		return array(
			"all" => $db_query['all'],
			"week" => $week,
			"today" => $today,
			"month" => $db_query['month']
		);
	}
}