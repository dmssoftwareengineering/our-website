<?php

class Model_permission extends CI_Model {
	
	public function __construct() {
    	parent::__construct();
		$this->load->model("../Model_core");   
		$this->load->model("../Model_dashboard");   
    }
	
	public function checkAccess($mod_perms) {
		if (!$this->Model_dashboard->user_in()) {//User Not Signed In
			return false;
		} else {//Check Access for the SignedIn User
			$role_id = $this->session->userdata('role_id');
			$mod_length = sizeof($mod_perms);
			
			foreach ($mod_perms as $key => $val) {				
				$user_perms = $this->session->userdata('role_permission');	
				
				if (!array_key_exists($key, $user_perms)) return false;
				
				$user_perms = $user_perms[$key];	
				
				$user_perms_length = sizeof($val);		
				
				$req_perms = $mod_perms[$key];
				$req_perms_length = sizeof($req_perms);		
				
				if ($user_perms == null) return false;
				
				for ($i=0;$i<$req_perms_length;$i++) {
					$_status = in_array($req_perms[$i], $user_perms);
					$_status = $_status >= 1 ? true : false;
					
					if (!$_status) return false;
				}
			}
			
			return true;
		}
	}
}