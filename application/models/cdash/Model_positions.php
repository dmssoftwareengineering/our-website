<?php

class Model_positions extends CI_Model {
	
	public function __construct() {
    	parent::__construct();
		$this->load->model("../Model_core");   
    }
	
	public function overall_resume_recruitment() {
		$query = "SELECT p.pos_name, p.pos_id FROM tbl_applications a, tbl_positions p WHERE a.pos_id = p.pos_id AND (a.date_created >= DATE_FORMAT(NOW() ,'%Y-%m-01') AND a.date_created <= DATE_FORMAT(NOW() ,'%Y-%m-31'))";		
		$query = $this->db->query($query);
		
		$recuited = $position_name = $resumes = "";
		
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$sub_query = "SELECT a.app_id FROM tbl_applications a, tbl_positions p WHERE a.pos_id = p.pos_id AND (a.date_created >= DATE_FORMAT(NOW() ,'%Y-%m-01') AND a.date_created <= DATE_FORMAT(NOW() ,'%Y-%m-31')) AND a.reviewed = '3' AND p.pos_id = ?";		
				$sub_query = $this->db->query($sub_query, array($row['pos_id']));
				
				$recuited = $recuited.$sub_query->num_rows().",";		

				$sub_query = "SELECT a.app_id FROM tbl_applications a, tbl_positions p WHERE a.pos_id = p.pos_id AND (a.date_created >= DATE_FORMAT(NOW() ,'%Y-%m-01') AND a.date_created <= DATE_FORMAT(NOW() ,'%Y-%m-31')) AND a.reviewed != '3' AND p.pos_id = ?";		
				$sub_query = $this->db->query($sub_query, array($row['pos_id']));
				
				$resumes = $resumes.$sub_query->num_rows().",";		
				$position_name = $position_name."'".$row['pos_name']."',";
			}
			
			$position_name = rtrim($position_name, ",");
			$recuited = rtrim($recuited, ",");
			$resumes = rtrim($resumes, ",");
			return array(
				"resumes" => $resumes,
				"positions" => $position_name,
				"recruited" => $recuited
			);
		}
		
		return array(
			"resumes" => 0,
			"positions" => " ",
			"recruited" => 0
		);

	}	
	
	public function resumes_open_position() {
		$query = "SELECT p.pos_name, p.pos_id FROM tbl_applications a, tbl_positions p WHERE a.pos_id = p.pos_id AND (a.date_created >= DATE_FORMAT(NOW() ,'%Y-%m-01') AND a.date_created <= DATE_FORMAT(NOW() ,'%Y-%m-31'))";		
		$query = $this->db->query($query);
		
		$position_name = $resumes = "";
		
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$sub_query = "SELECT a.app_id FROM tbl_applications a, tbl_positions p WHERE a.pos_id = p.pos_id AND (a.date_created >= DATE_FORMAT(NOW() ,'%Y-%m-01') AND a.date_created <= DATE_FORMAT(NOW() ,'%Y-%m-31')) AND a.reviewed = '0' AND p.pos_id = ?";		
				$sub_query = $this->db->query($sub_query, array($row['pos_id']));
				
				$resumes = $resumes.$sub_query->num_rows().",";		
				$position_name = $position_name."'".$row['pos_name']."',";
			}
			
			$position_name = rtrim($position_name, ",");
			$resumes = rtrim($resumes, ",");
			
			return array(
				"resumes" => $resumes,
				"positions" => $position_name
			);
		}
		
		return array(			
			"resumes" => 0,
			"positions" => " "
		);		
	}	
}