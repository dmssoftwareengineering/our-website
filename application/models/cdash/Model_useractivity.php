<?php

class Model_useractivity extends CI_Model {
	
	public function __construct() {
    	parent::__construct();
		$this->load->model("../Model_core");   
    }
	
	public function recordUseractivity($activity) {
		$ip_address = $this->Model_core->getClientIP();
		$user_id = $this->session->userdata('user_id');
		
		$this->db->trans_begin();
		
		$query = "INSERT INTO tbl_user_activity VALUES (NULL, ?, ?, ?, CURRENT_TIMESTAMP)";
        $query = $this->db->query($query, array($user_id, $activity, $ip_address));
		
        if ($query == "1") {
			$this->db->trans_commit();
			return true;
		}
		
		$this->db->trans_rollback();		
		return false;
	}

	public function getUserActivity($user_id) {
		$query = "SELECT * FROM tbl_user_activity WHERE user_id = ? ORDER BY time_created DESC";
		$query = $this->db->query($query, array($user_id));	
		
		$data = array();

		if ($query->num_rows() > 0) {			
			foreach ($query->result_array() as $row) {	
				$data[] = array(
					"activity" => $row['activity'],
					"ip_address" => $row['ip_address'],
					"time_created" => $row['time_created']
				);
			}
		}

		return $data;
	}
}