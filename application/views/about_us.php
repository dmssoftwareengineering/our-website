<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "About Us | DMS Software Engineering";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);		
   ?>
   
<style> .team-member{float:left;position:relative;width:5%}.team-member:nth-child(even){background-color:#0070c0}.member-image{display:block;width:100%;-webkit-filter:grayscale(100%);filter:grayscale(100%);max-height:220px}.team-overlay{position:absolute;bottom:0;left:0;right:0;top:100%;background-color:#0070c0;overflow:hidden;width:0;height:100%;transition:.5s ease;z-index:9999}.team-member:hover .team-overlay{width:166%;height:166%;left:-47px;top:-84px}.team-member:hover .member-image{-webkit-filter:grayscale(0%);filter:grayscale(0%)}.manager{background:#ffd700;padding-left:10px;padding-right:10px;color:#000;font-weight:700}.person-text{background:#0070c0;padding-left:10px;padding-right:10px}.person-data{color:#fff;font-size:15px;text-align:center;position:absolute;top:85%;left:50%;transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);white-space:nowrap;width:100%}.team-holder{width:100%;height:165px}h3 small{color:#666;font-size:10px;letter-spacing:1.4px;text-transform:uppercase;font-weight:400;display:block;margin-top:3px}.no-margin-bt{margin-bottom:0}.margin-tp-30{margin-top:30px;margin-bottom:0}.holder{background:#0070c0;margin:auto}.vertical-holder{width:1px;height:30px;margin-bottom:-8px}.horizontal-holder{width:112px;height:1px}.holder-width-100{width:100%}.company{font-size:16px;color:#0070c0;margin-top:10px}.grp-title{margin:20px 0}.holder-border-top{border-top:#0070c0 solid 1px}.com-seperator{height:50px;margin-bottom:-30px}.main-image{height:463px;-webkit-filter:grayscale(100%);filter:grayscale(100%);transition:.5s ease-out}.main-image:hover{height:463px;-webkit-filter:grayscale(0%);filter:grayscale(0%);transform:translate(0%,0%);-ms-transform:translate(0%,0%)}.middle-team{margin:0 44%;text-align:center}.team-m-l1{width:17%}.features-box{float:left;margin-bottom:10px;padding:0}.features-box img{height:245px;width:190px;display:block}.features-box .features-icon{margin-bottom:13px}.features-box h3{font-size:20px;margin-bottom:-7px}.features-box .tw-readmore{color:#2f2c2c;font-size:12px;line-height:13px;margin-top:10px}.first-row{margin-left:40%}.second-row{margin-left:16%}.our-com{color:#000;background:#ffd700;padding:6px 2px;-webkit-box-shadow:0 0 20px 0 #ffd700;box-shadow:0 0 20px 0 #ffd700}.features-box:hover{-webkit-box-shadow:0 -5px 7px 0 #ffd700;box-shadow:0 -5px 7px 0 #ffd700}.family{margin-bottom:50px}.managed{margin-bottom:50px;margin-top:22px}.info-dms{padding:30px;background:rgba(255,255,255,0.9)}.main-container{background:url('<?php echo base_url('assets/images/about/our-location.jpg');?>');background-repeat:no-repeat;background-size:100%;transition:.5s ease-out;-webkit-filter:grayscale(100%);filter:grayscale(100%);background-position:center}.main-container:hover{-webkit-filter:grayscale(0%);filter:grayscale(0%)}.award-icon img{max-height:125px;max-width:125px}@media (max-width:1200px){.features-box img{height:195px;width:160px}}@media (min-width:768px) and (max-width:991px){.first-row{margin-left:27%}.second-row{margin-left:5%}.features-box img{height:195px;width:160px}}@media (max-width:767px){.features-box img{height:155px;width:140px}}@media (min-width:770px) and (max-width:962px){.team-member{width:20%}.person-data{font-size:13px}}@media (min-width:520px) and (max-width:770px){.team-member{width:20%}.person-data{font-size:12px}}@media (min-width:430px) and (max-width:520px){.team-member{width:20%}.person-data{font-size:12px}}@media (min-width:328px) and (max-width:430px){.team-member{width:20%}.person-data{font-size:9px}}@media (min-width:320px) and (max-width:548px){.first-row{margin-left:90px}.second-row{margin-left:0}.features-box img{width:165px}}@media (min-width:548px) and (max-width:548px){.first-row{margin-left:20%}.second-row{margin-left:-10%}}.banner-area{color:#757575}@media (min-width:320px) and (max-width:760px){.banner-area{height:220px}.banner-heading.case-banner-heading{min-height:230px}.banner-heading .banner-case-title{font-size:30px}.team-member{width:24%}.team-holder{height:285px}.main-container{background:none}section{padding-top:0}.team-overlay{display:none}}.award-icos{width:100px}.award-icos *{font-size:15px}.clients-carousel .owl-item img{width:auto;max-height:110px;margin:0 auto;max-width:240px}.banner-case-title span{color:#049bda}</style>
</head>

<body>
   <?php $this->load->view('inc/header'); ?>
   
   <div id="banner-area" class="banner-area bg-overlay case-bg-overlay" style="background-image:url(<?php echo base_url('assets/images/about/about-bg.png'); ?>">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading case-banner-heading">
                  <h1 class="banner-case-title">Together we <span>achieve</span> more</h1>
               </div>
            </div>
         </div>
      </div>
   </div>
   
   <section class="main-container" data-aos="fade-left" data-aos-once="false">
      <div class="container">
         <div class="row">
            <div class="col-md-6 align-self-md-center">
               
            </div>
            <div class="col-md-6 info-dms">
               <div class="tw-about-bin">
                  <h2 class="column-title">
					<small>Not Colleagues, We are Family</small>
					DMS <span>Software Engineering</span>
				  </h2>
                  <span class="animate-border tw-mb-40 tw-mt-20"></span>
                  <p>Founded in 1977 as the software division of Data Management Systems Ltd, DMS Software Engineering (Pvt) Ltd. was incorporated as a private limited liability company in Sri Lanka in 1992. DMS Software Engineering is a member of the DMS group of companies.</p>
				  
				  <p>The company has staff strength of over 100 persons in different disciplines. A pioneer in the field of Packaged & Bespoke Software development in Sri Lanka, DMS software solutions are used by over 150 companies in Sri Lanka and in overseas.</p>
				  
				  <p>DMS Software Engineering, a Microsoft Gold Certified Partner is one of the well experienced and pioneered software development and consultancy organizations providing comprehensive solutions to its clients. The company specializes in offering Software/Web Application Development, Rich Internet Application Development, System Integration, Software Testing, Quality Assurance services, and more. In addition, DMS Software Engineering has industry-specific software expertise in Banking & Financial, Manufactures, Tea, and other diversified sectors.</p>
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="tw-mission">
      <div class="container">
         <div class="row" data-aos="fade-right" data-aos-once="false">
            <div class="col-md-12">
				<div class="row">
                  <div class="col-md-5">
					<div class="mission-body">
					   <div class="mission-title tw-mb-40">
						  <h2 class="column-title">Our Dream</h2>
						  <span class="animate-border bg-white border-orange tw-mt-30"></span>
					   </div>
					   <p>
						  To be the leading software development and business process outsourcing company in<br/> Sri Lanka.
					   </p>
					</div>
                  </div>
                  <div class="col-md-7">
					<div class="mission-body">
					   <div class="mission-title tw-mb-40">
						  <h2 class="column-title">Our Purpose</h2>
						  <span class="animate-border bg-white border-orange tw-mt-30"></span>
					   </div>
					   <p>
						  Provide high quality solutions on latest technologies while being sensitive to market requirements and enhancing the synergy across the group through satisfied staff to delight our valued customers.
					   </p>
					</div>
                  </div>
                    <div class="col-md-4" hidden>
                        <div class="mission-body">
                           <div class="mission-title tw-mb-40">
                              <h2 class="column-title">Our Way</h2>
                              <span class="animate-border bg-white border-orange tw-mt-30"></span>
                           </div>
                           <p>
                              We are a company with a conscience presenting you with endless opportunities utilizing next generation IT and consultancy services.
                           </p>
                        </div>					 
                  </div>
                </div>
            </div>
         </div>
      </div>
   </section>
   
   <section class="tw-awards">
      <div class="container">
         <div class="row wow fadeInLeft" data-aos="fade-left" data-aos-once="false">
            <div class="col text-center">			   
			   <div class="section-heading margin-tp-30">
                  <h2>
					<small>We are a part of</small>
                    <span>DMS Holdings</span>
                    <small>(Founded in 1977)</small>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
					<div class="holder vertical-holder"></div>
                  </h2>
				  <div class="row holder-border-top wow fadeInLeft">
					<div class="col-md-7 no-padding">						
						<div class="holder vertical-holder"></div>
						<h3>
							<div class="grp-title">Information<br/>Technology</div>
							<div class="holder vertical-holder"></div>
						</h3>
						
						<div class="row">							
							<div class="col-md-6">				
								<div class="holder horizontal-holder holder-width-100"></div>
								<div class="holder vertical-holder "></div>
								<h3>
									<div class="grp-title">IT Systems<br/>Sector</div>
									<div class="holder vertical-holder com-seperator"></div>
									<div class="holder horizontal-holder holder-width-100"></div>
								</h3>	
								<div class="row">
									<div class="col-md-6">
										<h3>
											<div class="company">Data Management Systems Ltd </div>
											<small>(Founded in 1977)</small>
										</h3>		
									</div>
									<div class="col-md-6">
										<h3>
											<div class="company">DMS Electronics Ltd </div>
											<small>(Founded in 1981)</small>
										</h3>		
									</div>
								</div>	
							</div>	
							<div class="col-md-6">				
								<div class="holder horizontal-holder holder-width-100"></div>
								<div class="holder vertical-holder"></div>
								<h3>
									<div class="grp-title">Software<br/>Sector</div>
									<div class="holder vertical-holder com-seperator"></div>
									<div class="holder horizontal-holder holder-width-100"></div>
								</h3>		
								<div class="row">
									<div class="col-md-6">
										<h3>
											<div class="company our-com">DMS Software Engineering (Pvt) Ltd </div>
											<small>(Founded in 1983)</small>
										</h3>		
									</div>
									<div class="col-md-6">
										<h3>
											<div class="company">DMS Software Technologies (Pvt) Ltd </div>
											<small>(Founded in 1988)</small>
										</h3>		
									</div>
								</div>
							</div>	
						</div>
					</div>
					<div class="col-md-5 ">
						<div class="holder vertical-holder" style="margin-bottom:22px"></div>
						<h3>
							<div class="grp-title">Garments<br/>Technology</div>
							<div class="holder vertical-holder com-seperator"></div>
							<div class="holder horizontal-holder holder-width-100"></div>
						</h3>
						
						<h3>
							<div class="grp-title">Apparel<br/>Sector</div>
							<div class="holder vertical-holder com-seperator" style="margin-bottom:-8px"></div>
							<div class="holder horizontal-holder holder-width-100"></div>
						</h3>
						
						<div class="row">
							<div class="col-md-12">
								<h3>
									<div class="company">DMS Garment Technologies (Pvt) Ltd </div>
									<small>(Founded in 1983)</small>
								</h3>		
							</div>							
						</div>
					</div>
				  </div>
               </div>
            </div>
         </div>
      </div>
   </section>   

   <!--<section class="tw-expert" hidden>
      <div class="container">
		<div class="row wow fadeInDown">
            <div class="col text-center">
               <div class="section-heading tw-mb-80">
                  <h2>
                     <small>Journey of DMS Software Engineering</small>
                     Our Journey
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">			
               <div class="timeline-wrapper">
                  <div class="row wow fadeInDown">
                     <div class="col-md-6 timeline-item left-part">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                           <p class="details">Our promising journey started on 08 February 2010 by Leonardo Decaprio, the CEO and Founder of
                              the SeoBin
                           </p>
                        </div>
                     </div>
                     <div class="col-md-6 timeline-item ">
                        <div class="timeline-date left-part">
                           <p class="title">2010</p>
                           <p class="tagline">we are featured</p>
                        </div>
                     </div>
                  </div>
                  <div class="row wow fadeInDown">
                     <div class="col-md-6 timeline-item">
                        <div class="timeline-date active">
                           <p class="title">2010</p>
                           <p class="tagline">we are featured</p>
                        </div>
                     </div>
                     <div class="col-md-6 timeline-item">
                        <div class="timeline-badge active"></div>
                        <div class="timeline-panel">
                           <p class="details">Our promising journey started on 08 February 2010 by Leonardo Decaprio, the CEO and Founder of
                              the SeoBin
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="row wow fadeInDown">
                     <div class="col-md-6 timeline-item left-part">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                           <p class="details">Our promising journey started on 08 February 2010 by Leonardo Decaprio, the CEO and Founder of
                              the SeoBin
                           </p>
                        </div>
                     </div>
                     <div class="col-md-6 timeline-item ">
                        <div class="timeline-date left-part">
                           <p class="title">2010</p>
                           <p class="tagline">we are featured</p>
                        </div>
                     </div>
                  </div>
                  <div class="row wow fadeInDown">
                     <div class="col-md-6 timeline-item">
                        <div class="timeline-date">
                           <p class="title">2010</p>
                           <p class="tagline">we are featured</p>
                        </div>
                     </div>
                     <div class="col-md-6 timeline-item">
                        <div class="timeline-badge"></div>
                        <div class="timeline-panel">
                           <p class="details">Our promising journey started on 08 February 2010 by Leonardo Decaprio, the CEO and Founder of
                              the SeoBin
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>-->

   <section id="our-team" class="tw-expert">
      <div class="container">
         <div class="row" data-aos="fade-left" data-aos-once="false">
            <div class="col text-center">
               <div class="section-heading family">
                  <h2>
                     <small>People who make the magic happen</small>
                     Our <span>Family</span>
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row" data-aos="fade-left" data-aos-once="false">
            <div class="col-12">
				<p class="text-center">DMS Software Engineering is always more than just a work place its the place where you build companionships for life, our energetic and knowledgable fun loving teams ensure we accomplish our tasks with the utmost creativity and performance.</p>
			</div>
		 </div>		 
		 
		 <div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col-12">	
				<div class="managed text-center">
                  <h2>
                     Managed By
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
			   
				<div class="features-box first-row">
				  <div class="features-icon d-table">
					 <div class="features-icon-inner d-table-cell">
						<img src="<?php echo base_url('assets/images/about/Picture1.png');?>" alt="">
					 </div>
				  </div>
				  <div><h3>Piyal De Silva</h3></div>
				  <div class="tw-readmore">Director/General Manager</div>
			   </div>
			</div>
		 </div>
		 <div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col-12">			   
			   <div class="features-box first-row">
				  <div class="features-icon d-table">
					 <div class="features-icon-inner d-table-cell">
						<img src="<?php echo base_url('assets/images/about/Picture2.png');?>" alt="">
					 </div>
				  </div>
				  <div><h3>T. Baheerathan</h3></div>
				  <div class="tw-readmore">General Manager</div>
			   </div>
			</div>
		 </div>	 
		 
		<div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col-12">				   
			   <div class="features-box second-row">
				  <div class="features-icon d-table">
					 <div class="features-icon-inner d-table-cell">
						<img src="<?php echo base_url('assets/images/about/Picture3.png');?>" alt="">
					 </div>
				  </div>
				  <div><h3>Manju<br/>Welivitage</h3></div>
				  <div class="tw-readmore">Manager<br/>BPO Services</div>
			   </div>
			   
			   <div class="features-box">
				  <div class="features-icon d-table">
					 <div class="features-icon-inner d-table-cell">
						<img src="<?php echo base_url('assets/images/about/Picture4.png');?>" alt="">
					 </div>
				  </div>
				  <div><h3>Yoginda<br/>Suraweera</h3></div>
				  <div class="tw-readmore">Manager<br/>Commercial Solutions</div>
			   </div>
			   
			   <div class="features-box">
				  <div class="features-icon d-table">
					 <div class="features-icon-inner d-table-cell">
						<img src="<?php echo base_url('assets/images/about/Picture5.png');?>" alt="">
					 </div>
				  </div>
				  <div><h3>Amal<br/>Roshantha</h3></div>
				  <div class="tw-readmore">Manager<br/>Banking & Financial Solutions</div>
			   </div>
			   
			   <div class="features-box">
				  <div class="features-icon d-table">
					 <div class="features-icon-inner d-table-cell">
						<img src="<?php echo base_url('assets/images/about/Picture6.png');?>" alt="">
					 </div>
				  </div>
				  <div><h3>Vijayaruban<br/>Nadarajah</h3></div>
				  <div class="tw-readmore">Manager<br/>Business Development</div>
			   </div>
			</div>
		</div>
				  
		
      </div>
   </section>  

	<div class="banner-area bg-overlay case-bg-overlay">
      <div class="" hidden>
         <div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col-12">
				<div class="team-holder">
					<div class="team-member">
					  <img src="<?php echo base_url('assets/core/images/team/expert3.png');?>" alt="Avatar" class="member-image">
					  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert3.png');?>');background-size:100%">
						<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
					  </div>
					</div>
					<div class="team-member">
					  <img src="<?php echo base_url('assets/core/images/team/expert4.png');?>" alt="Avatar" class="member-image">
					  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert4.png');?>');background-size:100%">
						<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
					  </div>
					</div>
					<div class="team-member">
					  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
					  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
						<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
					  </div>
					</div>
					<div class="team-member">
					  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
					  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
						<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
					  </div>
					</div>
					<div class="team-member">
					  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
					  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
						<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
					  </div>
					</div>
					<div class="team-member">
					  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
					  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
						<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
					  </div>
					</div>
					<div class="team-member">
					  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
					  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
						<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
					  </div>
					</div>
					<div class="team-member">
					  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
					  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
						<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
					  </div>
					</div>
					<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
				<div class="team-member">
				  <img src="<?php echo base_url('assets/core/images/team/expert2.png');?>" alt="Avatar" class="member-image">
				  <div class="team-overlay" style="background:url('<?php echo base_url('assets/core/images/team/expert2.png');?>');background-size:100%">
					<div class="person-data"><span class="person-text">Firstname Lastname</span></div>
				  </div>
				</div>
			</div>			
         </div>
      </div>
   </div>
   
   <section id="our-awards" class="tw-about-award">
      <div class="container">
		<div class="row" data-aos="fade-right" data-aos-once="false">
            <div class="col text-center">
               <div class="section-heading tw-mb-80">
                  <h2>
                     <small>Things we achieved together</small>
                     Our <span>Awards</span>
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
		 
		 <div class="row wow fadeInUp" data-aos="fade-up" data-aos-once="false">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/kaspersky_c_2018.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/corporate_top_10_2014.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/kaspersky_c_2009.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_gold_c_2009.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_cust.png');?>" alt="">
                     </div>
                  </div>				  
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_busi.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_dms.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_dms.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_gold_c_2008.png');?>" alt="">
                     </div>
                  </div>				  
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_gold_c_2007.png');?>" alt="">
                     </div>
                  </div>				  
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_gold_c_2006.png');?>" alt="">
                     </div>
                  </div>			  
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/awards/ms_partner_2007.png');?>" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div> 
      </div>
   </section>

   <section class="tw-client">
      <div class="container">
		<div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col text-center">
               <div class="section-heading tw-mb-80">
                  <h2>
                     Our Trusted <span>Partners</span>
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row wow fadeInUp" data-aos="fade-up" data-aos-once="false">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/partners/3i.jpg');?>" alt="" height="80px">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/partners/aura.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/partners/microsoft.jpg');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/partners/kaspersky.jpg');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/partners/sophos.jpg');?>" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>   

   <?php
	$this->load->view('inc/footer');		
   ?>
</body>
</html>