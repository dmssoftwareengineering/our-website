<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from html.creativegigs.net/aproch/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Oct 2018 05:48:54 GMT -->
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>ADNIC | Your reliable insurance</title>
		
		<meta name="author" content="Basura Ratnayake">	
		<meta name="copyright" content="Copyright 2018 DMS Software Engineering. All Rights Reserved."/>

		<!-- SEO Start -->
		<meta name="description" content="Some Description">
		<meta name="keywords" content="Some Keywords" />

		<meta property="og:type" content="website" />
		<meta property="og:url" content="https://www.adnic.ae/" />
		<meta property="og:title" content="Abu Dhabi National Insurance Company" />	
		
		<meta property="og:image" content="images/logo/logo.png">
		<meta property="og:site_name" content="DMS Software Engineering" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:description" content="Some Description" />	
		<meta property="fb:pages" content="201829626513462" />

		<link rel="canonical" href="https://www.adnic.ae/" />
		
		<script type='application/ld+json'>{"@context":"http://schema.org","@type":"Organization","url":"https://www.adnic.ae/","@id":"#organization","name":"Abu Dhabi National Insurance Company","logo":"images/logo/logo.png","contactPoint":[{"@type":"ContactPoint","email":"mailto:info@dmsswe.com","telephone":"+94-11-287-6700","contactType":"Information"}]}</script>

		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="images/logo/favicon.png">		
		
		<link rel="stylesheet" type="text/css" href="plugins/bootstrap/css/bootstrap.min.css">		
		<link rel="stylesheet" type="text/css" href="plugins/select2-master/dist/css/select2.min.css">		
		<link rel="stylesheet" type="text/css" href="plugins/Camera-master/css/camera.css">		
		<link rel="stylesheet" type="text/css" href="plugins/bootstrap-mega-menu/css/menu.css">		
		<link rel="stylesheet" type="text/css" href="core/fonts/font-awesome/css/font-awesome.min.css">		
		<link rel="stylesheet" type="text/css" href="plugins/owl-carousel/owl.carousel.css">		
		<link rel="stylesheet" type="text/css" href="plugins/WOW-master/css/libs/animate.css">		
		<link rel="stylesheet" type="text/css" href="core/fonts/icon/font/flaticon.css">		
		<link rel="stylesheet" type="text/css" href="plugins/fancybox/dist/jquery.fancybox.min.css">		
		<link rel="stylesheet" type="text/css" href="plugins/sanzzy-map/dist/snazzy-info-window.min.css">		
		<link rel="stylesheet" type="text/css" href="core/css/style.css">		
		<link rel="stylesheet" type="text/css" href="core/css/responsive.css">


		<!-- Fix Internet Explorer ______________________________________-->

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif]-->

			
	</head>

	<body>
		<div class="search-box tran5s" id="searchWrapper">
	   		<div class="container">
	   			<form action="#">
	   				<div id="close-button" class="tran3s"></div>
	   				<div class="input-wrapper">
	   					<input type="text" placeholder="Search....">
	   					<button class="tran3s"><i class="fa fa-search" aria-hidden="true"></i></button>
	   				</div>
	   			</form>
	   		</div>
	   	</div> <!-- /.search-box -->

		<div class="main-page-wrapper">

			<!-- ===================================================
				Loading Transition
			==================================================== -->
			<div id="loader-wrapper">
				<div id="loader"></div>
			</div>



			<!-- 
			=============================================
				Theme Header
			============================================== 
			-->
			<header class="theme-menu-wrapper">
				<div class="container">
					<div class="top-header clearfix">
						<div class="float-left greeting-text"><span>Hi,</span> <p class="greeting"></p> !</div>
						<ul class="float-right">
							<li>call us &nbsp;<a href="#" class="tran3s">971 800 8040</a></li>
							<li><a href="https://www.facebook.com/adnic.ae" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#" class="tran3s"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCTHWh0X6NnFaTZHhQi5x5dA" class="tran3s"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
						</ul>
					</div> <!-- /.top-header -->
					<div class="main-header-menu-wrapper clearfix">
						<!-- Logo -->
						<div class="logo float-left"><a href="index-2.html"><img src="images/logo/logo.png" alt="Logo"></a></div>

						<nav class="navbar-expand-lg float-right navbar-light" id="mega-menu-wrapper">
					    	<button class="navbar-toggler float-right clearfix" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					    		<i class="flaticon-menu-options"></i>
					    	</button>
					    	<div class="collapse navbar-collapse clearfix" id="navbarNav">
					    	  <ul class="navbar-nav nav">
								<li class="nav-item dot-fix">
					    	    	<a class="nav-link" href="index.html">Home</a>
					    	    </li>
								
					    	    <li class="nav-item dropdown-holder  active">
					    	    	<a class="nav-link" href="#">About</a>
					    	    	<ul class="sub-menu">
										<li><a href="about-us.html">ADNIC</a></li>
										<li><a href="#">Our People</a></li>
										<li><a href="#">Corporate</a></li>
										<li><a href="#">Governance</a></li>
										<li><a href="#service-details.html">Investor Relations</a></li>
										<li class="login"><a href="#index-3.html">Annual Reports</a></li>
									</ul>
					    	    </li>
					    	    <li class="nav-item dropdown-holder">
					    	    	<a class="nav-link" href="#">Products</a>
					    	    	<ul class="sub-menu">
					    	    		<li><a href="#portfolio-v1.html">Individual</a></li>
										<li><a href="#portfolio-v4.html">Corporate</a></li>
										<li><a href="#portfolio-v2.html">Reinsurance</a></li>
										<li><a href="#portfolio-v3.html">Bancassurance</a></li>
										<li><a href="#portfolio-details.html">Affinity Programs</a></li>
										<li class="login"><a href="#index-3.html">Customer Care</a></li>
									</ul>
					    	    </li>
								<li class="nav-item dropdown-holder">
					    	    	<a class="nav-link" href="#">Services</a>
					    	    	<ul class="sub-menu">
					    	    		<li><a href="#portfolio-v1.html">Mobility</a></li>
										<li><a href="#portfolio-v4.html">Risk Engineering</a></li>
										<li><a href="#portfolio-v2.html">Circulars</a></li>
										<li><a href="#portfolio-v3.html">Priority</a></li>
										<li><a href="#portfolio-details.html">Assist America</a></li>
										<li class="login"><a href="#index-3.html">Claims</a></li>
									</ul>
					    	    </li>
					    	    <li class="nav-item dropdown-holder">
					    	    	<a class="nav-link" href="#">Help & Support</a>
					    	    	<ul class="sub-menu">
					    	    		<li><a href="#portfolio-v1.html">Insurance Calculations</a></li>
										<li><a href="#portfolio-v4.html">Branch Network</a></li>
										<li><a href="#portfolio-v2.html">Customer Service</a></li>
										<li><a href="#portfolio-v3.html">Contact Us</a></li>
									</ul>
					    	    </li>
					    	    <li class="nav-item search-button"><button class="search b-p-bg-color" id="search-button"><i class="fa fa-search" aria-hidden="true"></i></button></li>
					    	  </ul>
					    	</div>
						</nav>
					</div>
				</div>
			</header>

			<div id="different-holder">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div id="fs-min" class="col-lg-4 btn-dh-s">
									A<span class="sup">-</span>
								</div>
								<div id="fs-norm" class="col-lg-4 btn-dh-s">
									A<span class="sup">n</span>
								</div>
								<div id="fs-plus" class="col-lg-4 btn-dh-s">
									A<span class="sup">+</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 btn-dh-s">
									Text to Speech <i class="fa fa-comment"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="inner-banner">
				<div class="overlay">
					<div class="container">
						<h5>About us</h5>
						<h2>Company Story</h2>
					</div> <!-- /.container -->
				</div> <!-- /.overlay -->
			</div> <!-- /.inner-banner -->

			
			
			<!-- 
			=============================================
				About us 
			============================================== 
			-->
			<div class="about-us-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-12">
							<h2 class="title">We’re here to <br>help you <br> when you need  your financial support</h2>
						</div> <!-- /.col- -->
						<div class="col-lg-7 col-12">
							<div class="text-wrapper">
								<h4>About us</h4>
								<h5>Insight loan advisors is completely independent loan advising service and our directory of lenders gives you all the information lorem ipsums sitamet</h5>
								<p>Vestibulum condimentum neque at interdum dignissim. Integer colutpat vel lorem ac fringilla. Vestibulum porttitor euismod udiam viverra euismod non hendrerit eros.Insight loan advisors is completely ndependent loan advising service and our directory of lenders gives you all the information lorem</p>
							</div> <!-- /.text-wrapper -->
						</div> <!-- /.col- -->
					</div> <!-- /.row -->
					<div class="row image-gallery wow fadeInUp">
						<div class="col-md-8 col-12"><img src="images/home/4.jpg" alt=""></div>
						<div class="col-md-4 col-12">
							<img src="images/home/5.jpg" alt="">
							<img src="images/home/6.jpg" alt="">
						</div>
					</div> <!-- /.image-gallery -->
				</div> <!-- /.container -->
			</div> <!-- /.about-us-section -->


			<!-- 
			=============================================
				Our Team
			============================================== 
			-->
			<div class="our-team">
				<div class="container">
					<div class="theme-title">
						<h2>Our Dedicated Team</h2>
						<a href="team.html">See all </a>
					</div> <!-- /.theme-title -->
					<div class="row">
						<div class="col-lg-6 col-12">
							<div class="single-team-member clearfix">
								<div class="image-box float-left">
									<img src="images/team/1.jpg" alt="">
									<div class="opacity">
										<ul>
											<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
										</ul>
									</div> <!-- /.opacity -->
								</div> <!-- /.image-box -->
								<div class="text-wrapper float-left">
									<h6 class="name">Shane Yuen </h6>
									<span>CEO, Canny Digital</span>
									<p>“Success is making our clients succeed. Nothing else matters.”</p>
									<img src="images/home/sign.png" alt="">
								</div><!--  /.text-wrapper -->
							</div> <!-- /.single-team-member -->
						</div> <!-- /.col- -->
						<div class="col-lg-6 col-12">
							<div class="single-team-member clearfix">
								<div class="image-box float-left">
									<img src="images/team/2.jpg" alt="">
									<div class="opacity">
										<ul>
											<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
										</ul>
									</div> <!-- /.opacity -->
								</div> <!-- /.image-box -->
								<div class="text-wrapper float-left">
									<h6 class="name">Shane Yuen </h6>
									<span>CEO, Canny Digital</span>
									<p>“Success is making our clients succeed. Nothing else matters.”</p>
									<img src="images/home/sign.png" alt="">
								</div><!--  /.text-wrapper -->
							</div> <!-- /.single-team-member -->
						</div> <!-- /.col- -->
						<div class="col-lg-6 col-12">
							<div class="single-team-member clearfix">
								<div class="image-box float-left">
									<img src="images/team/3.jpg" alt="">
									<div class="opacity">
										<ul>
											<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
										</ul>
									</div> <!-- /.opacity -->
								</div> <!-- /.image-box -->
								<div class="text-wrapper float-left">
									<h6 class="name">Shane Yuen </h6>
									<span>CEO, Canny Digital</span>
									<p>“Success is making our clients succeed. Nothing else matters.”</p>
									<img src="images/home/sign.png" alt="">
								</div><!--  /.text-wrapper -->
							</div> <!-- /.single-team-member -->
						</div> <!-- /.col- -->
						<div class="col-lg-6 col-12">
							<div class="single-team-member clearfix">
								<div class="image-box float-left">
									<img src="images/team/4.jpg" alt="">
									<div class="opacity">
										<ul>
											<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
										</ul>
									</div> <!-- /.opacity -->
								</div> <!-- /.image-box -->
								<div class="text-wrapper float-left">
									<h6 class="name">Shane Yuen </h6>
									<span>CEO, Canny Digital</span>
									<p>“Success is making our clients succeed. Nothing else matters.”</p>
									<img src="images/home/sign.png" alt="">
								</div><!--  /.text-wrapper -->
							</div> <!-- /.single-team-member -->
						</div> <!-- /.col- -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.our-team -->


			<!-- 
			=============================================
				Feature Banner
			============================================== 
			-->
			<div class="feature-banner bg-one">
				<div class="opacity overlay-one">
					<div class="container">
						<div class="theme-title">
							<h2>We’re all about helping you reach your next <br>financial goal and loan help.</h2>
						</div> <!-- /.theme-title -->
						<div class="row">
							<div class="col-sm-4 col-12">
								<div class="single-box">
			        				<h2 class="number"><span class="timer" data-from="0" data-to="15000" data-speed="1200" data-refresh-interval="5">0</span>+</h2>
			        				<p>Customers Empowered <br>$5 billion+</p>
			        			</div> <!-- /.single-box -->
							</div>  <!-- /.col- -->
							<div class="col-sm-4 col-12">
								<div class="single-box">
			        				<h2 class="number"><span class="timer" data-from="0" data-to="120" data-speed="1200" data-refresh-interval="5">0</span>+</h2>
			        				<p>Times International <br>Award Winner</p>
			        			</div> <!-- /.single-box -->
							</div>  <!-- /.col- -->
							<div class="col-sm-4 col-12">
								<div class="single-box">
			        				<h2 class="number"><span class="timer" data-from="0" data-to="37500" data-speed="1200" data-refresh-interval="5">0</span>+</h2>
			        				<p>Completed Projects <br>$18 billion+</p>
			        			</div> <!-- /.single-box -->
							</div>  <!-- /.col- -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.feature-banner -->



			<!--
			=====================================================
				Testimonial Slider Two
			=====================================================
			-->
			<div class="testimonial-section-two">
				<div class="container">
					<div class="theme-title text-center">
						<h6>Tetimonials</h6>
						<h2>What People Say About us!</h2>
					</div> <!-- /.theme-title -->
					<div class="row">
						<div class="clearfix client-slider-two">
							<div class="item">
								<div class="main-content-box border-fix">
									<p>The heights by great men reach kept were attain by sudden flight, but they companion slept, were make tooiling upward in the night.</p>
									<div class="name">
										<h6>Ash lee</h6>
										<span>Senior UI/UX Designer </span>
									</div>
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div> <!-- /.main-content-box -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="main-content-box border-fix">
									<p>The heights by great men reach kept were attain by sudden flight, but they companion slept, were make tooiling upward in the night.</p>
									<div class="name">
										<h6>Rashed ka.</h6>
										<span>Senior UI/UX Designer </span>
									</div>
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div> <!-- /.main-content-box -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="main-content-box border-fix">
									<p>The heights by great men reach kept were attain by sudden flight, but they companion slept, were make tooiling upward in the night.</p>
									<div class="name">
										<h6>Mahfuz Na.</h6>
										<span>Senior UI/UX Designer </span>
									</div>
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div> <!-- /.main-content-box -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="main-content-box border-fix">
									<p>The heights by great men reach kept were attain by sudden flight, but they companion slept, were make tooiling upward in the night.</p>
									<div class="name">
										<h6>Rahat Joa.</h6>
										<span>Senior UI/UX Designer </span>
									</div>
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div> <!-- /.main-content-box -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="main-content-box border-fix">
									<p>The heights by great men reach kept were attain by sudden flight, but they companion slept, were make tooiling upward in the night.</p>
									<div class="name">
										<h6>Ash lee</h6>
										<span>Senior UI/UX Designer </span>
									</div>
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div> <!-- /.main-content-box -->
							</div> <!-- /.item -->
						</div> <!-- /.client-slider-two -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.testimonial-section-two -->




			<footer class="theme-footer">
				<div class="container">
					<div class="content-wrapper">
						<h2>Subscribe to Our Newsletter!</h2>
						<form action="#" class="subscribe-form">
							<div class="row">
								<div class="col-lg-5 col-md-6 col-12">
									<input type="text" placeholder="Full Name*">
								</div>
								<div class="col-lg-5 col-md-6 col-12">
									<input type="email" placeholder="Email Address*">
								</div>
								<div class="col-lg-2 col-12">
									<input type="submit" value="SUBSCRIBE NOW">
								</div>
							</div> <!-- /.row -->
						</form> <!-- /.subscribe-form -->

						<div class="footer-bottom-wrapper row">
							<div class="col-lg-3 col-sm-6 col-12 footer-logo" style="margin-top:40px">
								<div class="logo"><a href="index-2.html"><img src="images/logo/logo.png" alt="Logo"></a></div>
							</div> <!-- /.footer-logo -->
							<div class="col-lg-3 col-sm-6 col-12 footer-list">
								<!--<h4>Quick Links</h4>-->
								<ul>
									<li><a href="#">About ADNIC</a></li>
									<li><a href="#">Individual insurance</a></li>
									<li><a href="#">Security</a></li>
									<li><a href="#">Corporate Insurance</a></li>
								</ul>
							</div> <!-- /.footer-list -->
							<div class="col-lg-3 col-sm-6 col-12 footer-list">
								<ul>
									<li><a href="about-us.html">News & Media</a></li>
									<li><a href="#">Contact Us</a></li>
									<li><a href="team.html">Careers</a></li>
									<li><a href="#">Investor Relations</a></li>
								</ul>
							</div> <!-- /.footer-list -->
							<div class="col-lg-3 col-sm-6 col-12 footer-list">
								<ul>
									<li><a href="#">Whistleblower</a></li>
									<li><a href="#">ADNIC Archive</a></li>
								</ul>
							</div> <!-- /.footer-list -->
						</div> <!-- /.footer-bottom-wrapper -->

						<div class="copyright-wrapper row">
							<div class="col-md-6 col-sm-8 col-12">
								<p>
									© 2018 <a href="#">Abu Dhabi National Insurance Company,</a> All rights reserved.<br/>
									Designed with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://www.dmsswe.lk">DMS Software Engineering</a>.
								</p>
							</div>
							<div class="col-md-6 col-sm-4 col-12">
								<ul>
									<li><a href="https://www.facebook.com/adnic.ae"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="https://www.youtube.com/channel/UCTHWh0X6NnFaTZHhQi5x5dA"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div> <!-- /.copyright-wrapper -->
					</div>
				</div> <!-- /.container -->
			</footer> <!-- /.theme-footer -->

	        <!-- Scroll Top Button -->
			<button class="scroll-top tran3s">
				<i class="fa fa-angle-up" aria-hidden="true"></i>
			</button>

			<script src="plugins/jquery.2.2.3.min.js"></script>
			<!-- Popper js -->
			<script src="plugins/popper.js/popper.min.js"></script>
			<!-- Bootstrap JS -->
			<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
			
			<!-- Camera Slider -->
			<script src='plugins/Camera-master/scripts/jquery.mobile.customized.min.js'></script>
			<script src='plugins/Camera-master/scripts/jquery.easing.1.3.js'></script> 
			<script src='plugins/Camera-master/scripts/camera.min.js'></script>
			<!-- Select JS -->
			<script src="plugins/select2-master/dist/js/select2.min.js"></script>
			<!-- Mega menu  -->
			<script src="plugins/bootstrap-mega-menu/js/menu.js"></script>
			<!-- WOW js -->
			<script src="plugins/WOW-master/dist/wow.min.js"></script>
			<!-- owl.carousel -->
			<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
			<!-- js count to -->
			<script src="plugins/jquery.appear.js"></script>
			<script src="plugins/jquery.countTo.js"></script>
			<!-- accrue.min.js -->
			<script src="plugins/accrue.js/jquery.accrue.min.js"></script>
			<!-- Fancybox -->
			<script src="plugins/fancybox/dist/jquery.fancybox.min.js"></script>

			<!-- Theme js -->
			<script src="core/js/core.js"></script>
		</div> <!-- /.main-page-wrapper -->
	</body>

<!-- Mirrored from html.creativegigs.net/aproch/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Oct 2018 05:48:57 GMT -->
</html>