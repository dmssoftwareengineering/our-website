<!DOCTYPE html>
<html lang="en">
	
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>ADNIC | Your reliable insurance</title>
		
		<meta name="author" content="Basura Ratnayake">	
		<meta name="copyright" content="Copyright 2018 DMS Software Engineering. All Rights Reserved."/>

		<!-- SEO Start -->
		<meta name="description" content="Some Description">
		<meta name="keywords" content="Some Keywords" />

		<meta property="og:type" content="website" />
		<meta property="og:url" content="https://www.adnic.ae/" />
		<meta property="og:title" content="Abu Dhabi National Insurance Company" />	
		
		<meta property="og:image" content="images/logo/logo.png">
		<meta property="og:site_name" content="DMS Software Engineering" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:description" content="Some Description" />	
		<meta property="fb:pages" content="201829626513462" />

		<link rel="canonical" href="https://www.adnic.ae/" />
		
		<script type='application/ld+json'>{"@context":"http://schema.org","@type":"Organization","url":"https://www.adnic.ae/","@id":"#organization","name":"Abu Dhabi National Insurance Company","logo":"images/logo/logo.png","contactPoint":[{"@type":"ContactPoint","email":"mailto:info@dmsswe.com","telephone":"+94-11-287-6700","contactType":"Information"}]}</script>

		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="<?php echo base_url('assets/adnic/images/logo/favicon.png');?>">		
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/plugins/bootstrap/css/bootstrap.min.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/plugins/select2-master/dist/css/select2.min.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/plugins/Camera-master/css/camera.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/plugins/bootstrap-mega-menu/css/menu.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/core/fonts/font-awesome/css/font-awesome.min.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/plugins/owl-carousel/owl.carousel.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/plugins/WOW-master/css/libs/animate.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/core/fonts/icon/font/flaticon.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/plugins/fancybox/dist/jquery.fancybox.min.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/plugins/sanzzy-map/dist/snazzy-info-window.min.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/core/css/style.css');?>">		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adnic/core/css/responsive.css');?>">

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="plugins/html5shiv.js"></script>
			<script src="plugins/respond.js"></script>
		<![endif]-->
		<style>
			.circle-holder{
				padding:50px 0
			}
			.circle {
				border-radius: 45px;
				color: #000;
				border: #ff9000 solid 1px;
				margin-right: 10px;
				padding: 30px 0px;
				text-align: center;
				margin-bottom: 10px;
				/* height: 100px; */
				font-size: 13px;
			}
			
			.circle:hover{
				background:#ff9000;
				color:#fff
			}
			
			.our-service .theme-title {
				padding-bottom: 30px;
			}
			
			.our-service .single-service .text{
				padding-bottom:30px;
				margin-bottom:20px
			}
			
			.theme-title p {
				border: 1px solid rgba(0,0,0,0.05);
				border-bottom: 0;
				padding: 20px 0;
			}
		</style>
			
	</head>

	<body>
		<div class="search-box tran5s" id="searchWrapper">
	   		<div class="container">
	   			<form action="#">
	   				<div id="close-button" class="tran3s"></div>
	   				<div class="input-wrapper">
	   					<input type="text" placeholder="Search....">
	   					<button class="tran3s"><i class="fa fa-search" aria-hidden="true"></i></button>
	   				</div>
	   			</form>
	   		</div>
	   	</div> <!-- /.search-box -->
		<div class="main-page-wrapper">

			<div id="loader-wrapper">
				<div id="loader"></div>
			</div>
			
			<header class="theme-menu-wrapper">
				<div class="container">
					<div class="top-header clearfix">
						<div class="float-left greeting-text"><span>Hi,</span> <p class="greeting"></p> !</div>
						<ul class="float-right">
							<li>call us &nbsp;<a href="#" class="tran3s">971 800 8040</a></li>
							<li><a href="https://www.facebook.com/adnic.ae" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#" class="tran3s"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCTHWh0X6NnFaTZHhQi5x5dA" class="tran3s"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
						</ul>
					</div> <!-- /.top-header -->
					<div class="main-header-menu-wrapper clearfix">
						<!-- Logo -->
						<div class="logo float-left"><a href="index-2.html"><img src="<?php echo base_url('assets/adnic/images/logo/logo.png');?>" alt="Logo"></a></div>

						<nav class="navbar-expand-lg float-right navbar-light" id="mega-menu-wrapper">
					    	<button class="navbar-toggler float-right clearfix" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					    		<i class="flaticon-menu-options"></i>
					    	</button>
					    	<div class="collapse navbar-collapse clearfix" id="navbarNav">
					    	  <ul class="navbar-nav nav">
								<li class="nav-item dot-fix active">
					    	    	<a class="nav-link" href="#contact-us.html">Home</a>
					    	    </li>
								
					    	    <li class="nav-item dropdown-holder">
					    	    	<a class="nav-link" href="#">About</a>
					    	    	<ul class="sub-menu">
										<li><a href="about-us.html">ADNIC</a></li>
										<li><a href="#">Our People</a></li>
										<li><a href="#">Corporate</a></li>
										<li><a href="#">Governance</a></li>
										<li><a href="#service-details.html">Investor Relations</a></li>
										<li class="login"><a href="#index-3.html">Annual Reports</a></li>
									</ul>
					    	    </li>
					    	    <li class="nav-item dropdown-holder">
					    	    	<a class="nav-link" href="#">Products</a>
					    	    	<ul class="sub-menu">
					    	    		<li><a href="#portfolio-v1.html">Individual</a></li>
										<li><a href="#portfolio-v4.html">Corporate</a></li>
										<li><a href="#portfolio-v2.html">Reinsurance</a></li>
										<li><a href="#portfolio-v3.html">Bancassurance</a></li>
										<li><a href="#portfolio-details.html">Affinity Programs</a></li>
										<li class="login"><a href="#index-3.html">Customer Care</a></li>
									</ul>
					    	    </li>
								<li class="nav-item dropdown-holder">
					    	    	<a class="nav-link" href="#">Services</a>
					    	    	<ul class="sub-menu">
					    	    		<li><a href="#portfolio-v1.html">Mobility</a></li>
										<li><a href="#portfolio-v4.html">Risk Engineering</a></li>
										<li><a href="#portfolio-v2.html">Circulars</a></li>
										<li><a href="#portfolio-v3.html">Priority</a></li>
										<li><a href="#portfolio-details.html">Assist America</a></li>
										<li class="login"><a href="#index-3.html">Claims</a></li>
									</ul>
					    	    </li>
					    	    <li class="nav-item dropdown-holder">
					    	    	<a class="nav-link" href="#">Help & Support</a>
					    	    	<ul class="sub-menu">
					    	    		<li><a href="#portfolio-v1.html">Insurance Calculations</a></li>
										<li><a href="#portfolio-v4.html">Branch Network</a></li>
										<li><a href="#portfolio-v2.html">Customer Service</a></li>
										<li><a href="#portfolio-v3.html">Contact Us</a></li>
									</ul>
					    	    </li>
					    	    <li class="nav-item search-button"><button class="search b-p-bg-color" id="search-button"><i class="fa fa-search" aria-hidden="true"></i></button></li>
					    	  </ul>
					    	</div>
						</nav>
					</div>
				</div>
			</header>
			
			<div id="different-holder">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div id="fs-min" class="col-lg-4 btn-dh-s">
									A<span class="sup">-</span>
								</div>
								<div id="fs-norm" class="col-lg-4 btn-dh-s">
									A<span class="sup">n</span>
								</div>
								<div id="fs-plus" class="col-lg-4 btn-dh-s">
									A<span class="sup">+</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 btn-dh-s">
									Text to Speech <i class="fa fa-comment"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="theme-main-banner" class="banner-one">
				<div data-src="<?php echo base_url('assets/adnic/images/home/slide-1.jpg');?>">
					<div class="camera_caption">
						<div class="container">
							<h1 class="wow fadeInUp animated">Enjoy <span>Your</span> Life Today <br/> Let Us Take Care of<br/> <span>Your</span></h1>
							<div class="circle-holder">
								<div class="row ">
									<div class="col-lg-1 circle">Life</div>
									<div class="col-lg-1 circle">Health</div>
									<div class="col-lg-1 circle">Wedding</div>
									<div class="col-lg-1 circle">Vehicles</div>
								</div>
							</div>
							
							<div>
								<a href="#" class="button-one wow fadeInLeft animated" data-wow-delay="0.3s">Read More</a>
								<a href="service-v1.html" class="button-two wow fadeInRight animated" data-wow-delay="0.3s">Join Us Today</a>
							</div>
						</div> <!-- /.container -->
					</div> <!-- /.camera_caption -->
				</div>
				<div data-src="<?php echo base_url('assets/adnic/images/home/slide-2.jpg');?>">
					<div class="camera_caption">
						<div class="container">
							<h1 class="wow fadeInUp animated">Personal Bank Loan <br>From <span>$12,500</span> </h1>
							<p class="wow fadeInUp animated" data-wow-delay="0.2s">We have wide rang of loans ection for our customer & <br>it’s start low to high with low interest.</p>
							<a href="#" class="button-one wow fadeInLeft animated" data-wow-delay="0.3s">Apply for Loan</a>
							<a href="service-v1.html" class="button-two wow fadeInRight animated" data-wow-delay="0.3s">See services</a>
						</div> <!-- /.container -->
					</div> <!-- /.camera_caption -->
				</div>
				<div data-src="<?php echo base_url('assets/adnic/images/home/slide-3.jpg');?>">
					<div class="camera_caption">
						<div class="container">
							<h1 class="wow fadeInUp animated">Personal Bank Loan <br>From <span>$12,500</span> </h1>
							<p class="wow fadeInUp animated" data-wow-delay="0.2s">We have wide rang of loans ection for our customer & <br>it’s start low to high with low interest.</p>
							<a href="#" class="button-one wow fadeInLeft animated" data-wow-delay="0.3s">Apply for Loan</a>
							<a href="service-v1.html" class="button-two wow fadeInRight animated" data-wow-delay="0.3s">See services</a>
						</div> <!-- /.container -->
					</div> <!-- /.camera_caption -->
				</div>
			</div> <!-- /#theme-main-banner -->
			
			<div class="top-feature">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-4 col-12">
							<div class="single-feature clearfix">
								<img src="<?php echo base_url('assets/adnic/images/icon/1.png');?>" alt="" class="float-left tran3s">
								<div class="text float-left">
									<p>Up to $5M</p>
									<h4><a href="#" class="tran3s">Individual Insurance</a></h4>
								</div> <!-- /.text -->
							</div> <!-- /.single-feature -->
						</div> <!-- /.col- -->
						<div class="col-lg-4 col-md-6 col-sm-4 col-12">
							<div class="single-feature clearfix">
								<img src="<?php echo base_url('assets/adnic/images/icon/2.png');?>" alt="" class="float-left tran3s">
								<div class="text float-left">
									<p>From 5.60%</p>
									<h4><a href="#" class="tran3s">Corporate Insurance</a></h4>
								</div> <!-- /.text -->
							</div> <!-- /.single-feature -->
						</div> <!-- /.col- -->
						<div class="col-lg-4 d-md-none d-lg-block col-sm-4 col-12">
							<div class="single-feature clearfix">
								<img src="<?php echo base_url('assets/adnic/images/icon/3.png');?>" alt="" class="float-left tran3s">
								<div class="text float-left">
									<p>10 Days Process</p>
									<h4><a href="#" class="tran3s">Partner Services</a></h4>
								</div> <!-- /.text -->
							</div> <!-- /.single-feature -->
						</div> <!-- /.col- -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.top-feature -->

			<div class="our-service">
				<div class="container">
					<div class="theme-title text-center">
						<h2>We offer wide range of insurance covers.</h2>
						<p>Individual</p>
					</div> 
					
					<div class="row">
						<div class="clearfix product-slider-one">
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/1.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Home Insurance</a></h4>
									</div> <!-- /.text -->
								</div> <!-- /.single-service -->
							</div> <!-- /.col- -->
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/2.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Life Insurance</a></h4>
									</div> <!-- /.text -->
								</div> <!-- /.single-service -->
							</div> <!-- /.col- -->
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/3.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Medical Insurance</a></h4>
									</div> <!-- /.text -->
								</div> <!-- /.single-service -->
							</div>
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/4.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Motor Insurance</a></h4>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/1.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Personal Watercraft Insurance</a></h4>
									</div> <!-- /.text -->
								</div> <!-- /.single-service -->
							</div> <!-- /.col- -->
							
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/2.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Personal Accident Insurance</a></h4>
									</div> 
								</div> <!-- /.single-service -->
							</div> <!-- /.col- -->
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/4.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Wedding Insurance (Mabrouk)</a></h4>
									</div>
								</div>
							</div>						
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/3.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Travel Insurance</a></h4>
									</div> 
								</div> 
							</div> 
						</div>
					</div>
					
					<div class="theme-title text-center">
						<p>Corporate</p>
					</div> 
					<div class="row">
						<div class="clearfix product-slider-two">
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/1.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Aviation Insurance</a></h4>
									</div> <!-- /.text -->
								</div> <!-- /.single-service -->
							</div> <!-- /.col- -->
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/2.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Engineering and Construction Insurance</a></h4>
									</div> <!-- /.text -->
								</div> <!-- /.single-service -->
							</div> <!-- /.col- -->
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/3.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Energy (Oil and Gas) Onshore & Offshore Insurance </a></h4>
									</div> <!-- /.text -->
								</div> <!-- /.single-service -->
							</div>
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/4.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Financial Lines Insurance</a></h4>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/1.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Group Medical Insurance</a></h4>
									</div> <!-- /.text -->
								</div> <!-- /.single-service -->
							</div> <!-- /.col- -->
							
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/2.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Liabilities Insurance</a></h4>
									</div> 
								</div> <!-- /.single-service -->
							</div> <!-- /.col- -->
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/4.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Life Insurance</a></h4>
									</div>
								</div>
							</div>						
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/3.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Marine Cargo Insurance</a></h4>
									</div> 
								</div> 
							</div> 					
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/3.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Marine Hull Insurance</a></h4>
									</div> 
								</div> 
							</div> 				
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/3.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Motor Fleet Insurance</a></h4>
									</div> 
								</div> 
							</div> 
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/3.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Property Insurance</a></h4>
									</div> 
								</div> 
							</div> 
							<div class="col-lg-12">
								<div class="single-service">
									<div class="image-box"><img src="<?php echo base_url('assets/adnic/images/service/3.jpg');?>" alt=""></div>
									<div class="text">
										<h4><a href="service-details.html">Travel Insurance</a></h4>
									</div> 
								</div> 
							</div> 
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.our-service -->

			<div class="feature-banner bg-one">
				<div class="opacity overlay-one">
					<div class="container">
						<div class="theme-title">
							<h2>We’re all about helping you reach your next <br>financial goal and loan help.</h2>
						</div> <!-- /.theme-title -->
						<div class="row">
							<div class="col-sm-4 col-12">
								<div class="single-box">
			        				<h2 class="number"><span class="timer" data-from="0" data-to="15000" data-speed="1200" data-refresh-interval="5">0</span>+</h2>
			        				<p>Customers Empowered <br>$5 billion+</p>
			        			</div> <!-- /.single-box -->
							</div>  <!-- /.col- -->
							<div class="col-sm-4 col-12">
								<div class="single-box">
			        				<h2 class="number"><span class="timer" data-from="0" data-to="120" data-speed="1200" data-refresh-interval="5">0</span>+</h2>
			        				<p>Times International <br>Award Winner</p>
			        			</div> <!-- /.single-box -->
							</div>  <!-- /.col- -->
							<div class="col-sm-4 col-12">
								<div class="single-box">
			        				<h2 class="number"><span class="timer" data-from="0" data-to="37500" data-speed="1200" data-refresh-interval="5">0</span>+</h2>
			        				<p>Completed Projects <br>$18 billion+</p>
			        			</div> <!-- /.single-box -->
							</div>  <!-- /.col- -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.opacity -->
			</div> <!-- /.feature-banner -->
			
			<div class="work-progress">
				<div class="container">
					<div class="theme-title text-center">
						<h2>Fast &amp; Easy Application Process.</h2>
						<p>See how you will get the loan step by step easily</p>
					</div> <!-- /.theme-title -->
					<div class="wrapper">
						<div class="row">
							<div class="col-sm-4 col-12">
								<div class="single-figure">
									<div class="count">01</div>
									<h4><a href="#">Apply Bank Loan</a></h4>
									<p>First we will apply for a loan with  your needs and amount.</p>
								</div> <!-- /.single-figure -->
							</div> <!-- /.col- -->
							<div class="col-sm-4 col-12">
								<div class="single-figure">
									<div class="count">02</div>
									<h4><a href="#">Apply Review Loan</a></h4>
									<p>After apply bank loan they will review their requirments</p>
								</div> <!-- /.single-figure -->
							</div> <!-- /.col- -->
							<div class="col-sm-4 col-12">
								<div class="single-figure">
									<div class="count">03</div>
									<h4><a href="#">Approve Bank Loan</a></h4>
									<p>After completed the review bank will approve the loan.</p>
								</div> <!-- /.single-figure -->
							</div> <!-- /.col- -->
						</div> <!-- /.row -->
					</div> <!-- /.wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.work-progress -->

			<div class="testimonial-section">
				<div class="container">
					<div class="row">
						<div class="col-xl-8 col-12 offset-xl-4 wow fadeInUp">
							<div class="theme-title">
								<h6>Testimonials</h6>
								<h2>Check what people say <br>About us!</h2>
							</div> <!-- /.theme-title-one -->
						</div> <!-- /.col- -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
				<div class="main-bg-wrapper">
					<div class="overlay">
						<div id="watch-video">
							<h6>Watch</h6>
							<h4>Intro Video</h4>
							<a data-fancybox href="https://www.youtube.com/embed/r-AuLm7S3XE?rel=0&amp;amshowinfo=0" class="tran3s"><i class="flaticon-play-button"></i></a>
						</div>
						<div class="main-slider-wrapper">
							<div class="testimonial-slider">
								<div class="item">
									<i class="flaticon-close"></i>
									<p>“Excepteur sint occaecat cupida non at proident, sunt culp officia deserun moll anim est laborum. Sed perspi!</p>
									<div class="clearfix">
										<img src="images/home/a-1.jpg" alt="" class="float-left">
										<div class="name float-left">
											<h6>Rashed Ka.</h6>
											<span>Berlin</span>
										</div>
									</div>
								</div>
								<div class="item">
									<i class="flaticon-close"></i>
									<p>“Excepteur sint occaecat cupida non at proident, sunt culp officia deserun moll anim est laborum. Sed perspi!</p>
									<div class="clearfix">
										<img src="images/home/a-1.jpg" alt="" class="float-left">
										<div class="name float-left">
											<h6>Zubayer Hasan</h6>
											<span>Berlin</span>
										</div>
									</div>
								</div>
								<div class="item">
									<i class="flaticon-close"></i>
									<p>“Excepteur sint occaecat cupida non at proident, sunt culp officia deserun moll anim est laborum. Sed perspi!</p>
									<div class="clearfix">
										<img src="images/home/a-1.jpg" alt="" class="float-left">
										<div class="name float-left">
											<h6>Foqrul Munna</h6>
											<span>Berlin</span>
										</div>
									</div>
								</div>
							</div> <!-- /.testimonial-slider -->
						</div> <!-- /.main-slider-wrapper -->
					</div>
				</div> <!-- /.main-bg-wrapper -->
			</div> <!-- /.testimonial-section -->
			
			<div class="why-choose-us style-one">
				<div class="container">
					<div class="theme-title text-center">
						<h2>Why We’re Different</h2>
						<p>Cum sociis natoque penatibus et magnis parturient. Pro vel nibh et elit</p>
					</div> <!-- /.theme-title -->
					<div class="row">
						<div class="col-lg-4 col-md-6 col-12">
							<div class="single-block">
								<img src="images/icon/4.png" alt="">
								<h4><a href="#">Low Interest</a></h4>
								<p>Cum sociis natoq magnis partuent. Pro vel nibh et elit mollis </p>
								<a href="#">Check Interest</a>
							</div> <!-- /.single-block -->
						</div> <!-- /.col- -->
						<div class="col-lg-4 col-md-6 col-12">
							<div class="single-block">
								<img src="images/icon/5.png" alt="">
								<h4><a href="#">Fast Loan Process</a></h4>
								<p>Cum sociis natoq magnis partuent. Pro vel nibh et elit mollis </p>
								<a href="#">Time Duration</a>
							</div> <!-- /.single-block -->
						</div> <!-- /.col- -->
						<div class="col-lg-4 col-md-6 col-12">
							<div class="single-block">
								<img src="images/icon/6.png" alt="">
								<h4><a href="#">No Hidden Fees</a></h4>
								<p>Cum sociis natoq magnis partuent. Pro vel nibh et elit mollis </p>
								<a href="#">Check All Fees</a>
							</div> <!-- /.single-block -->
						</div> <!-- /.col- -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.why-choose-us -->
			
			<div class="latest-update">
				<div class="container">
					<div class="theme-title">
						<h2>Check what happen inside <br/>our company that affects the world.</h2>
						<a href="blog-grid.html">GO TO NEWS</a>
					</div> <!-- /.theme-title -->

					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-4 col-12">
							<div class="single-update-post">
								<div class="count">01</div>
								<h4><a href="blog-details.html">ADNIC strengthens partnership with UAE Genetic Diseases Association to support the prevention of genetic disorders .</a></h4>
								<p>September 16, 2017</p>
							</div> <!-- /.single-update-post -->
						</div> <!-- /.col- -->
						<div class="col-lg-4 col-md-6 col-sm-4 col-12">
							<div class="single-update-post">
								<div class="count">02</div>
								<h4><a href="blog-details.html">ADNIC reports AED 150.5 million net profit for the first half of 2018.</a></h4>
								<p>August 1, 2018</p>
							</div> <!-- /.single-update-post -->
						</div> <!-- /.col- -->
						<div class="col-lg-4 d-md-none d-lg-block col-sm-4 col-12">
							<div class="single-update-post">
								<div class="count">03</div>
								<h4><a href="blog-details.html">How To Stop Living Your Life On Autopilot</a></h4>
								<p>August 15, 2017</p>
							</div> <!-- /.single-update-post -->
						</div> <!-- /.col- -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.latest-update -->
			
			<footer class="theme-footer">
				<div class="container">
					<div class="content-wrapper">
						<h2>Subscribe to Our Newsletter!</h2>
						<form action="#" class="subscribe-form">
							<div class="row">
								<div class="col-lg-5 col-md-6 col-12">
									<input type="text" placeholder="Full Name*">
								</div>
								<div class="col-lg-5 col-md-6 col-12">
									<input type="email" placeholder="Email Address*">
								</div>
								<div class="col-lg-2 col-12">
									<input type="submit" value="SUBSCRIBE NOW">
								</div>
							</div> <!-- /.row -->
						</form> <!-- /.subscribe-form -->

						<div class="footer-bottom-wrapper row">
							<div class="col-lg-3 col-sm-6 col-12 footer-logo" style="margin-top:40px">
								<div class="logo"><a href="index-2.html"><img src="<?php echo base_url('assets/adnic/images/logo/logo.png');?>" alt="Logo"></a></div>
							</div> <!-- /.footer-logo -->
							<div class="col-lg-3 col-sm-6 col-12 footer-list">
								<!--<h4>Quick Links</h4>-->
								<ul>
									<li><a href="#">About ADNIC</a></li>
									<li><a href="#">Individual insurance</a></li>
									<li><a href="#">Security</a></li>
									<li><a href="#">Corporate Insurance</a></li>
								</ul>
							</div> <!-- /.footer-list -->
							<div class="col-lg-3 col-sm-6 col-12 footer-list">
								<ul>
									<li><a href="about-us.html">News & Media</a></li>
									<li><a href="#">Contact Us</a></li>
									<li><a href="team.html">Careers</a></li>
									<li><a href="#">Investor Relations</a></li>
								</ul>
							</div> <!-- /.footer-list -->
							<div class="col-lg-3 col-sm-6 col-12 footer-list">
								<ul>
									<li><a href="#">Whistleblower</a></li>
									<li><a href="#">ADNIC Archive</a></li>
								</ul>
							</div> <!-- /.footer-list -->
						</div> <!-- /.footer-bottom-wrapper -->

						<div class="copyright-wrapper row">
							<div class="col-md-6 col-sm-8 col-12">
								<p>
									© 2018 <a href="#">Abu Dhabi National Insurance Company,</a> All rights reserved.<br/>
									Designed with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://www.dmsswe.lk">DMS Software Engineering</a>.
								</p>
							</div>
							<div class="col-md-6 col-sm-4 col-12">
								<ul>
									<li><a href="https://www.facebook.com/adnic.ae"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="https://www.youtube.com/channel/UCTHWh0X6NnFaTZHhQi5x5dA"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div> <!-- /.copyright-wrapper -->
					</div>
				</div> <!-- /.container -->
			</footer> <!-- /.theme-footer -->
			
						<!--Start of Tawk.to Script-->
			<script type="text/javascript">
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/5bd0247d476c2f239ff5be70/default';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
			})();
			</script>
			<!--End of Tawk.to Script-->

	        <!-- Scroll Top Button -->
			<button class="scroll-top tran3s">
				<i class="fa fa-angle-up" aria-hidden="true"></i>
			</button>

			<script src="<?php echo base_url('assets/adnic/plugins/jquery.2.2.3.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/popper.js/popper.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/Camera-master/scripts/jquery.mobile.customized.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/Camera-master/scripts/jquery.easing.1.3.js');?>"></script> 
			<script src="<?php echo base_url('assets/adnic/plugins/Camera-master/scripts/camera.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/select2-master/dist/js/select2.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/bootstrap-mega-menu/js/menu.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/WOW-master/dist/wow.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/owl-carousel/owl.carousel.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/jquery.appear.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/jquery.countTo.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/accrue.js/jquery.accrue.min.js');?>"></script>
			<script src="<?php echo base_url('assets/adnic/plugins/fancybox/dist/jquery.fancybox.min.js');?>"></script>

			<!-- Theme js -->
			<script src="<?php echo base_url('assets/adnic/core/js/core.js');?>"></script>
		</div> <!-- /.main-page-wrapper -->
	</body>
</html>