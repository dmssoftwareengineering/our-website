<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Apply to DMS Software Engineering | Careers";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);		
   ?>
   <style>
	.contact-us-form input[type="text"],.contact-us-form input[type="phone"],.contact-us-form input[type="email"]{padding:8px 20px;border-radius:10px}.contact-us-form input[type="file"]{font-size:12px;background:#fff;padding:9px 13px;border:1px solid #F1F1F1;border-radius:10px}label{font-size:14px;margin-bottom:0;padding-left:21px;font-weight:700}label span{color:red}.custom-select{border:1px solid #F1F1F1;padding:16px 30px;border-radius:50px;border:1px solid #F1F1F1;padding:5px 20px;border-radius:10px}.pos{position:relative}.btn-primary{border-radius:10px;padding:10px 30px}.error-msg{display:none;color:red;font-size:12px;padding-left:23px}
   </style>
</head>

<body>
   <?php $this->load->view('inc/header'); ?>
   
   <div id='recaptcha' class="g-recaptcha" data-sitekey="6LeVAGwUAAAAAMB4sy-SmzMCf3qJWRSIQib5JJ7y" data-callback="onloadCallback" data-size="invisible"></div>
   
   <section id="main-container" class="main-container" data-aos="fade-left" data-aos-once="false">
      <div class="container">
         <div class="row">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>Team Player, Innovative and Good at What You Do Then</small>
                     Apply to <span>DMS Software Engineering</span>
                     <small>Today and be a Part of Our Family</small>
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
		 
		 <div class="row">
			<div class="col">
				<div class="contact-us-form">
				   <div class="error-container"></div>
				   <div class="row">
					  <div class="col-lg-6">
						 <div class="form-group">
							<label>First Name <span>*</span></label>
							<input class="form-control form-name" name="first_name" id="first_name" placeholder="Enter Your First Name" type="text">
							<span class="error-msg">Please enter valid first name</span>
						 </div>
					  </div>
					  <div class="col-lg-6">
						 <div class="form-group">
							<label>Last Name <span>*</span></label>
							<input class="form-control form-name" name="last_name" id="last_name" placeholder="Enter Your Last Name" type="text">
							<span class="error-msg">Please enter valid last name</span>
						 </div>
					  </div>
					  <div class="col-lg-6">
						 <div class="form-group">
							<label>Mobile Number <span>*</span></label>
							<input class="form-control form-name" name="mobile_number" id="mobile_number" placeholder="Enter Your Mobile Number" type="text">
							<span class="error-msg">Please enter valid mobile number</span>
						 </div>
					  </div>
					  <div class="col-lg-6">
						 <div class="form-group">
							<label>Email Address <span>*</span></label>
							<input class="form-control form-name" name="email" id="email" placeholder="Enter Your Email" type="text">
							<span class="error-msg">Please enter valid email</span>
						 </div>
					  </div>
					  <div class="col-lg-12">
						 <div class="form-group pos">
							<label>Position Applying <span>*</span></label>
							
							<?php								
								$id_found = array_key_exists('id', $this->input->get()) >= 1 ? '1' : '0';
								$pos_name = '';
							?>
							
							<select id="position_select" name="position_select" class="custom-select" style="width: 100%" placeholder="asdas" <?php echo $id_found == '1' ? 'disabled' : ''; ?>>
								<option value="-1" data-pos="" disabled selected>Select your position</option>
								<?php 	
									for($i=0;$i<sizeof($positions);$i++){
										$_id = $positions[$i]['pos_id'];										
										
										if ($id_found == '1' && $_id == $this->input->get()['id']) {
											$pos_name = $positions[$i]['pos_name'];
											echo '<option value="'.$_id.'" data-pos="'.$positions[$i]['pos_name'].'" selected>'.$positions[$i]['pos_name'].'</option>';
										} else {
											echo '<option value="'.$_id.'" data-pos="'.$positions[$i]['pos_name'].'">'.$positions[$i]['pos_name'].'</option>';
										}
									}
								?>
							</select>
							<input name="position" id="position" type="text" hidden value="<?php echo $pos_name;  ?>">
							<span class="error-msg">Please enter valid position</span>
						 </div>
					  </div>
					  <div class="col-lg-6">
						 <div class="form-group">
							<label>LinkedIn Profile</label>
							<input class="form-control form-name" name="linkedin" id="linkedin" placeholder="Enter Your LinkedIn Profile Link" type="text">
							<span class="error-msg">Please enter valid linkedin url</span>
						 </div>
					  </div>
					  <div class="col-lg-6">
						 <div class="form-group">
							<label>Resume / CV <span>*</span></label>
							<input class="form-control form-name" name="file" id="file" placeholder="Select a Resume to Submit" type="file">
							<span class="error-msg">Please select a file</span>
						 </div>
					  </div>
				   </div>
				   <div class="text-right">
					  <button id="send-resume" class="btn btn-primary">Apply Now</button>
				   </div>
			 </div>
			</div>
		 </div>
      </div>
   </section>
   
   <?php
		$this->load->view('inc/footer');		
   ?>
   <script>
		$(function ($) {
			$('#position_select').change(function(){
				var tag = $("option[value="+$(this).val()+"]", this).attr('data-pos');
				console.log(tag);
				$('#position').val(tag);
			});
			
			$('#send-resume').click(function() {
				grecaptcha.execute();
				sendFile_Ajax([
					'first_name',
					'last_name',
					'mobile_number',
					'email',
					'position',
					'linkedin',
					'file',
					'position_select',
				], [
					/^[A-Za-z ]{3,30}$/i,
					/^[A-Za-z ]{5,40}$/i,
					/^[0-9]{10,11}$/i,
					/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w+)+$/i,
					/^[A-Za-z ]{10,30}$/i,
					/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:\/?#[\]@!\$&'\(\)\*\+,;=.]+$/i,
					'file_uplaod',
					/^[0-9]{1,3}$/i
				], 
				'<?php echo base_url('careers/sendResume');?>','<?php echo $csrf_token; ?>','Job');
			});
		});
   </script>
	
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>
</html>