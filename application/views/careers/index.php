<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Careers | DMS Software Engineering";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);		
   ?>
   <style>
	.positions{
		font-weight:bold;
		margin-bottom:0
	}
	
	.positions a{
		color:#2f2c2c
	}
	.positions a:hover{
		color:#0070c0
	}
   </style>
</head>

<body>
   <?php $this->load->view('inc/header'); ?>
   
   <section id="main-container" class="main-container" data-aos="fade-left" data-aos-once="false">
      <div class="container">
         <div class="row">
            <div class="col-md-7">
               <div class="tw-about-bin">
                  <h2 class="column-title">
					<small>Not Colleagues, We are Family</small>
					 Life at <span>DMS SWE</span>
				   </h2>
                  <span class="animate-border tw-mb-40 tw-mt-20"></span>
				  
				  <p><img src="<?php echo base_url('assets/core/images/news/post2.jpg');?>" alt="" class="img-fluid"></p> 
				  
                  <p>A career with <strong>DMS Software Engineering</strong> will always be more than a job. An organization that pioneered the IT industry in Sri Lanka over 30 years. DMS Software Engineering (Pvt) Ltd consists of an energetic group of knowledgeable professionals. We believe that our people are our greatest assets and it is people who will ensure our success. </p>
				  
				  <p> Our vision is to hire the best, not for what they can provide today, but for what they can deliver tomorrow. We invest in our people through emerging technology training and certifications. A continuous process of formal training and project assignments ensures that our employees have the leading-edge expertise in emerging technologies. At DMS Software Engineering (Pvt) Ltd, freedom inspires ideas and the working environment nurtures excellence. </p>
               </div>
            </div>
			
            <div class="col-md-4">               
			  <h2 class="column-title">Latest Vacancies</h2>
			   
			   <div>				
					<?php $pos_length = sizeof($positions); ?>
				
					<?php 
						for($i=0;$i<$pos_length;$i++){
							$pos_name = strtolower(str_replace(" ", "-",$positions[$i]['pos_name']));
							$pos_name = rtrim($pos_name, '-');
							
					?>
					

						<p class="positions"><a href="<?php echo base_url('careers/view/'.$positions[$i]['pos_id'].'/'.$pos_name); ?>"><?php echo $positions[$i]['pos_name']; ?></a></p>							
					<?php }?>
			   </div>
			   
			   
				<?php if ($pos_length <= 0) { ?>
					<p>We are always searching for talented individuals such as yourself. <br/> <a href="<?php echo base_url('careers/apply');?>">Send us your resume today</a></p>
				<?php } ?>
			   
			   
			   
            </div>
         </div>
      </div>
   </section>
   
   <?php
	$this->load->view('inc/footer');		
   ?>
</body>

</html>