<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Careers | DMS Software Engineering";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);		
   ?>
   <style>
	.positions{
		font-weight:bold;
		margin-bottom:0
	}
	
	.apply {
		background:#fff;
		border:1px solid #0070c0;
		color:#0070c0;
		width: 40%;
		padding: 0 10px;
	}
	.apply:hover{
		background: #0070c0;
		color: #fff;
	}
   </style>
</head>

<body>
   <?php $this->load->view('inc/header'); ?>
   
   <section id="main-container" class="main-container" data-aos="fade-left" data-aos-once="false">
      <div class="container">
         <div class="row">
            <div class="col-md-7">
               <div class="tw-about-bin">
                  <h2 class="column-title">
					<small><a href="<?php echo base_url('careers'); ?>">Careers</a> / <?php echo $pos_data['pos_name']; ?></small>
					 <?php echo $pos_data['pos_name']; ?>
				   </h2>
                  <span class="animate-border tw-mb-40 tw-mt-20"></span>
				  
				  <div id="pos-req"></div>
				  
				  <?php 
					$link_name = strtolower(str_replace(" ", "-", $pos_data['pos_name']));
					$link_name = rtrim($link_name, '-');
					
					$link_name = $link_name.'/?id='.$pos_data['pos_id'];
				  ?>
				  
				  <a class="apply" href="<?php echo base_url('careers/apply/'.$link_name); ?>">Apply for this Position</a>
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <?php
	$this->load->view('inc/footer');
   ?>
   <script>		
		$(function() {				
			function htmlspecialchars_decode(str) {
				var map = {
					"&amp;": "&",
					"&lt;": "<",
					"&gt;": ">",
					"&quot;": "\"",
					"&#39;": "'"
				};
				return str.replace(/(&amp;|&lt;|&gt;|&quot;|&#39;)/g, function(m) { return map[m]; });
			}
			
			var pos_data = '<?php echo $pos_data['pos_require']; ?>';
			pos_data = htmlspecialchars_decode(pos_data);
			$('#pos-req').html(pos_data);		
		});
   </script>
</body>

</html>