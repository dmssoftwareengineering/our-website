<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Contact Us | DMS Software Engineering";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);		
   ?>
</head>

<body>
   <?php $this->load->view('inc/header'); ?>
	
   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row" data-aos="fade-down" data-aos-once="false">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>Get in touch with us</small>
                     We'd <i class="fa fa-heart" aria-hidden="true"></i> to Talk
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-4" data-aos="fade-left" data-aos-once="false">
               <div class="tw-contact-box">
                  <div class="contact-heading">
                     <h3>Registered Office</h3>
                  </div>
                  <div class="contact-info-box-content">
                     <i class="fa fa-map-marker"></i>
                     <p>No. 221/5,<br/>Dharmapala Mawatha,<br/>Colombo 07,<br/>Sri Lanka. </p>
                  </div>
               </div>
            </div>
            <div class="col-md-4" data-aos="fade-down" data-aos-once="false">
               <div class="tw-contact-box">
                  <div class="contact-heading">
                     <h3>Corporate Office</h3>
                  </div>
                  <div class="contact-info-box-content">
                     <i class="fa fa-map-marker"></i>
                     <p>No. 380, Koswatta Road,<br/>Kalapaluwawa,<br/>Rajagiriya,<br/>Sri Lanka. </p>
                  </div>
               </div>
            </div>
            <div class="col-md-4" data-aos="fade-right" data-aos-once="false">
               <div class="tw-contact-box">
                  <div class="contact-heading">
                     <h3>Contact Details</h3>
                  </div>
                  <div class="contact-info-box-content">
                     <i class="fa fa-phone"></i>
                     <p><strong>Hotline:</strong> (+94) 11 287 6700</p>
					 <p><strong>Fixed Lines:</strong> (+94) 11 287 6875/78</p>
					 <p><strong>Fax:</strong> (+94) 11 287 6877</p>
                     <i class="fa fa-envelope"></i>
                     <p>info@dmsswe.com </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <div class="banner-area" data-aos="fade-down" data-aos-once="false">
        <div id="map" class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.779166987992!2d79.91640721477289!3d6.916984595002169!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae2599c0bb97f25%3A0x41f84c2421969f1a!2sDMS+Software+Engineering+(Pvt)+Ltd!5e0!3m2!1sen!2slk!4v1530596163272" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
   </div>

   <!--<section id="tw-contact-us-testimonial" class="tw-contact-us-testimonial" hidden>
      <div class="container">
		<div class="row">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>What our clients think of us</small>
                     Hall of Fame
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row justify-content-center">
            <div class="col-md-8 text-center">
               <div class="testimonial-slider owl-carousel">
                  <div class="testimonial-content">
                     <div class="testimonial-meta">
                        <h4>
                           Jason Stattham
                           <small>CEO Microhost</small>
                        </h4>
                        <i class="icon icon-quote2"></i>
                     </div>
                     <div class="testimonial-text">
                        <p>Start working with an company that can do provide every thing at you need to generate awareness,
                           drive traffic, connect with</p>
                     </div>
                  </div>
                  <div class="testimonial-content">
                     <div class="testimonial-meta">
                        <h4>
                           Jason Stattham
                           <small>CEO Microhost</small>
                        </h4>
                        <i class="icon icon-quote2"></i>
                     </div>
                     <div class="testimonial-text">
                        <p>Start working with an company that can do provide every thing at you need to generate awareness,
                           drive traffic, connect with</p>
                     </div>
                  </div>
                  <div class="testimonial-content">
                     <div class="testimonial-meta">
                        <h4>
                           Jason Stattham
                           <small>CEO Microhost</small>
                        </h4>
                        <i class="icon icon-quote2"></i>
                     </div>
                     <div class="testimonial-text">
                        <p>Start working with an company that can do provide every thing at you need to generate awareness,
                           drive traffic, connect with</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>-->

   <?php $this->load->view('inc/footer'); ?>
</body>
</html>