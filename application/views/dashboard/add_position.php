<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Add Position";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
	?>	
	<style>
		.validation-error-label{
			display:none
		}
		.note-placeholder {
			color: grey;
			padding: 10px;
		}
	</style>
</head>
<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "position_a";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>	
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Positions</span> - Add</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url('dashboard'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="<?php echo base_url('dashboard/positions'); ?>">Positions</a></li>
							<li class="active">Add Position</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
					<div class="row">
						<div class="col-md-4">

							<!-- Basic layout-->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-reading position-left"></i> Position Details</h5>
								</div>

								<div class="panel-body">
									<div class="form-group">
										<label>Position Name:</label>
										<input id="pos-name" type="text" class="form-control" placeholder="Enter Position Name">
										<label id="pos-name-error" class="validation-error-label">Please Enter a Valid Position Name</label>
									</div>
									
									<div class="form-group">
										<label>Position Manager:</label>
										<select id="manager_select" class="select-results-color">
											<?php for ($i=0;$i<sizeof($users);$i++) { ?>									
												<option value="<?php echo $users[$i]['user_id']; ?>"><?php echo $users[$i]['first_name']." ".$users[$i]['last_name']; ?></option>
											<?php }?>
										</select>	
										<input id="pos-manager" type="hidden" value="<?php echo $users[0]['user_id']; ?>"">
										<label id="pos-manager-error" class="validation-error-label">Select a Manager for the Position</label>
									</div>
									
									<div class="text-right">
										<button id="btn-add" class="btn btn-primary">Add Position <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
							<!-- /basic layout -->
						</div>

						<div class="col-md-8">

							<!-- Static mode -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-reading position-left"></i> Position Requirements</h5>
								</div>

								<div class="panel-body">
									<div id="pos-req-sam"></div>
									<input id="pos-req" type="hidden">
									<label id="pos-name-req" class="validation-error-label">Please Enter Valid Position Requirements</label>
								</div>
							</div>							
							<!-- /static mode -->

						</div>
					</div>
					<!-- /vertical form options -->
					
					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<?php $this->load->view('dashboard/inc/foot'); ?>
	<script type="text/javascript" src="<?php echo base_url('assets/core/js/alertify.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/editors/summernote/summernote.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/forms/selects/select2.min.js');?>"></script>	
	<script>
		$(function() {			
			base_url = '<?php echo base_url('dashboard/positions'); ?>';
			var url_path = '<?php echo base_url('dashboard/addPosition');?>',
				csrf_token = '<?php echo $csrf_token; ?>';
				
			$('.select-results-color').select2();
			
			$('#manager_select').on('select2:select', function (e) {
				var man_id = $(this).val();		
				$('#pos-manager').val(man_id);
			});
				
			$('#pos-req-sam').summernote({
				height: 400,
				focus: true,
				placeholder: 'Enter Position Requirements Here...',
				toolbar: [
					['style', ['bold', 'italic', 'underline', 'clear']],
					['fontsize', ['fontsize']],
					['color', ['color']],
					['font', ['strikethrough', 'superscript', 'subscript']],
					['para', ['ul', 'ol', 'paragraph']],
					['height', ['height']],
					['insert', ['picture', 'link', 'table']],
					['Misc', ['codeview']]
				]
			});
			
			$('#btn-add').click(function() {
				var pos_req = $('#pos-req-sam').summernote('code');
				pos_req = htmlSpecialChars(pos_req);				
				$('#pos-req').val(pos_req);
				
				sendText_Ajax([
					'pos-name',
					'pos-req',
					'pos-manager'
				], [
					/^[A-Za-z0-9 ]{10,30}$/i,
					/^[\w\W]{30,}$/i,
					/^[0-9]{1,8}$/i,
				], 
				url_path,csrf_token);
			});
		});
	</script>
</body>
</html>
