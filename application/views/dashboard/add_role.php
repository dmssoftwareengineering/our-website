<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Add Role";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
	?>	
	<style>
		.validation-error-label{
			display:none
		}
		.perm-link{
			cursor:pointer
		}
		.perm-link:hover{
			background:grey;
			border-color:grey;
		}
	</style>
</head>
<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "role_a";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>	
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Roles</span> - Add New</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url('dashboard'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="<?php echo base_url('dashboard/roles'); ?>">Roles</a></li>
							<li class="active">Add Role</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
					<div class="row">
						<div class="col-md-4">

							<!-- Basic layout-->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-reading position-left"></i> Role Details</h5>
								</div>

								<div class="panel-body">
									<div class="form-group">
										<label>Role Name:</label>
										<input id="role-name" type="text" class="form-control" placeholder="Enter Role Name">
										<label id="role-name-error" class="validation-error-label">Please Enter a Valid Role Name</label>
									</div>
									
									<div class="text-right">
										<button id="btn-add" class="btn btn-primary">Add Role <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
							<!-- /basic layout -->
						</div>

						<div class="col-md-8">

							<!-- Static mode -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-reading position-left"></i> Role Permission(s)</h5>
								</div>

								<div class="panel-body">
									<div id="role-perm-view">
										<div class="panel-group panel-group-control content-group-lg" id="accordion-control">
											<?php
												$modules = array();
												$perms = array();
												
												$prev_mod = '';
												
												for ($i=0;$i<sizeof($permissions);$i++) {													
													if ($permissions[$i]['module_name'] != $prev_mod) {	
														$modules[] = array(
															"mod_name" => $permissions[$i]['module_name'],
															"permissions" => array()
														);
														$perms = array();
													}		
													
													array_push($modules[sizeof($modules)-1]['permissions'], array(
														"id" => $permissions[$i]['permission_id'],
														"sht_name" => $permissions[$i]['perm_short_name'],
														"name" => $permissions[$i]['perm_name']
													));
																										
													$prev_mod = $permissions[$i]['module_name'];
												}
											?>
										
											<?php for ($i=0;$i<sizeof($modules);$i++) { ?>												
												<div class="panel panel-white">
													<div class="panel-heading">
														<h6 class="panel-title">
															<a class="collapsed" data-toggle="collapse" data-parent="#accordion-control" href="#permission-mod-<?php echo $i; ?>" aria-expanded="false"><?php echo $modules[$i]['mod_name']; ?></a>
														</h6>
													</div>	
													<div id="permission-mod-<?php echo $i; ?>" class="panel-collapse collapse">
													<?php for ($j=0;$j<sizeof($modules[$i]['permissions']);$j++) { ?>														
														<div class="panel-body list-group-item">														
															<?php echo $modules[$i]['permissions'][$j]['name']; ?>
															<span id="rem_<?php echo $modules[$i]['permissions'][$j]['id']; ?>" class="label bg-danger-400 perm-link remove hide">Remove Permission</span>
															<span id="add_<?php echo $modules[$i]['permissions'][$j]['id']; ?>" class="label bg-blue-400 perm-link add">Add Permission</span>
														</div>														
													<?php }?>	
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
									<input id="role-perms" type="hidden">
									<label id="role-perms-error" class="validation-error-label">Please Select atleast one permission for Role</label>
								</div>
							</div>							
							<!-- /static mode -->

						</div>
					</div>
					<!-- /vertical form options -->
					
					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<?php $this->load->view('dashboard/inc/foot'); ?>
	<script type="text/javascript" src="<?php echo base_url('assets/core/js/alertify.js'); ?>"></script>
	<script>
		$(function() {	
			base_url = '<?php echo base_url('dashboard/roles'); ?>';
			var url_path = '<?php echo base_url('dashboard/addRole');?>',
				csrf_token = '<?php echo $csrf_token; ?>';
				
			var selected_perms = [];
			
			function isPerm(perm_index){
				return selected_perms.indexOf(perm_index) > -1 ? true : false;
			}
				
			$('.add').click(function(){
				var perm_id = this.id.replace("add_", "");
				if (!isPerm(perm_id)) selected_perms.push(perm_id);				
				
				$("#"+this.id).hide();
				$("#rem_"+perm_id).removeClass('hide');
			});
			
			$('.remove').click(function(){				
				var perm_id = this.id.replace("rem_", "");
				if (isPerm(perm_id)) selected_perms.splice(selected_perms.indexOf(perm_id), 1);				
				
				$("#"+this.id).addClass('hide');
				$("#add_"+perm_id).show();
			});
			
			$('#btn-add').click(function() {
				$('#role-perms').val(selected_perms);
				
				sendText_Ajax([
					'role-name',
					'role-perms'
				], [
					/^[A-Za-z0-9 ]{5,40}$/i,
					/^[0-9,]+$/i
				], 
				url_path,csrf_token);
			});
		});
	</script>
</body>
</html>