<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Add User";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
	?>	
	<style>
		.validation-error-label{
			display:none
		}
		.note-placeholder {
			color: grey;
			padding: 10px;
		}
	</style>
</head>
<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "user_a";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>	
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Users</span> - Add</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url('dashboard'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="<?php echo base_url('dashboard/users'); ?>">Users</a></li>
							<li class="active">Add User</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
					<div class="row">
						<div class="col-md-4">

							<!-- Basic layout-->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-user position-left"></i> Position Details</h5>
								</div>

								<div class="panel-body">
									<div class="form-group">
										<label>First Name:</label>
										<input id="first_name" type="text" class="form-control" placeholder="Enter Your First Name">
										<label id="first-name-error" class="validation-error-label">Please Enter Your First Name</label>
									</div>
									
									<div class="form-group">
										<label>Last Name:</label>
										<input id="last_name" type="text" class="form-control" placeholder="Enter Your Last Name">
										<label id="last-name-error" class="validation-error-label">Please Enter Your Last Name</label>
									</div>
									
									<div class="form-group">
										<label>Email Address:</label>
										<input id="email" type="text" class="form-control" placeholder="Enter Your Email Address">
										<label id="email-error" class="validation-error-label">Please Enter Valid dmsswe.com Address</label>
									</div>
									
									<div class="form-group">
										<label>Password:</label>
										<input id="password" type="password" class="form-control" placeholder="Enter Password">
										<label id="password-error" class="validation-error-label">Please Enter Valid Password</label>
									</div>
									
									<div class="text-right">
										<button id="btn-add" class="btn btn-primary">Add New User <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
							<!-- /basic layout -->
						</div>

						<div class="col-md-8">

							<!-- Static mode -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-users4 position-left"></i> User Authorization</h5>
								</div>

								<div class="panel-body">
									<div class="form-group">
										<label>Select a Role for the New User</label>
										<select id="role_select" class="select-results-color">
											<?php 
												$role_ids = "";$role_perm="";
												for ($i=0;$i<sizeof($roles);$i++) { 											
													echo '<option value="'.$roles[$i]['role_id'].'">'.$roles[$i]['role_name'].'</option>';
													$role_ids = $role_ids.$roles[$i]['role_id']."|";	
													$role_perm = str_replace("<br/>",",", $role_perm.$roles[$i]['permission']."|");			
												}
												$role_ids = rtrim($role_ids, "|");

												$role_perm = rtrim($role_perm, "|");
												$role_perm = ltrim($role_perm, ",");
												$role_perm = str_replace("|,", "|", $role_perm);
											?>
										</select>										
										<input id="pos-id" type="hidden" value="<?php echo $roles[0]['role_id']; ?>">
										<label id="pos-id-error" class="validation-error-label">Please Select a Role for the User</label>
									</div>
								
									<div class="panel-heading">
										<h5 class="panel-title">Permissions Granted for the Role</h5>
										<div class="form-group">
											<label id="role_permissions"></label>
											<label id="pos-name-error" class="validation-error-label">Please Enter a Valid First Name</label>
										</div>
									</div>
								</div>
							</div>							
							<!-- /static mode -->
						</div>
					</div>
					<!-- /vertical form options -->
					
					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<?php $this->load->view('dashboard/inc/foot'); ?>
	<script type="text/javascript" src="<?php echo base_url('assets/core/js/alertify.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/editors/summernote/summernote.min.js');?>"></script>	
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/forms/selects/select2.min.js');?>"></script>	
	<script>
		base_url = '<?php echo base_url('dashboard/users'); ?>';
		var url_path = '<?php echo base_url('dashboard/addUser');?>',
			csrf_token = '<?php echo $csrf_token; ?>',
			role_ids = '<?php echo $role_ids ?>',
			role_perm = '<?php echo $role_perm ?>';
	
		$('.select-results-color').select2();
		
		role_perm = role_perm.replace(/[,]/g, "<br/>");
		
		role_ids = role_ids.split("|");
		role_perm = role_perm.split("|");
		
		for ( var i=0;i<role_perm.length;i++) {			
			role_perm[i] = role_perm[i].replace(",","");
		}
		
		$('#role_select').on('select2:select', function (e) {
			var role_id = $(this).val();			
			$('#role_permissions').html(role_perm[role_id-1]);		
			$('#pos-id').val(role_id);
		});
		
		$('#role_permissions').html(role_perm[0]);	
		
		$('#btn-add').click(function(){					
				sendText_Ajax([
					'first_name',
					'last_name',
					'email',
					'password',
					'pos-id'
				], [
					/^[A-Za-z ]{4,30}$/i,
					/^[A-Za-z ]{5,40}$/i,
					/^[A-Za-z0-9_]+@dmsswe.com$/i,
					/^[A-Za-z0-9\.@]{5,60}$/,
					/^[0-9]{1,8}$/
				], 
				url_path, csrf_token);
		});
	</script>
</body>
</html>
