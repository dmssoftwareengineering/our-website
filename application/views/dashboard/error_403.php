<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Access Denied | Career Dashboard";
		$this->load->view('dashboard/inc/head', $data);
	?>
	<style>
		.content:first-child {
			padding-top: 100px;
		}
		.error-title {
			color:red
		}
	</style>
</head>

<body>

	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
		
			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>				
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Error title -->
					<div class="text-center content-group">
						<h1 class="error-title">Access Denied</h1>
						<h5>
							Your user account is forbidden to perform this task. <br/> 
							If you require permission for the task, please request it from an administrator. <br/> 
							<a href="asdas">Contact Administrator</a>
						</h5>
					</div>
					<!-- /error title -->


					<!-- Error content -->
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
							<div class="row">
								<div class="col-sm-12">
									<a  href="javascript:history.back()" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Go Back</a>
								</div>
							</div>
						</div>
					</div>
					<!-- /error wrapper -->


					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<?php $this->load->view('dashboard/inc/foot'); ?>
</body>
</html>