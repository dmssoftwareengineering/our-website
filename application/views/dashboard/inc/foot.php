<!-- Core JS files -->
<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/loaders/pace.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/db/js/core/libraries/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/db/js/core/libraries/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/loaders/blockui.min.js');?>"></script>
<script>var base_url = '<?php echo current_url(); ?>';</script>
<script type="text/javascript" src="<?php echo base_url('assets/db/js/core/app.min.js');?>"></script>
<!-- /core JS files -->