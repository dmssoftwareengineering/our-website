<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/db/css/icons/icomoon/styles.css');?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/db/css/bootstrap.css');?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/db/css/core.css');?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/db/css/components.css');?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/db/css/colors.css');?>" rel="stylesheet" type="text/css">

<meta name="robots" content="noindex,nofollow">
<!-- /global stylesheets -->