
<?php if ($user_in == '1') { ?>
<div class="sidebar sidebar-main">
	<div class="sidebar-content">

		<?php
			$navs = array(
				'home' => '', 'resume_v' => '', 'position_v' => '', 'permission_v' => '', 'role_v' => '', 'user_v' => '', 'position_a' => '', 'user_a' => '', 'role_a' => ''
			);
			$main_nav = array(
				'home' => '', 'resume' => '', 'position' => '', 'permission' => '', 'role' => '', 'user' => '', 'interview' => ''
			);
			
			$main_menu = explode("_", $selected_item);
			$main_menu = sizeof($main_menu) == 2 ? $main_menu[0] : $main_menu[0];			
			
			$main_nav[$main_menu] = 'active';
			$navs[$selected_item] = 'active';			
		?>
	
		<!-- User menu -->
		<div class="sidebar-user">
			<div class="category-content">
				<div class="media">
					<div class="media-body">
						<span class="media-heading text-semibold"><?php echo $this->session->userdata('first_name'); ?> <?php echo $this->session->userdata('last_name'); ?></span>
						<div class="text-size-mini text-muted">
							<i class="icon-tree7 text-size-small"></i> <?php echo $this->session->userdata('role_name'); ?> Role
						</div>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list">
							<li>
								<a href="<?php echo base_url('dashboard/profile/settings');?>"><i class="icon-cog3"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /user menu -->

		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">
					<!-- Main -->
					<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
					<li class="<?php echo $main_nav['home']; ?>"><a href="<?php echo base_url('dashboard');?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
					<li class="<?php echo $main_nav['resume']; ?>">
						<a href="#"><i class="icon-user-tie"></i> <span>Applicants</span></a>
						<ul>
							<li><a href="<?php echo base_url('careers/apply'); ?>">Add Applicant</a></li>
							<li class="<?php echo $navs['resume_v']; ?>"><a href="<?php echo base_url('dashboard/applicants');?>">Search Applicant(s)</a></li>
						</ul>
					</li>
					<li class="<?php echo $main_nav['position']; ?>">
						<a href="#"><i class="icon-stack2"></i> <span>Positions</span></a>
						<ul>
							<li class="<?php echo $navs['position_a']; ?>"><a href="<?php echo base_url('dashboard/position/add');?>">Add Position</a></li>
							<li class="<?php echo $navs['position_v']; ?>"><a href="<?php echo base_url('dashboard/positions');?>">Search Position(s)</a></li>
						</ul>
					</li>
					<li class="<?php echo $main_nav['user']; ?>">
						<a href="#"><i class="icon-users"></i> <span>Users</span></a>
						<ul>
							<li class="<?php echo $navs['user_a']; ?>"><a href="<?php echo base_url('dashboard/user/add');?>" id="layout1">Add New User</a></li>
							<li class="<?php echo $navs['user_v']; ?>"><a href="<?php echo base_url('dashboard/users');?>" id="layout3">Search User(s)</a></li>
						</ul>
					</li>
					<li class="<?php echo $main_nav['role']; ?>">
						<a href="#"><i class="icon-droplet2"></i> <span>User Roles</span></a>
						<ul>
							<li class="<?php echo $navs['role_a']; ?>"><a href="<?php echo base_url('dashboard/role/add');?>">Add Role</a></li>
							<li class="<?php echo $navs['role_v']; ?>"><a href="<?php echo base_url('dashboard/roles');?>">Search Role(s)</a></li>
						</ul>
					</li>
					<li class="<?php echo $main_nav['interview']; ?>"><a href="<?php echo base_url('dashboard/interviews');?>"><i class="icon-hat"></i> <span>Interviews</span></a></li>
					<li class="<?php echo $main_nav['permission']; ?>">
						<a href="#"><i class="icon-flip-vertical4"></i> <span>Permissions</span></a>
						<ul>
							<li class="<?php echo $navs['permission_v']; ?>"><a href="<?php echo base_url('dashboard/permissions');?>">Search Permission(s)</a></li>
						</ul>
					</li>
					<!-- /page kits -->
				</ul>
			</div>
		</div>
		<!-- /main navigation -->
	</div>
</div>
<?php } ?>