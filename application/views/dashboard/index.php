<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		} 
		
		if ($user_data['first_time'] == '1') {
			redirect(base_url('dashboard/user/change-password'));
		}
	?>
	<style>
		#interviews{
			min-height:490px
		}
	</style>
</head>

<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header'); ?>
	<!-- /main navbar -->
	
	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "home";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>				
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Home</span> - Dashboard</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Dashboard</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-lg-4">							
							<?php 
								$today_length = sizeof($interviews['today']);
								$week_length = sizeof($interviews['week']);
							?>
							<div id="interviews" class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Scheduled Interviews</h6>
									<div class="heading-elements">
										<span class="heading-text"><i class="icon-history text-warning position-left"></i> <a href="<?php echo base_url('dashboard/interviews'); ?>">All Interviews</a></span>
									</div>
								</div>

								<!-- Numbers -->
								<div class="container-fluid">
									<div class="row text-center">
										<div class="col-md-4">
											<div class="content-group">
												<h6 class="text-semibold no-margin"><i class="icon-clipboard3 position-left text-slate"></i> <?php echo $week_length; ?></h6>
												<span class="text-muted text-size-small">this week</span>
											</div>
										</div>

										<div class="col-md-4">
											<div class="content-group">
												<h6 class="text-semibold no-margin"><i class="icon-calendar3 position-left text-slate"></i> <?php echo $interviews['month']; ?></h6>
												<span class="text-muted text-size-small">this month</span>
											</div>
										</div>

										<div class="col-md-4">
											<div class="content-group">
												<h6 class="text-semibold no-margin"><i class="icon-comments position-left text-slate"></i> <?php echo $interviews['all']; ?></h6>
												<span class="text-muted text-size-small">all interviews</span>
											</div>
										</div>
									</div>
								</div>
								<!-- /numbers -->

								<!-- Area chart -->
								<div id="messages-stats">
									<svg width="100%" height="40"><g transform="translate(0,0)" width="100%"><path class="d3-area" style="fill: rgb(92, 107, 192);" d="M0,6.184118060435697C1.2114595483579291,7.607286167321797,11.856677169423909,25.180704406815508,17.3,26.507378777231203S29.177285546859636,15.98271296940317,34.6,14.6170063246662S47.006648641360925,19.86062394151599,51.900000000000006,17.793394237526353S64.06574894145355,1.8019379860118612,69.2,0S81.48078183742173,3.713128555055727,86.5,5.650035137034433S98.08409767543385,13.890741833909207,103.80000000000001,13.352073085031622S115.89664820931661,0.6772653433259761,121.10000000000001,2.3893183415319763S133.17917110934866,23.048359203279535,138.4,24.736472241742796S151.3486564066683,16.058594114460487,155.7,13.576950105411104S167.47422268923614,3.849780225879287,173,5.00351370344343S184.95788465335247,19.295135632782085,190.29999999999998,20.801124385101897S202.24213056412663,16.237514660630644,207.60000000000002,14.757554462403375S219.13972433551666,11.051981442573238,224.9,11.243851018974S237.34387930119357,13.807260300461175,242.20000000000002,15.91004919184821S253.81994918535182,26.92769710156148,259.5,26.226282501756852S271.04400494801956,11.389543594088062,276.8,11.63738580463809S288.6055909864129,26.493024937364385,294.09999999999997,27.71609276177091S306.25534915105845,21.12829328524932,311.4,19.339423752635277S323.1742226892361,14.531438693904589,328.7,15.685172171468729S340.3475864384567,25.75997759900717,346,26.563598032326073S358.28352245600877,22.544281446378296,363.29999999999995,20.604356992269853S375.6557076870147,15.199862491210538,380.59999999999997,13.183415319747013S392.60648239587897,4.910723921767043,397.90000000000003,6.493323963457485S409.7546450452351,22.20501250596135,415.20000000000005,23.527758257203093S426.90406251506596,15.875543047344268,432.5,14.89810260014055S444.33461613466704,18.767397655430585,449.8,17.484188334504566S461.5197881920549,7.79444847593634,467.1,6.774420238931835S479.17421232919685,9.478296753469586,484.40000000000003,11.159522136331695S495.93675681053503,18.046297446722868,501.7,17.905832747716094S516.5820338200984,11.377007573470411,519,10.316233309908643L519,40C516.1166666666667,40,507.46666666666664,40,501.7,40S490.1666666666667,40,484.40000000000003,40S472.8666666666667,40,467.1,40S455.56666666666666,40,449.8,40S438.26666666666665,40,432.5,40S420.9666666666667,40,415.20000000000005,40S403.66666666666674,40,397.90000000000003,40S386.3666666666667,40,380.59999999999997,40S369.0666666666666,40,363.29999999999995,40S351.76666666666665,40,346,40S334.46666666666664,40,328.7,40S317.16666666666663,40,311.4,40S299.8666666666666,40,294.09999999999997,40S282.56666666666666,40,276.8,40S265.26666666666665,40,259.5,40S247.9666666666667,40,242.20000000000002,40S230.66666666666669,40,224.9,40S213.3666666666667,40,207.60000000000002,40S196.06666666666666,40,190.29999999999998,40S178.76666666666665,40,173,40S161.46666666666664,40,155.7,40S144.16666666666666,40,138.4,40S126.86666666666667,40,121.10000000000001,40S109.56666666666668,40,103.80000000000001,40S92.26666666666667,40,86.5,40S74.96666666666667,40,69.2,40S57.66666666666667,40,51.900000000000006,40S40.36666666666667,40,34.6,40S23.066666666666666,40,17.3,40S2.8833333333333333,40,0,40Z"></path></g></svg>
								</div>
								<!-- /area chart -->

								<!-- Tabs -->
			                	<ul class="nav nav-lg nav-tabs nav-justified no-margin no-border-radius bg-indigo-400 border-top border-top-indigo-300">
									<li class="active">
										<a href="#ints-today" class="text-size-small text-uppercase" data-toggle="tab">
											Today
											<span class="badge bg-danger-400"><?php echo $today_length; ?></span>
										</a>
									</li>

									<li>
										<a href="#ints-week" class="text-size-small text-uppercase" data-toggle="tab">
											This Week
											<span class="badge bg-danger-400"><?php echo $week_length; ?></span>
										</a>
									</li>
								</ul>
								<!-- /tabs -->

								<!-- Tabs content -->
								<div class="tab-content">
									<div class="tab-pane active fade in has-padding" id="ints-today">
										<ul class="media-list">
											<?php if ($today_length == 0){ ?>
												<li class="media">
													<div class="media-body">
														No Interviews Schedule for Today
													</div>
												</li>
											<?php
												}
												for ($i=0;$i<$today_length;$i++) { 											
											?>
											<li class="media">
												<div class="media-body">
													<a href="<?php echo base_url('dashboard/interview/'.$interviews['today'][$i]['int_id']); ?>"><?php echo $interviews['today'][$i]['app_name']; ?></a>
													<span class="media-annotation pull-right"><?php echo $interviews['today'][$i]['interview_dt']; ?></span>

													<span class="display-block text-muted"><?php echo $interviews['today'][$i]['pos_name']; ?></span>
												</div>
											</li>
											<?php } ?>
										</ul>
									</div>

									<div class="tab-pane fade has-padding" id="ints-week">
										<ul class="media-list">
											<ul class="media-list">
												<?php if ($week_length == 0){ ?>
													<li class="media">
														<div class="media-body">
															No Interviews Schedule this Week
														</div>
													</li>
												<?php
													}
													for ($i=0;$i<$week_length;$i++) { 											
												?>
												<li class="media">
													<div class="media-body">
														<a href="<?php echo base_url('dashboard/interview/'.$interviews['week'][$i]['int_id']); ?>"><?php echo $interviews['week'][$i]['app_name']; ?></a>
														<span class="media-annotation pull-right"><?php echo $interviews['week'][$i]['interview_dt']; ?></span>

														<span class="display-block text-muted"><?php echo $interviews['week'][$i]['pos_name']; ?></span>
													</div>
												</li>
												<?php } ?>
											</ul>
										</ul>
									</div>
								</div>
								<!-- /tabs content -->
							</div>
						</div>
						
						<div class="col-lg-4">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Overall Resume and Recruitment</h6>
									<div class="heading-elements">
										<span class="heading-text"><i class="icon-history text-warning position-left"></i> <a href="<?php echo base_url('dashboard/applicants'); ?>">All Applicants</a></span>
									</div>
								</div>

								<div class="panel-body">
									<div class="chart-container">
										<div id="resume_overall" class="chart has-fixed-height"></div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-4">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Resumes for Open Positions</h6>
									<div class="heading-elements">
										<span class="heading-text"><i class="icon-history text-warning position-left"></i> <a href="<?php echo base_url('dashboard/positions'); ?>">All Positions</a></span>
									</div>
								</div>

								<div class="panel-body">
									<div class="chart-container">
										<div id="open_positions" class="chart has-fixed-height"></div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<?php $this->load->view('dashboard/inc/foot'); ?>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/visualization/echarts/echarts.js');?>"></script>
	
	<script type="text/javascript">
		$(function () {	
			var ro_pos_names = [<?php echo $overall_recruit['positions']; ?>],
				ro_resumes = [<?php echo $overall_recruit['resumes']; ?>],
				ro_recruited = [<?php echo $overall_recruit['recruited']; ?>];
				
			var op_resumes = [<?php echo $overall_recruit['resumes']; ?>],
				op_pos_names = [<?php echo $overall_recruit['positions']; ?>];
				
			var op_data = [];			
			var op_data_length = op_resumes.length;
			
			for (var i=0;i<op_data_length;i++) {
				op_data.push({
					value : op_resumes[i],
					name : op_pos_names[i]
				});
			}	
		
			require.config({
				paths: {
					echarts: '<?php echo base_url('assets/db/js/plugins/visualization/echarts');?>'
				}
			});
					
			require(
				[
					'echarts',
					'echarts/theme/limitless',
					'echarts/chart/bar',
					'echarts/chart/line',
					'echarts/chart/pie',
				],
				
				function (ec, limitless) {
					var resume_overall = ec.init(document.getElementById('resume_overall'), limitless);
					var open_positions = ec.init(document.getElementById('open_positions'), limitless);

					resume_overall_options = {
						grid: {
							x: 35,
							x2: 10,
							y: 35,
							y2: 25
						},

						tooltip: {
							trigger: 'axis',
							axisPointer: {
								type: 'shadow' 
							},
							formatter: function (params) {
								return params[0].name + '<br/>'
								+ params[0].seriesName + ': ' + params[0].value + '<br/>'
								+ params[1].seriesName + ': ' + (params[1].value + params[0].value);
							}
						},

						legend: {
							selectedMode: false,
							data: ['Resumes', 'Recruited']
						},

						calculable: false,

						xAxis: [{
							type: 'category',
							data: ro_pos_names
						}],

						yAxis: [{
							type: 'value',
							boundaryGap: [0, 0.1]
						}],

						series: [
							{
								name: 'Resumes',
								type: 'bar',
								stack: 'sum',
								barCategoryGap: '50%',
								itemStyle: {
									normal: {
										color: '#0070c0',
										barBorderColor: '#0070c0',
										barBorderWidth: 6,
										label: {
											show: true,
											position: 'insideTop'
										}
									},
									emphasis: {
										color: '#0070c0',
										barBorderColor: '#0070c0',
										barBorderWidth: 6,
										label: {
											show: true,
											textStyle: {
												color: '#fff'
											}
										}
									}
								},
								data: ro_resumes
							},
							{
								name: 'Recruited',
								type: 'bar',
								stack: 'sum',
								itemStyle: {
									normal: {
										color: '#3090e0',
										barBorderColor: '#0070c0',
										barBorderWidth: 6,
										barBorderRadius: 0,
										label: {
											show: true, 
											position: 'top',
											formatter: function (params) {
												var roa_length = resume_overall_options.xAxis[0].data.length;
												
												for (var i = 0, l = roa_length; i < l; i++) {
													if (resume_overall_options.xAxis[0].data[i] == params.name) {
														return resume_overall_options.series[0].data[i] + params.value;
													}
												}
											},
											textStyle: {
												color: '#0070c0'
											}
										}
									},
									emphasis: {
										barBorderColor: '0070c0',
										barBorderWidth: 6,
										label: {
											show: true,
											textStyle: {
												color: '0070c0'
											}
										}
									}
								},
								data:ro_recruited
							}
						]
					};

					open_positions_options = {
						tooltip: {
							trigger: 'item',
							formatter: "{a} <br/>{b}: {c} ({d}%)"
						},

						legend: {
							orient: 'horizontal',
							x: 'left',
							data: op_pos_names
						},

						calculable: false,

						series: [{
							name: 'Resumes for Open Positions',
							type: 'pie',
							radius: '70%',
							center: ['50%', '57.5%'],
							data: op_data
						}]
					};
										
					resume_overall.setOption(resume_overall_options);
					open_positions.setOption(open_positions_options);

					window.onresize = function () {
						setTimeout(function () {
							resume_overall.resize();
							open_positions.resize();
						}, 200);
					}
				}
			);
		});
	</script>
	<!-- /theme JS files -->
</body>
</html>