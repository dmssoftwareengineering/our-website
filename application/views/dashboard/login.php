<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Login";
		$this->load->view('dashboard/inc/head', $data);
		
		if ($user_in) {
			redirect(base_url('dashboard'));
		}
	?>
	<style>
		.login-container .page-container .login-form, .login-container .page-container .registration-form {
			margin:10% auto 20px auto
		}
		
		.validation-error-label{
			display:none
		}
	</style>
</head>

<body class="login-container login-cover">
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content pb-20">
					<div class="panel panel-body login-form">
						<div class="text-center">
							<img src="<?php echo base_url('assets/core/images/logo/footer_logo.png');?>" alt="" class="img-fluid">
							<h5 class="content-group">Career Dashboard <small class="display-block">Enter Your Login Credentials to Proceed</small></h5>
						</div>

						<div class="form-group has-feedback has-feedback-left">
							<input id="email" name="email" type="text" class="form-control" placeholder="Email Address">
							<div class="form-control-feedback">
								<i class="icon-user text-muted"></i>
							</div>
							<label id="email-error" class="validation-error-label">Please Enter a Valid Email Address</label>
						</div>

						<div class="form-group has-feedback has-feedback-left">
							<input id="password" type="password" class="form-control" placeholder="Password">
							<div class="form-control-feedback">
								<i class="icon-lock2 text-muted"></i>
							</div>
							<label id="password-error" class="validation-error-label">Please Enter a Valid Password</label>
						</div>

						<div class="form-group">
							<button id="login" type="submit" class="btn bg-blue btn-block">Login into the Dashboard <i class="icon-arrow-right14 position-right"></i></button>
						</div>
					</div>

				</div>

			</div>

		</div>
	</div>
	<?php $this->load->view('dashboard/inc/foot'); ?>
	<script type="text/javascript" src="<?php echo base_url('assets/core/js/alertify.js');?>"></script>
	<script>
		$(function() {
			var url_path = '<?php echo base_url('dashboard/loginUser');?>';
			$('#login').click(function(){
				sendText_Ajax([
					'email',
					'password'
				], [
					/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w+)+$/i,
					/^[A-Za-z0-9\.@]{5,60}$/i
				], 
				url_path, "", function(response){
					var data = response.data;
					
					if (data['first_time'] === "1") {
						window.location.href = "<?php echo base_url('dashboard/user/change-password'); ?>"
					} else {
						window.location.href = "<?php echo base_url('dashboard'); ?>"						
					}						
				});
			});
		});
	</script>
</body>
</html>
