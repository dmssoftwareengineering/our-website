<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Resumes";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
	?>	
</head>
<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "resume_v";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>	
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Applicants</span></h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="<?php echo base_url('careers/apply'); ?>" class="btn btn-link btn-float has-text"><i class="icon-add text-primary"></i><span>Add Applicant</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url('dashboard'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Applicants</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">

					<!-- Highlighting rows and columns -->
					<div class="panel panel-flat">

						<table class="table table-bordered table-hover datatable-highlight">
							<thead>
								<tr>
									<th>Applicant Name</th>
									<th>Position Applied</th>
									<th>Contact Details</th>
									<th>Date Applied</th>
									<th>Application Status</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									for ($i=0;$i<sizeof($resumes);$i++) { 
								?>
									<tr>
										<td><?php echo $resumes[$i]['first_name']." ".$resumes[$i]['last_name']; ?></td>
										<td><a href="<?php echo base_url('dashboard/position/view/'.$resumes[$i]['pos_id']);?>"><?php echo $resumes[$i]['pos_name']; ?></a></td>
										<td>
											<strong>Mobile:</strong> <?php echo $resumes[$i]['mobile_number']; ?><br/>
											<strong>Email:</strong> <?php echo $resumes[$i]['email']; ?>												
											<?php 
												if ($resumes[$i]['linked_in'] != "") {
													echo '<br/><strong>LinkedIn:</strong> <a href="https://www.linkedin.com/in/'.$resumes[$i]['linked_in'].'" target="_blank">View Profile</a>';
												}
											?>											
										</td>
										<td><?php echo $resumes[$i]['date_created']; ?></td>
										<td>
											<span class="label <?php echo $resumes[$i]['reviewed_id'] == '0' ? 'label-danger' : 'label-success'; ?>"><?php echo $resumes[$i]['reviewed_status']; ?></span>
										</td>
										<td class="text-center">
											<ul class="icons-list">
												<li class="dropdown">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">
														<i class="icon-menu9"></i>
													</a>

													<ul class="dropdown-menu dropdown-menu-right">
														<li><a href="<?php echo base_url('dashboard/applicant/view/'.$resumes[$i]['app_id']); ?>"><i class="icon-user"></i> View Application</a></li>
													</ul>
												</li>
											</ul>
										</td>
									</tr>								
								<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /highlighting rows and columns -->

					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<?php $this->load->view('dashboard/inc/foot'); ?>
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/tables/datatables/datatables.min.js');?>"></script>
	<!-- Theme JS files -->
	<script>		
		$(function() {
			$.extend( $.fn.dataTable.defaults, {
				autoWidth: false,
				columnDefs: [{ 
					orderable: false,
					width: '100px',
					targets: [ 5 ]
				}],
				dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
				language: {
					search: '<span>Search Applicant(s):</span> _INPUT_',
					searchPlaceholder: 'Type to search...',
					lengthMenu: '<span>Show Results:</span> _MENU_',
					paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
				}
			});
			
			var lastIdx = null;
			var table = $('.datatable-highlight').DataTable({
				lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
			});
			 
			$('.datatable-highlight tbody').on('mouseover', 'td', function() {
				var colIdx = table.cell(this).index().column;

				if (colIdx !== lastIdx) {
					$(table.cells().nodes()).removeClass('active');
					$(table.column(colIdx).nodes()).addClass('active');
				}
			}).on('mouseleave', function() {
				$(table.cells().nodes()).removeClass('active');
			});
		});
	</script>
	<!-- /theme JS files -->
</body>
</html>
