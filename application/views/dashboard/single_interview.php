<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Interviews";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
	?>	
	<style>
		.modal-dialog{width:1000px}
		
		#preview-panel {
			border-left:1px solid #e4e1e1;
			padding-left:10px
		}	
		
		.validation-error-label{
			display:none
		}
		
		.interview-hold {
			border-bottom: #2196F3 dashed 1px;
			margin-bottom:10px;
			padding-bottom 10px;
		}
		
		.interview{
			margin:10px
		}
		
		.panel-heading {
			border-top: 1px solid #ddd;
		}
		
		.int-early {
			padding: 0px 23px 10px;
			font-weight: bold;
			color: red;
		}
	</style>
</head>

<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "interview";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>	
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Interview</span> - Detailed View</h4>

							<ul class="breadcrumb position-right">
								<li><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
								<li><a href="<?php echo base_url('dashboard/interviews'); ?>">Interviews</a></li>
								<li class="active">Detailed View</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-4">
							<!-- Task details -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title"><i class="icon-user-tie position-left"></i> Interviewer Details</h6>
								</div>
								
								<table class="table table-borderless table-xs content-group-sm">
									<tbody>
										<tr>
											<td>Position Manager:</td>
											<td class="text-right"><a href="<?php echo base_url('dashboard/user/view/'.$int_data['position_man_id']); ?>"><?php echo $int_data['person_name']; ?></a></td>
										</tr>
										<tr>
											<td>Contact Number:</td>
											<td class="text-right"><?php echo $int_data['person_con']; ?></td>
										</tr>
										<tr>
											<td>Interview Set By:</td>
											<td class="text-right"><a href="<?php echo base_url('dashboard/user/'.$int_data['user_id']); ?>"><?php echo $int_data['first_name']; ?></a></td>
										</tr>
									</tbody>
								</table>
								
								<div class="panel-heading">
									<h6 class="panel-title">Interview Details</h6>
								</div>								
								
								<?php
									//Handle the command buttons and controls
									$btn_commands = array(
										"cancel" => '<div id="btn-cancel" class="btn-group state-btn"><button type="button" class="btn bg-danger" title="Cancel this Interview">Cancel Interview</button></div>',
										"complete" => '<div id="btn-complete" class="btn-group state-btn"><button type="button" class="btn btn-primary" title="Click here if the interview is finished">Complete Interview</button></div>',
										"ongoing" => '<div id="btn-ongoing" class="btn-group state-btn"><button type="button" class="btn bg-slate-700" title="Click here if you started the interview">Interview Ongoing</button></div>',
									);
									
									$text_area = '';
								
									if ($int_data['interview_status'] == 'N' || $int_data['interview_status'] == 'C') {
										$btn_commands['complete'] = '';
										$btn_commands['ongoing'] = '';
										$btn_commands['cancel'] = '';
										
										$text_area = 'readonly';
									} else if ($int_data['interview_status'] == 'O') {
										$btn_commands['cancel'] = '';
										$btn_commands['ongoing'] = '';
									}
								?>
								
								<input id="user_id" type="hidden" value="<?php echo $int_data['int_id'];?>" />
								<label id="user_id-error" class="validation-error-label">Invalid Interview Id</label>								
								
								<input id="state" type="hidden" value="<?php echo $int_data['interview_status'];?>" />
								<label id="state-error" class="validation-error-label">Invalid Interview State</label>

								<table class="table table-borderless table-xs content-group-sm">
									<tbody>
										<tr>
											<td>Interview Id:</td>
											<td class="text-right"><?php echo $int_data['int_id']; ?></td>
										</tr>
										<tr>
											<td>Date and Time:</td>
											<td class="text-right"><?php echo $int_data['interview_dt']; ?></td>
										</tr>
										<tr>
											<td>Applicant Details:</td>
											<td class="text-right"><a href="<?php echo base_url('dashboard/applicant/view/'.$int_data['app_id']); ?>"><?php echo $int_data['app_name']; ?></a></td>
										</tr>
										<tr>
											<td>Interview Status:</td>
											<td class="text-right">
												<span class="label <?php echo $int_data['interview_status'] == 'N' ? 'label-danger' : 'label-success'; ?>"><?php echo $int_data['status']; ?></span>
											</td>
										</tr>	
									</tbody>
								</table>
								
								<div class="row">								
									<div class="col-lg-12">					
										<div class="panel-heading">
											<h6 class="panel-title"><i class="icon-newspaper position-left"></i> Interview Remarks</h6>
										</div>

										<div class="panel-body">
											<textarea id="message" rows="5" cols="5" class="form-control" placeholder="Please enter your remarks on the interview" <?php echo $text_area; ?>><?php echo $int_data['interview_remarks'] == 'Not Set' ? '' :$int_data['interview_remarks'];?></textarea>
											<label id="message-error" class="validation-error-label">Please Enter a Valid Remark</label>
										</div>
									</div>
								</div>
								
								<div class="btn-group btn-group-justified">								
									<?php
										if ($int_data['interview_today'] == '1') {
											echo $btn_commands['cancel'];
											echo $btn_commands['complete'];
											echo $btn_commands['ongoing'];
										} else if ($int_data['interview_today'] == '2'){
											echo "<div class='int-early'>You cannot make changes, the interview date is passed</div>";											
										} else {									
											echo $btn_commands['cancel'];
										} 
									?>										
								</div>
							</div>								
						</div>
						<div class="col-lg-8">
							<div class="row">								
								<div class="col-lg-12">									
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h6 class="panel-title"><i class="icon-newspaper position-left"></i> Applicant Resume Preview</h6>
										</div>

										<div class="panel-body">
											<iframe src="http://docs.google.com/gview?url=<?php echo $app_data['resume_link']; ?>&embedded=true" width="100%" height="100%" style="min-height:1000px;border:0"></iframe>
										</div>
									</div>
								</div>
							</div>							
						</div>
					</div>

					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('dashboard/inc/foot'); ?>	
	
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/notifications/sweet_alert.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/core/js/alertify.js'); ?>"></script>
	
	<script>
		$(function() {
			base_url = '<?php echo current_url(); ?>';
			var url_path = '<?php echo base_url('dashboard/changeInterviewState');?>',
				csrf_token = '<?php echo $csrf_token; ?>',
				user_id	= <?php echo $int_data['user_id']; ?>,
				app_id = <?php echo $int_data['app_id']; ?>,
				int_id = <?php echo $int_data['int_id']; ?>,
				state = '<?php echo $int_data['interview_status'];?>';
				
			$('.state-btn').click(function() {
				var btn_id = $(this)[0].id;
				
				state = btn_id == "btn-complete" ? 'C' : btn_id == "btn-cancel" ? 'N' : 'O';
				$('#state').val(state);
				
				if (state == 'O') $('#message').val('Interview Ongoing');
				
				sendText_Ajax([
					'state',
					'user_id',
					'message'
				], [
					/^(C|N|O)$/,
					/^[0-9]{1,8}$/i,
					/^[A-Za-z0-9+_$# ]{10,}$/i
				],
				url_path, csrf_token, function() {
					swal({
						title: "Interview State",
						text: "Interview State Changed",
						confirmButtonColor: "#66BB6A",
						type: "success"
					});
					window.location.href = base_url;
				});
			});
		});
	</script>
</body>
</html>