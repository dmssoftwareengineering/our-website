<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Positions";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
	?>	
	<style>
		.detail-data .col-md-6 {
			margin-bottom: 20px;
		}
	</style>
</head>

<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "position_v";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>	
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Positions</span> - Detailed View</h4>

							<ul class="breadcrumb position-right">
								<li><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
								<li><a href="<?php echo base_url('dashboard/positions'); ?>">Positions</a></li>
								<li class="active">Detailed View</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">
					<input id="pos-id" type="hidden" value="<?php echo $pos_data['pos_id']; ?>">
					<input id="pos-status" type="hidden" value="<?php echo $pos_data['pos_active'];?>">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-4">
							<!-- Task details -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title"><i class="icon-files-empty position-left"></i> Position Details</h6>
								</div>
								
								<div class="panel-body detail-data">
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-stack3 position-left"></i> Position Id:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $pos_data['pos_id']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-user-ties position-left"></i> Position Name:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $pos_data['pos_name']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-users position-left"></i> Position Manager:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<a href="<?php echo base_url('dashboard/user/view/'.$pos_data['pos_manager_id']); ?>"><?php echo $pos_data['pos_manager']; ?></a>
											</div>
										</div>
									</div>
								</div>

								<table class="table table-borderless table-xs content-group-sm">
									<tbody>										
										<tr>
											<td><i class="icon-folder-search position-left"></i> Application Status:</td>
											<td class="text-right">
												<ul class="list-inline list-inline-condensed heading-text pull-right">
													<li class="dropdown">
														<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">
															<span class="label <?php echo $pos_data['pos_active'] == '0' ? 'label-danger' : 'label-success'; ?>"><?php echo $pos_data['pos_status']; ?></span>
															<span class="caret"></span>
														</a>

														<ul id="pos-change" class="dropdown-menu dropdown-menu-right">
															<li data-pos-status="0" class="<?php echo $pos_data['pos_active'] == '0' ? 'active' : '' ?>"><a href="#">Close Position</a></li>
															<li data-pos-status="1" class="<?php echo $pos_data['pos_active'] == '1' ? 'active' : '' ?>"><a href="#">Open Position</a></li>
														</ul>
													</li>
												</ul>
											</td>
										</tr>	
									</tbody>
								</table>
							</div>																
							<!-- /task details -->							
						</div>
						<div class="col-lg-8">
							<div class="row">								
								<div class="col-lg-12">									
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h6 class="panel-title"><i class="icon-newspaper position-left"></i> Position Requirements</h6>
										</div>

										<div id="pos-req" class="panel-body">
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /detailed task -->

					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
	<?php $this->load->view('dashboard/inc/foot'); ?>	
	<script type="text/javascript" src="<?php echo base_url('assets/core/js/alertify.js'); ?>"></script>
	<script>		
		$(function() {				
			var pos_data = '<?php echo $pos_data['pos_require']; ?>',
				pos_status = <?php echo (int)$pos_data['pos_active']; ?>,
				url_path = '<?php echo base_url('dashboard/changePositionStatus');?>',
				csrf_token = '<?php echo $csrf_token; ?>';
			
			pos_data = htmlspecialchars_decode(pos_data);
			$('#pos-req').html(pos_data);								

			$('#pos-change li').click(function(){	
				var sel_status = $(this)[0].attributes['data-pos-status'].value;
				
				if (pos_status == sel_status) return;
				
				$('#pos-status').val(sel_status);
				
				sendText_Ajax([
					'pos-id',
					'pos-status'
				], [
					/^[0-9]{1,8}$/i,
					/^[0-9]{1,2}$/i
				], 
				url_path, csrf_token);
			});
		});		
	</script>
</body>
</html>