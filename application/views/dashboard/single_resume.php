<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Resumes";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
	?>	
	<style>
		.modal-dialog{width:1000px}
		
		#preview-panel {
			border-left:1px solid #e4e1e1;
			padding-left:10px
		}	
		
		.validation-error-label{
			display:none
		}
		
		.interview-hold {
			border-bottom: #2196F3 dashed 1px;
			margin-bottom:10px;
			padding-bottom 10px;
		}
		
		.interview{
			margin:10px
		}
		
		.detail-data .col-md-6 {
			margin-bottom: 20px;
		}
	</style>
</head>

<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "resume_v";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>	
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Applicants</span> - Detailed View</h4>

							<ul class="breadcrumb position-right">
								<li><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
								<li><a href="<?php echo base_url('dashboard/applicants'); ?>">Applicants</a></li>
								<li class="active">Detailed View</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">					
					<input id="pos-id" type="hidden" value="<?php echo $user_data['app_id']; ?>">
					<input id="pos-status" type="hidden" value="<?php echo $user_data['reviewed_id'];?>">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-4">
							<!-- Task details -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title"><i class="icon-files-empty position-left"></i> Applicant Details</h6>
								</div>
								
								<div class="panel-body detail-data">
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-user-tie position-left"></i> First Name:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['first_name']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-user-ties position-left"></i> Last Name:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['last_name']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-calendar position-left"></i> Date of Birth:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												N/A
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-phone2 position-left"></i> Mobile Number:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['mobile_number']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-mail5 position-left"></i> Email Address:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['email']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-linkedin position-left"></i> LinkedIn:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php
												if (!empty($user_data['linked_in'])) {
													echo '<a href="https://www.linkedin.com/in/'.$user_data['linked_in'].'" target="_blank">View Profile</a>';
												} else {
													echo 'N/A';
												}
											?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-stack2 position-left"></i> Position Applied:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<a href="<?php echo base_url('dashboard/position/'.$user_data['pos_id']); ?>"><?php echo $user_data['pos_name']; ?></a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-calendar2 position-left"></i> Date Applied:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['date_created']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-file-eye2 position-left"></i> Application Status:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<ul class="list-inline list-inline-condensed heading-text pull-right">
													<li class="dropdown">
														<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">
															<span class="label <?php echo $user_data['reviewed_id'] == '0' ? 'label-danger' : 'label-success'; ?>"><?php echo $user_data['reviewed_status']; ?></span>
															<span class="caret"></span>
														</a>

														<ul id="app-change" class="dropdown-menu dropdown-menu-right">
															<li data-app-status="0" class="<?php echo $user_data['reviewed_id'] == '0' ? 'active' : '' ?>"><a href="#">Not Reviewed</a></li>
															<li data-app-status="1" class="<?php echo $user_data['reviewed_id'] == '1' ? 'active' : '' ?>"><a href="#">Reviewed</a></li>
															<li data-app-status="3" class="<?php echo $user_data['reviewed_id'] == '3' ? 'active' : '' ?>"><a href="#">Recruited</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>		
							
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title"><i class="icon-users2 position-left"></i> Applicant Interview(s)</h6>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-12">
											<div class="media-body">
												<div class="media-heading interview-hold">													
													<?php 
														if (sizeof($interviews) > 0){
															for ($i=0;$i<sizeof($interviews);$i++) { 
																echo '<div class="interview">
																	With <strong>'.$interviews[$i]['person_name'].'</strong> on '.$interviews[$i]['interview_dt'].' <strong>-></strong> <a href="'.base_url('dashboard/interview/'.$interviews[$i]['interview_id']).'"><strong>See Interview</strong></a>
																</div>';										
															}
														} else {
															echo '<div class="interview">No Interviews Scheduled</div>';
														}														
													?>
												</div>
											</div>										
										</div>
									</div>
									
									<div class="media-body">
										<p class="media-heading">Send an email to '<?php echo $user_data['first_name']; ?>' requesting an interview. <br/> 
											<button class="btn btn-primary" data-toggle="modal" data-target="#modal_form_vertical">Click to Prepare Email</button>
										</p>
									</div>	
								</div>
							</div>
							
							<div id="modal_form_vertical" class="modal fade">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Request Interview VIA Email</h5>
										</div>

										<input id="app_id" type="hidden" value="<?php echo $user_data['app_id']; ?>">
										<input id="user_id" type="hidden" value="<?php echo $this->session->userdata('user_id'); ?>">
										<div class="modal-body">
											<div class="row">
												<div class="col-sm-4">
													<h5 class="modal-title">Date and Time</h5>
													<div class="form-group">
														<div class="row">
															<div class="col-sm-6">
																<label>Date for Interview:</label>
																<input id="int_date" name="date" class="form-control" type="date">
																<label id="int_date-error" class="validation-error-label">Please Select a Date for the Interview</label>
															</div>

															<div class="col-sm-6">
																<label>Time for Interview:</label>
																<input id="int_time" name="time" class="form-control" type="time">
																<label id="int_time-error" class="validation-error-label">Please Select a Time for the Interview</label>
															</div>
														</div>
													</div>
													
													<h5 class="modal-title">Contact Person</h5>
													<div class="form-group">
														<div class="row">
															<div class="col-sm-6">
																<label>Person Name:</label>																
																<select id="sel-pos-manager" data-placeholder="Please select a manager" class="select">
																	<?php for ($i=0;$i<sizeof($users);$i++) { 	?>			
																		<option value="<?php echo $users[$i]['user_id'];?>" data-name="<?php echo $users[$i]['first_name'];?>"><?php echo $users[$i]['first_name'];?></option>
																	<?php } ?>
																</select>
																
																<input id="pos-manager" class="form-control text-light" placeholder="Contact Person Name" type="hidden" value="<?php echo $users[0]['user_id'];?>">
																<label id="pos-manager-error" class="validation-error-label">Please Enter a Valid Person Name</label>
															</div>
															<div class="col-sm-6">
																<label>Contact Number:</label>
																<input id="mobile_number" class="form-control text-light" placeholder="Contact Number" type="text">
																<label id="mobile_number-error" class="validation-error-label">Please Enter a Valid Contact Number</label>
															</div>
														</div>
													</div>
												</div>
												<div class="col-sm-8">
													<div id="preview-panel">
														<h5 class="modal-title">Email Preview</h5><br/>
														<p>
															<strong>Dear <?php echo $user_data['first_name']; ?></strong>,<br/><br/>									
															Thank you for applying to DMS Software Engineering. <br/><br/>	
															Your application for the '<?php echo $user_data['pos_name']; ?>' position stood out to us and we would like to invite you for an interview at our office.
														</p>		
														
														<p>You will meet with <span class="person-name">person_name</span>. The interview will last about 60 minutes. You’ll have the chance to discuss the position’s responsibilities and learn more about our company.</p>
														
														<p>We would like to conduct your interview <strong><span id="date-time">date_time</span></strong>, please let me know your availability. I will schedule the interview once I receive your reply.</p>
														
														<p>Our offices are located at <strong>380 Koswatta Road, Kalapaluwawa, Rajagiriya</strong> (<a href="https://goo.gl/maps/QdzUWTAMYj62" target="_blank">Location</a>). You can find an attached screenshot of our exact location. <br/>Please bring your NIC, so you can receive a visitor’s pass at the reception.</p>
														
														For furthur information please contact <span class="person-name">person_name</span> on <span id="person-number">phone_number</span>.<br/><br/>	
														<strong>Best Wishes</strong><br/>
														DMS Software Engineering
													</div>
												</div>
											</div>												
										</div>

										<div class="modal-footer">
											<button id="btn-request-interview" class="btn btn-primary">Send Interview Request</button>
										</div>
									</div>
								</div>
							</div>
							<!-- Attached files -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title"><i class="icon-link position-left"></i> Applicant Resume(s)</h6>
								</div>

								<div class="panel-body">
									<ul class="media-list">
										<li class="media">
											<div class="media-left media-middle">
												<i class="icon-file-pdf icon-2x text-muted"></i>
											</div>

											<div class="media-body">
												<p class="media-heading text-semibold"><?php echo $user_data['first_name']."_resume"; ?></p>
												<ul class="list-inline list-inline-separate list-inline-condensed text-size-mini text-muted">
													<li>Keep this file private</li>
												</ul>
											</div>

											<div class="media-right media-middle">
												<ul class="icons-list">
													<li><a href="<?php echo $user_data['resume_link']; ?>"><i class="icon-download"></i></a></li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- /attached files -->
							<!-- /task details -->							
						</div>
						<div class="col-lg-8">
							<div class="row">								
								<div class="col-lg-12">									
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h6 class="panel-title"><i class="icon-newspaper position-left"></i> Applicant Resume Preview</h6>
										</div>

										<div class="panel-body">
											<iframe src="https://docs.google.com/gview?url=<?php echo $user_data['resume_link']; ?>&embedded=true" width="100%" height="100%" style="min-height:1000px;border:0"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('dashboard/inc/foot'); ?>	
	
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/core/libraries/jasny_bootstrap.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/notifications/bootbox.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/notifications/sweet_alert.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/forms/selects/select2.min.js');?>"></script>	
	<script type="text/javascript" src="<?php echo base_url('assets/core/js/alertify.js'); ?>"></script>
	
	<script>
		$(function() {
			base_url = '<?php echo base_url('dashboard/interviews'); ?>';
			var url_path = '<?php echo base_url('dashboard/setInterview');?>',
				csrf_token = '<?php echo $csrf_token; ?>',
				user_id	= <?php echo $this->session->userdata('user_id'); ?>,
				app_path = '<?php echo base_url('dashboard/changeAppState');?>',
				app_status = <?php echo (int)$user_data['reviewed_id']; ?>;				
				
			/* Application Status - Start */
			$('#app-change li').click(function(){	
				var sel_status = $(this)[0].attributes['data-app-status'].value;
				
				if (app_status == sel_status) return;
				
				$('#pos-status').val(sel_status);
				
				sendText_Ajax([
					'pos-id',
					'pos-status'
				], [
					/^[0-9]{1,8}$/i,
					/^[0-9]{1,2}$/i
				], 
				app_path, csrf_token, function(){										
					swal({
						title: "Application Status",
						text: "Application State has been changed",
						confirmButtonColor: "#66BB6A",
						type: "success"
					});
				});
			});
			/* Application Status - End */
			
			/* Interview - Start */
			$('.select').select2();
			$('#sel-pos-manager').on('select2:select', function (e) {
				var manager_id = $('#sel-pos-manager :selected').val(),
					manager_name = $('#sel-pos-manager :selected').text();
				
				$('.person-name').html(manager_name);		
				$('#pos-manager').val(manager_id);
			});
			
			$('#onshown_callback').on('click', function() {
				$('#modal_form_vertical').on('shown.bs.modal', function() {});
			});
			
			$('#pos-manager, #mobile_number, #int_date, #int_time').keyup(function() {
				var con_name = $('#pos-manager').val(),
					con_number = $('#mobile_number').val(),
					cur_d = $('#int_date').val(),
					cur_t = $('#int_time').val();
				
				$('.person-name').html(con_name);
				$('#person-number').html(con_number);				
				$('#date-time').html(cur_d+'   '+cur_t);
			});
			
			$('#btn-request-interview').click(function() {				
				sendText_Ajax([
					'int_date',
					'int_time',
					'pos-manager',
					'mobile_number',
					'user_id',
					'app_id'
				], [
					/^(201)\d{1}-(0[1-9]|1[0-2])-(0[1-9]|1\d|2\d|3[01])$/i,
					/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/i,
					/^[0-9]{1,8}$/i,
					/^[0-9]{10}$/i,
					/^[0-9]{1,8}$/i,
					/^[0-9]{1,8}$/i
				], 
				url_path, csrf_token, function(){					
					$('.modal-backdrop, #modal_form_vertical').hide();
					
					swal({
						title: "Interview Request",
						text: "Email has been sent to the applicant",
						confirmButtonColor: "#66BB6A",
						type: "success"
					});
				});
			});
			/* Interview - End */
		});		
	</script>
</body>
</html>