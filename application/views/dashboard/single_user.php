<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - Users";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
	?>	
	<style>
		.detail-data .col-md-6 {
			margin-bottom: 20px;
		}
	</style>
</head>

<body>
	<!-- Main navbar -->
	<?php $this->load->view('dashboard/inc/header') ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php 
				$data["selected_item"] = "user_v";
				$this->load->view('dashboard/inc/sidebar', $data); 
			?>	
			<!-- /main sidebar -->

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left" onclick="javascript:history.back()"></i>  <span class="text-semibold">Users</span> - Detailed View</h4>

							<ul class="breadcrumb position-right">
								<li><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
								<li><a href="<?php echo base_url('dashboard/Users'); ?>">Users</a></li>
								<li class="active">Detailed View</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">
					<!-- Detailed task -->
					<div class="row">
						<div class="col-lg-4">
							<!-- Task details -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title"><i class="icon-files-empty position-left"></i> User Details</h6>
								</div>								
								<div class="panel-body detail-data">
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-user-ties position-left"></i> First Name:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['first_name']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-user-ties position-left"></i> Last Name:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['last_name']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-user-ties position-left"></i> Email Address:
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['email_address']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-user-ties position-left"></i> Role
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<?php echo $user_data['role_name']; ?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="media-body">
												<i class="icon-user-ties position-left"></i> User Initiated
											</div>
										</div>
										<div class="col-md-6">
											<div class="media-body text-right">
												<span class="label <?php echo $user_data['first_time'] == '1' ? 'label-danger' : 'label-success'; ?>"><strong><?php echo $user_data['first_time'] == '1' ? 'No' : 'Yes'; ?></strong></span>
											</div>
										</div>
									</div>
								</div>

								<div class="panel-heading">
									<h6 class="panel-title"><i class="icon-users position-left"></i> Role Permissions</h6>
								</div>
								<div class="panel-body detail-data">
									<?php
										$modules = array();
										$perms = array();
										
										$prev_mod = '';

										$rp_size = sizeof($user_data["role_permission"]);
										
										for ($i=0;$i<$rp_size;$i++) {	
											$module_name = $user_data["role_permission"][$i]['module'];											
											if ($module_name != $prev_mod) {	
												$modules[] = array(
													"mod_name" => $module_name,
													"permissions" => array()
												);
												$perms = array();
											}		
											
											array_push($modules[sizeof($modules)-1]['permissions'], array(
												"name" => $user_data["role_permission"][$i]['perm_name']
											));
																								
											$prev_mod = $module_name;
										}
									?>
									<?php for ($i=0;$i<sizeof($modules);$i++) { ?>												
										<div class="panel panel-white">
											<div class="panel-heading">
												<h6 class="panel-title">
													<a class="collapsed" data-toggle="collapse" data-parent="#accordion-control" href="#permission-mod-<?php echo $i; ?>" aria-expanded="false"><?php echo $modules[$i]['mod_name']; ?></a>
												</h6>
											</div>	
											<div id="permission-mod-<?php echo $i; ?>" class="panel-collapse collapse">
											<?php for ($j=0;$j<sizeof($modules[$i]['permissions']);$j++) { ?>														
												<div class="panel-body list-group-item">														
													<?php echo $modules[$i]['permissions'][$j]['name']; ?>
												</div>														
											<?php }?>	
											</div>
										</div>
									<?php } ?>
								</div>
							</div>																
							<!-- /task details -->							
						</div>
						<div class="col-lg-8">
							<div class="row">								
								<div class="col-lg-12">			
									<div class="panel panel-flat">	
										<div class="panel-heading">
											<h6 class="panel-title"><i class="icon-newspaper position-left"></i> User Activity</h6>
										</div>

										<table class="table table-bordered table-hover datatable-highlight">
											<thead>
												<tr>
													<th>Activity</th>
													<th>Date/Time</th>
													<th>IP Address</th>
												</tr>
											</thead>
											
											<tbody>
												<?php 
													$us_size = sizeof($user_activity);
													for ($i=0;$i<$us_size;$i++) { 												
												?>
													<tr>
														<td><?php echo $user_activity[$i]['activity']; ?></td>
														<td><?php echo $user_activity[$i]['time_created']; ?></td>
														<td><?php echo $user_activity[$i]['ip_address']; ?></td>
													</tr>
												<?php } ?>								
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /detailed task -->

					<!-- Footer -->
					<?php $this->load->view('dashboard/inc/footer'); ?>
					<!-- /footer -->
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<?php $this->load->view('dashboard/inc/foot'); ?>	
	<script type="text/javascript" src="<?php echo base_url('assets/db/js/plugins/tables/datatables/datatables.min.js');?>"></script>
	<script>		
		$(function() {
			$('.datatable-highlight').DataTable();	
		});
	</script>
</body>
</html>