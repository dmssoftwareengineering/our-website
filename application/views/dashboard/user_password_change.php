<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		$data["title"] = "Career Dashboard - User Password Change";
		$this->load->view('dashboard/inc/head', $data);
		
		if (!$user_in) {
			redirect(base_url('dashboard/login'));
		}
		
		if ($user_data['first_time'] == '0') {
			redirect(base_url('dashboard'));
		}
	?>
	<style>
		.login-container .page-container .login-form, .login-container .page-container .registration-form {
			margin:10% auto 20px auto
		}
		
		.validation-error-label{
			display:none
		}
	</style>
</head>

<body class="login-container login-cover">
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content pb-20">
					<div class="panel panel-body login-form">
						<div class="text-center">
							<img src="<?php echo base_url('assets/core/images/logo/footer_logo.png');?>" alt="" class="img-fluid">
							<h5 class="content-group">Career Dashboard <small class="display-block">Since this is your first login attempt,<br/> Please set a new password. </small></h5>
						</div>

						<div class="form-group has-feedback has-feedback-left">
							<input id="email" name="email" type="text" class="form-control" placeholder="Email Address" value="<?php echo $user_data['email']; ?>" disabled="true">
						</div>

						<div class="form-group has-feedback has-feedback-left">
							<input id="password" type="password" class="form-control" placeholder="Password">
							<div class="form-control-feedback">
								<i class="icon-lock2 text-muted"></i>
							</div>
							<label id="password-error" class="validation-error-label">Please Enter a Valid Password</label>
						</div>
						
						<div class="form-group has-feedback has-feedback-left">
							<input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password">
							<div class="form-control-feedback">
								<i class="icon-lock2 text-muted"></i>
							</div>
							<label id="password-confirm-error" class="validation-error-label">Doesn't Match with the Password</label>
						</div>

						<div class="form-group">
							<button id="change-password" type="submit" class="btn bg-blue btn-block">Save Password <i class="icon-arrow-right14 position-right"></i></button>
						</div>
					</div>

				</div>

			</div>

		</div>
	</div>
	<?php $this->load->view('dashboard/inc/foot'); ?>
	<script type="text/javascript" src="<?php echo base_url('assets/core/js/alertify.js');?>"></script>
	<script>
		$(function() {
			var url_path = '<?php echo base_url('dashboard/changePassword');?>';
			$('#change-password').click(function(){				
				sendText_Ajax([
					'email',
					'password',
					'password-confirm'
				], [
					/^[A-Za-z0-9_]+@dmsswe.com$/,
					/^[A-Za-z0-9\.@]{5,60}$/i,
					'confirm|password'
				], 
				url_path, "", function(response){
					window.location.href = "<?php echo base_url('dashboard/login'); ?>"
				});
			});
		});
	</script>
</body>
</html>
