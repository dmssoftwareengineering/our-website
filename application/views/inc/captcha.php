<script>
	function onloadCallback(){
		grecaptcha.execute();
		sendText_Ajax(
			['com_name','person_name','email','contact_number','message'], 
			[/^[A-Za-z0-9 ]{3,20}$/i,/^[A-Za-z ]{5,20}$/i,/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w+)+$/i,/^([+][0-9]{2})[0-9]{8,10}$/i,/^[A-Za-z0-9+_$# ]{20,250}$/i], 
			'<?php echo base_url('contact/requestService');?>','<?php echo $csrf_token; ?>','<?php echo $type; ?>'
		);
	}
	
	$(function ($) {
		$('#btn-reqInfo').click(function() {
			onloadCallback();
		});
	});
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>