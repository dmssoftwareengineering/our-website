<footer id="tw-footer" class="tw-footer" style="background-image:url('<?php echo base_url('assets/core/images/footer_mountain.png');?>')">
  <div class="container">
	 <div class="row">				
		  <div class="col-md-2">
			 <div class="footer-widget footer-left-widget">								
				<ul>
				  <li><strong>REGISTERED OFFICE</strong><br/></li>
				   <li class="contact-foot">
					No. 221/5,<br/>Dharmapala Mawatha, <br/>
					Colombo 07,<br/>
					Sri Lanka.
				   </li>
				</ul>
			 </div>
		  </div>
		  <div class="col-md-3">
			 <div class="footer-widget footer-left-widget">			
				<ul>
				   <li><strong>CORPORATE OFFICE</strong><br/></li>
				   <li class="contact-foot">				
					No. 380, Koswatta Road,<br/>
					Kalapaluwawa, <br/>
					Rajagiriya (10107),<br/>
					Sri Lanka.
				   </li>
				   
				   <li class="contact-foot"><br/>
					Hot Line : +94 11 287 6700<br/>
					Fixed Lines : +94 11 287 6875/6878<br/>
					Fax : +94 11 287 6877<br/>
					Email: info@dmsswe.com
				   </li>
				</ul>
			 </div>
			</div>
			
			<div class="col-md-2">
			 <div class="footer-widget footer-left-widget">	
				<ul class="contact-foot">
					<li><a href="<?php echo base_url('products'); ?>"><strong>WHAT WE DO</strong></a></li>
					<li class="contact-foot"><a href="<?php echo base_url('products/#bank'); ?>">Business Solutions</a></li>
					<li class="contact-foot"><a href="<?php echo base_url('what-we-do/tailored-application-software-development'); ?>">Software Development</a></li>
					<li class="contact-foot"><a href="<?php echo base_url('what-we-do/creative-web-design-and-development'); ?>">Web Design</a></li>
					<li class="contact-foot"><a href="<?php echo base_url('what-we-do/mobile-development'); ?>">Mobile Apps</a></li>
					<li class="contact-foot"><a href="<?php echo base_url('what-we-do/cloud-solutions'); ?>">Cloud Solutions</a></li>
					<li class="contact-foot"><a href="<?php echo base_url('what-we-do/security-solutions'); ?>">Security Solutions</a></li>
					<li class="contact-foot"><a href="<?php echo base_url('what-we-do/payroll-and-bpo-services'); ?>">Payroll BPO Services</a></li>
					<li class="contact-foot"><a href="<?php echo base_url('what-we-do/data-capturing-and-digitizing'); ?>">Document Digitization</a></li>
					<li class="contact-foot"><a href="<?php echo base_url('what-we-do/managed-services'); ?>">Managed Services</a></li>
				</ul>	
			 </div>
			</div>
			
			<div class="col-md-2">
			 <div class="footer-widget footer-left-widget">	
				<ul>
				   <li><a href="<?php echo base_url('careers'); ?>"><strong>CAREERS</strong></a></li>
				   <li class="contact-foot"><a href="<?php echo base_url('careers'); ?>">Inside DMS</a></li>
				   <li class="contact-foot"><a href="<?php echo base_url('careers/apply'); ?>">Career Opportunities</a></li>
				</ul>
			 </div>
			</div>		
			
			<div class="col-md-2">
			 <div class="footer-widget footer-left-widget">		
				
				<ul class="foot-follow">						
				   <li><strong>CONNECT WITH US</strong></li>
				   <li style="float:left;margin-right:5px;"><a href="https://www.facebook.com/dmsswe/" target="_blank" class="fa fa-facebook"></a> </li>
				   <li style="float:left;margin-right:5px;"><a href="https://www.linkedin.com/company/dms-software-engineering-pvt-ltd" target="_blank" class="fa fa-linkedin"></a> </li>
				   <li style="float:left;margin-right:5px;"><a href="https://www.linkedin.com/company/dms-software-engineering-pvt-ltd" target="_blank" class="fa fa-google-plus"></a> </li>
				</ul>
			 </div>
		  </div>   
	 </div>
	 <div class="row">
		<div class="col-md-12">
			<div class="foot-copyright">Copyright © <?php echo date('Y'); ?> <strong>DMS Software Engineering</strong>. All Rights Reserved</div>
			<div class="foot-creator">Designed with <i class="fa fa-heart" aria-hidden="true"></i> by DMS Software Engineering.</div>
		</div>
	 </div>
  </div>
  <div id="back-to-top" class="back-to-top">
	 <button class="btn btn-dark" title="Back to Top">
		<i class="fa fa-angle-up"></i>
	 </button>
  </div>
</footer>
   
<?php
	$this->load->view('inc/foot');		
?>