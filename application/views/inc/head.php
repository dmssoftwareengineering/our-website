<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title><?php echo $title;?></title> 

<meta name="author" content="Basura Ratnayake">	
<meta name="copyright" content="Copyright <?php echo date('Y'); ?> DMS Software Engineering. All Rights Reserved."/>

<!-- SEO Start -->
<meta name="description" content="<?php echo $description;?>">
<meta name="keywords" content="<?php echo $keywords;?>" />

<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo base_url(uri_string());?>" />
<meta property="og:title" content="<?php echo $title;?>" />	

<?php 
	$image_script = base_url('assets/core/images/logo/logo.png');//Need to replace this with a big logo
	if (isset($new_image)) {
		$image_script = $new_image;
	}
?>

<meta property="og:image" content="<?php echo $image_script; ?>">
<meta property="og:site_name" content="DMS Software Engineering" />
<meta property="og:locale" content="en_US" />
<meta property="og:description" content="<?php echo $description;?>" />	
<meta property="fb:pages" content="201829626513462" />

<link rel="canonical" href="<?php echo base_url(uri_string());?>" />

<script type='application/ld+json'>{"@context":"http://schema.org","@type":"Organization","url":"<?php echo base_url(uri_string());?>","@id":"#organization","name":"DMS Software Engineering","logo":"<?php echo $image_script; ?>","contactPoint":[{"@type":"ContactPoint","email":"mailto:info@dmsswe.com","telephone":"+94-11-287-6700","contactType":"Information"}]}</script>
<!-- SEO End -->

<!-- Core CSS - Start -->
<link rel="stylesheet" href="<?php echo base_url('assets/core/css/font-awesome.min.css')?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/core/css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/core/css/plugins.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/core/css/aos.css'); ?>" />

<link rel="stylesheet" href="<?php echo base_url('assets/core/css/core.min.css'); ?>" />
<!-- Core CSS - End -->

<link rel="shortcut icon" href="<?php echo base_url('assets/core/images/logo/dmsswe.ico'); ?>" type="image/x-icon" />

<link rel="apple-touch-icon" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon.png');?>" />
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon-57x57.png');?>" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon-72x72.png');?>" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon-76x76.png');?>" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon-114x114.png');?>" />
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon-120x120.png');?>" />
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon-144x144.png');?>" />
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon-152x152.png');?>" />
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/core/images/logo/apple-touch-icon-180x180.png');?>" />

<meta name="application-name" content="DMS Software Engineering"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="<?php echo base_url('assets/core/images/logo/mstile-144x144.png');?>" />
<meta name="msapplication-square70x70logo" content="<?php echo base_url('assets/core/images/logo/mstile-70x70.png');?>" />
<meta name="msapplication-square150x150logo" content="<?php echo base_url('assets/core/images/logo/mstile-150x150.png');?>" />
<meta name="msapplication-wide310x150logo" content="<?php echo base_url('assets/core/images/logo/mstile-310x150.png');?>" />
<meta name="msapplication-square310x310logo" content="<?php echo base_url('assets/core/images/logo/mstile-310x310.png');?>" />

<!--[if lt IE 9]>
<script src="<?php echo base_url('assets/core/js/html5shiv.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/core/js/respond.min.js');?>"></script>
<![endif]-->