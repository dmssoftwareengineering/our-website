<?php $this->load->view('inc/preloader'); ?>

<!--<div class="search-bar">
  <i class="fa fa-close"></i>
  <form class="search-bar-fixed" action="#">
	 <input type="text" placeholder="search...">
	 <button type="submit"><i class="fa fa-search"></i></button>
  </form>
</div>-->

<!--<div class="offcanvas-menu">
  <div class="offcanvas-menu-overlay"></div>
  <div class="container">
	 <div class="row">
		<div class="col-md-12">
		   <div class="offcanvas-wrapper">
			  <div class="offcanvas-inner">
				 <a href="<?php echo base_url();?>" class="logo">
					<img src="<?php echo base_url('assets/core/images/logo/logo.png'); ?>" alt="">
				 </a>
				 <p>Start working with an company that can provide everything you need to generate awareness, drive traffic,
					connect with customers, and increase sales montes, nascetur ridiculus mus</p>
				 <div class="row">
					<div class="col-md-12">
					   <div class="contact-us">
						  <div class="contact-icon">
							 <i class="icon icon-map2"></i>
						  </div>
						  <div class="contact-info">
							 <h3>380, Koswatta Rd</h3>
							 <p>Kalapaluwawa, Rajagiriya</p>
						  </div>
					   </div>
					</div>
					<div class="col-md-12">
					   <div class="contact-us">
						  <div class="contact-icon">
							 <i class="icon icon-phone3"></i>
						  </div>
						  <div class="contact-info">
							 <h3>(+94) 11 287 6700</h3>
							 <p>Give us a call</p>
						  </div>
					   </div>
					</div>
					<div class="col-md-12">
					   <div class="contact-us">
						  <div class="contact-icon">
							 <i class="icon icon-envelope2"></i>
						  </div>
						  <div class="contact-info">
							 <h3>info@dmsswe.com</h3>
							 <p>Drop us a mail</p>
						  </div>
					   </div>
					</div>
				 </div>
				 <div class="footer-social-link">
					<ul>
					   <li><a href="https://www.facebook.com/dmsswe/" target="_blank"><i class="fa fa-facebook"></i></a></li>
					   <li><a href="https://www.linkedin.com/company/dms-software-engineering-pvt-ltd" target="_blank"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				 </div>
			  </div>
			  <button class="menu-close-btn"><i class="fa fa-close"></i></button>
		   </div>
		</div>
	 </div>
  </div>
</div>-->

<div class="tw-top-bar no-border top-bar-dark">
  <div class="container">
	 <div class="row">
		<div class="col-md-8 text-left">
		   <div class="top-contact-info">
			  <a href="#"><span><i class="icon icon-map-marker2"></i>380, Koswatta Road, Kalapaluwawa, Rajagiriya</span></a>
			  <a href="mailto:info@dmsswe.com"><span><i class="icon icon-envelope"></i>info@dmsswe.com</span></a>
			  <a href="tel:+94112876700"><span><i class="icon icon-phone3"></i>(+94) 11 287 6700</span></a>
		   </div>
		</div>
		<div class="col-md-4 ml-auto text-right">
		   <div class="top-social-links">
			  <a href="https://www.facebook.com/dmsswe/" target="_blank"><i class="fa fa-facebook"></i></a>
			  <a href="https://www.linkedin.com/company/dms-software-engineering-pvt-ltd" target="_blank"><i class="fa fa-linkedin"></i></a>
		   </div>
		</div>
	 </div>
  </div>
</div>

<header>
  <div class="tw-head">
	 <div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-white">
		   <a class="navbar-brand tw-nav-brand" href="<?php echo base_url();?>">
			  <img src="<?php echo base_url('assets/core/images/logo/logo.png'); ?>" alt="DMS Software Engineering">
		   </a>
		   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
			  aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
		   </button>
		   <div id="navbarSupportedContent" class="collapse navbar-collapse justify-content-center">
			  <ul class="navbar-nav">
				 <li class="nav-item <?php echo  $status[0];?>"><a class="nav-link" href="<?php echo base_url();?>">Home</a></li>
				 <li class="nav-item <?php echo  $status[2];?>"><a class="nav-link" href="<?php echo base_url('who-we-are');?>">Who We Are</a></li>
				 <li class="nav-item dropdown">
					<a class="nav-link" href="<?php echo base_url('products'); ?>" data-toggle="dropdown">
					   What We Do
					   <span class="tw-indicator"><i class="fa fa-angle-down"></i></span>
					</a>
					<ul class="dropdown-menu tw-dropdown-menu">
					   <li><a href="<?php echo base_url('products/#bank'); ?>">BUSINESS SOLUTIONS</a></li>
					   <li><a href="<?php echo base_url('what-we-do/tailored-application-software-development'); ?>">SOFTWARE DEVELOPMENT</a></li>
					   <li><a href="<?php echo base_url('what-we-do/creative-web-design-and-development'); ?>">WEB DESIGN</a></li>
					   <li><a href="<?php echo base_url('what-we-do/mobile-development'); ?>">MOBILE APPS</a></li>
					   <li><a href="<?php echo base_url('what-we-do/cloud-solutions'); ?>">CLOUD SOLUTIONS</a></li>
					   <li><a href="<?php echo base_url('what-we-do/security-solutions'); ?>">SECURITY SOLUTIONS</a></li>
					
					   <li class="special-header-li"><a href="<?php echo base_url('what-we-do/payroll-and-bpo-services');?>">PAYROLL BPO SERVICES</a></li>
					   <li class="special-header-li"><a href="<?php echo base_url('what-we-do/data-capturing-and-digitizing');?>">DOCUMENT DIGITIZATION</a></li>
					   <li class="special-header-li"><a href="<?php echo base_url('what-we-do/managed-services'); ?>">MANAGED SERVICES</a></li>
					</ul>
					<!-- End of Dropdown menu -->
				 </li>
				 <li class="nav-item <?php echo  $status[5];?>"><a class="nav-link" href="<?php echo base_url('partners'); ?>">Partners</a></li>
				 <li class="nav-item <?php echo  $status[1];?>"><a class="nav-link" href="<?php echo base_url('reach-us'); ?>">Reach Us</a></li>
			  </ul>
		   </div>
		</nav>
	 </div>
  </div>
  <div id="tw-megamenu" class="dropdown-menu tw-mega-menu">
   <div class="row">
	  <div class="col-md-12 col-lg-2 mega-m-right-border">
		 <ul>
			<li data-id="applications" class="tw-megamenu-title prod-sections selected-prod">
			   <a href="javascript:void(0)"><h4>Business Solutions</h4></a>
			</li>
			<li data-id="software" class="tw-megamenu-title prod-sections">
			   <a href="javascript:void(0)"><h4>Software Development</h4></a>
			</li>
			<li data-id="web" class="tw-megamenu-title prod-sections">
			   <a href="javascript:void(0)"><h4>Web Design</h4></a>
			</li>
			<li data-id="mobile" class="tw-megamenu-title prod-sections">
			   <a href="javascript:void(0)"><h4>Mobile Apps</h4></a>
			</li>
			<li data-id="cloud" class="tw-megamenu-title prod-sections">
			   <a href="javascript:void(0)"><h4>Cloud Solutions</h4></a>
			</li>
			<li data-id="security" class="tw-megamenu-title prod-sections">
			   <a href="javascript:void(0)"><h4>Security Solutions</h4></a>
			</li>
			<li data-id="bpo-nav" class="tw-megamenu-title prod-sections">
			   <a href="javascript:void(0)"><h4>Payroll BPO Services</h4></a>
			</li>
		 </ul>
		 <div class="nav-bottom"></div>
	  </div>
	  <div id="applications" class="col-md-12 col-lg-10 app-holder show">
		<div class="row">
		  <div class="col mega-m-right-border">
			 <ul>
				<li class="tw-megamenu-title">
				   <a href="<?php echo base_url('products/#bank'); ?>"><h3>Banking & Financial<!--<br/><span class="tw-megamenu-subtitle">(Banking & Financial Service Industry)</span>--></h3></a>
				</li>
				<li>
					<a href="<?php echo base_url('products/banksys/1'); ?>">DMS - BANKSYS</a>
					<div class="tw-megamenu-desc">A conventional banking solution for co-operative rural banks</div>
				</li>
				<li>
					<a href="<?php echo base_url('products/imago/2'); ?>">DMS - IMAGO</a>
					<div class="tw-megamenu-desc">Cheque Imaging and Truncation Solution</div>
				</li>
				<li>
					<a href="<?php echo base_url('products/amlock/16'); ?>">DMS-CRIMS</a>
					<div class="tw-megamenu-desc">CRIB information Management System</div>
				</li>
				<li>
					<a href="<?php echo base_url('products/deposit-information-system'); ?>">DMS-DIS</a>
					<div class="tw-megamenu-desc">Deposit Insurance System</div>
				</li>
			 </ul>
		  </div>
		  <div class="col mega-m-right-border">
			 <ul>
				<li class="tw-megamenu-title">
				   <a href="<?php echo base_url('products/#bank'); ?>"><h3>Insurance<!--<br/><span class="tw-megamenu-subtitle">(Banking & Financial Service Industry)</span>--></h3></a>
				</li>
				<li>
					<a href="<?php echo base_url('products/amlock/16'); ?>" class="third-party">PREMIA</a>
					<div class="tw-megamenu-desc">General Insurance</div>
				</li>
				<li>
					<a href="<?php echo base_url('products/banksys/1'); ?>" class="third-party">PREMIA</a>
					<div class="tw-megamenu-desc">Life Insurance</div>
				</li>
				<li>
					<a href="<?php echo base_url('products/banksys/1'); ?>" class="third-party">PREMIA</a>
					<div class="tw-megamenu-desc">Health Insurance</div>
				</li>
			 </ul>
		  </div>
		<div class="col mega-m-right-border">
		 <ul>
			<li class="tw-megamenu-title">
			   <a href="<?php echo base_url('products/#manufacture'); ?>"><h3>Tea Industry</h3></a>
			</li>
			<li>
				<a href="<?php echo base_url('products/teamics'); ?>">DMS - TEAMICS</a>
				<div class="tw-megamenu-desc">Tea Exporters Automated Management Information Control System.</div>
			</li>
		 </ul>
	  </div>
	  
	  <div class="col">
		<ul>
			<li class="tw-megamenu-title">
			   <a href="<?php echo base_url('products/#diversify'); ?>"><h3>Diversified</h3></a>
			</li>
			<li>
				<a href="<?php echo base_url('products/acuire'); ?>">DMS ACUIRE</a>
				<div class="tw-megamenu-desc">Mini Accounting ERP Solution for SMEs</div>
			</li>
			<li>
				<a href="<?php echo base_url('products/hris/10'); ?>">DMS-HRIS</a>
				<div class="tw-megamenu-desc">Human Resource Information System</div>
			</li>
			<li>
				<a href="<?php echo base_url('products/hris/10'); ?>">DMS-LEAVE/TAS</a>
				<div class="tw-megamenu-desc">Leave and Time & Attendance Solution</div>
			</li>
			<li>
				<a href="<?php echo base_url('products/dms-payadmin'); ?>">DMS - PAYADMIN</a>
				<div class="tw-megamenu-desc">Complete Payroll Processing Solution</div>
			</li>
		</ul>							 
	  </div>		
		</div>
	  </div>
	  
	  <div id="bpo-nav" class="col-md-12 col-lg-10 app-holder hide">
		<div class="row" >
		  <div class="col-md-12 col-lg-4 tw-services-br">
			 <ul>
				<li class="tw-megamenu-title">
				   <h3><a href="<?php echo base_url('what-we-do/payroll-and-bpo-services');?>">Payroll and Business Process Outsourcing Services</a></h3>
				</li>
				<li>
				   <p>Our Payroll BPO Service is highly adaptable managed payroll outsourcing solution, no matter how small or large your organization is. We aim to provide a highly efficient and comprehensive managed payroll solution.</p>
				</li>
			 </ul>
		  </div>
		  <div class="col-md-12 col-lg-4 tw-services-br tw-services-bg">
			 <ul>
				<li class="tw-megamenu-title">
				   <h3><a href="<?php echo base_url('what-we-do/data-capturing-and-digitizing');?>">Data Capturing & Digitization Services</a></h3>
				</li>
				<li>
				   <p>The procedure of using software and scanners to transfer an image on paper to a digital image. Hard copy paper documents are fed through scanning devices that incorporate specialized document scanning to create a digital Portable Document Format (PDF).</p>
				</li>
			 </ul>
		  </div>
		  <div class="col-md-12 col-lg-4 tw-services-br">
			 <ul>
				<li class="tw-megamenu-title">
				   <h3><a href="">Managed Services</a></h3>
				</li>
				<li>
				   <p>With ever increasing business demands companies want to stay focused on their core business activity and outsource support services to save time & energy.</p>
				</li>
			 </ul>
		  </div>
		</div>
	  </div>
	
	  <div id="software" class="col-md-12 col-lg-10 app-holder hide">
		<div class="row" >
		  <div class="col-md-12 col-lg-6">			
			 <ul>
				<li class="tw-megamenu-title">
				   <h3><a class="software-tw-mega-a" href="<?php echo base_url('what-we-do/tailored-application-software-development');?>">Tailored Software Development</a></h3>
				</li>
				<li>
				   <p>Custom software development with the latest tech to automated quality assurance, you get all types of software development services to help you build, sustain, and modernise enterprise software — as well as our extensive 20+ year expertise in building IT solutions.</p>
				   <p><strong>We have the best talent to meet to your needs:</strong></p>
				   <p class="software-talents">Dedicated Software Development Team</p>
				   <p class="software-talents">Software Testing & QA</p>
				   <p class="software-talents">Agile Development Methodology</p>
				   <p class="software-talents">Competency & Skills</p>
				</li>
			 </ul>
		  </div>
		  <div class="col-md-12 col-lg-6 tw-services-br">
			<img src="<?php echo base_url('assets/core/images/background/software-develpment2.png');?>" alt="" class="img-fluid">
		  </div>
	   </div>
	  </div>	
  </div>
 </div>
</header>