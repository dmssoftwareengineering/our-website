<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "DMS Software Engineering";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);	
   ?>
</head>
<body>
   <?php $this->load->view('inc/header'); ?>
   
   <div class="tw-hero-slider owl-carousel">
      <div class="slider-1">
         <div class="slider-wrapper d-table">
            <div class="slider-inner d-table-cell">
               <div class="container">
                  <div class="row wow fadeInUp">
                     <div class="col-md-12">
                        <div class="slider-content">
							<div class="row">
								<div class="col-md-6">
									<h1><span class="sld-1-s1">Launching of our</span><br> <span class="sld-1-s2">Design Thinking</span><br> <span class="sld-1-s3">Studio</span></h1>
									<a href="<?php echo base_url('what-we-do/creative-web-design-and-development'); ?>" class="btn btn-primary">Take a walkthrough <i class="fa fa-angle-right"></i></a>
								</div>
								<div class="col-md-6">
									<img src="<?php echo base_url('assets/images/slides/slide1-img.png');?>" alt="" class="img-fluid slider-img"/>
								</div>
							</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="slider-2">
         <div class="slider-wrapper d-table">
            <div class="slider-inner d-table-cell">
               <div class="container">
                  <div class="row justify-content-center">
                     <div class="col-md-12">
                        <div class="slider-content">
							<div class="row justify-content-center">
								<div class="col-md-3">
									<img src="<?php echo base_url('assets/images/slides/slide2-img.png');?>" alt="" class="img-fluid slider-img">
								</div>
								<div class="col-md-6">
									<h1>Accelerate all your business activities</h1>
								   <p>by using one of the most intelligent business process management solution</p>
								</div>
							</div>
                        </div>
                     </div>
                  </div>
               </div>
			   <a href="<?php echo base_url('what-we-do/creative-web-design-and-development'); ?>" class="btn btn-primary">See Automation in Action</a>
            </div>
         </div>
      </div>

      <div class="slider-3">
         <div class="slider-wrapper d-table">
            <div class="slider-inner d-table-cell">
               <div class="container">
				<div class="row justify-content-center">
				<div class="col">
				<div class="slider-content">
                  <div class="row justify-content-center">
                     <div class="col-md-5">                        
                           <h1><span>Our Fintech</span><br/>Center of Excellence</h1>
                           <p>involves locals and overseas principals to offer<br/><span>Banking and Financial <br/>Related Solutions</span></p>
                           <a href="#">Explore the offers <i class="fa fa-angle-right"></i></a>
                        
                     </div>
					 <div class="col-md-6">
						<img src="<?php echo base_url('assets/images/slides/slide3-img.png');?>" alt="" class="img-fluid slider-img">
					 </div>
                  </div>
				</div>
				</div>
				</div>
               </div>
            </div>
         </div>
      </div>
	  
	  <div class="slider-5">
         <div class="slider-wrapper d-table">
            <div class="slider-inner d-table-cell">
               <div class="container">
                  <div class="row justify-content-center">
                     <div class="col">
                        <div class="slider-content">
							<div class="row">
								<div class="col-md-7">
								   <h1>BPO Services</h1>
								   <p>To accelerate your business growth</p>
								   <p>Proven Excellence in Payroll, Document Digitizing and Managed Services (Help Desk)</p>
								   <a href="#"><div class="sld-5-a">We Innovate</div></a>
								   <a href="#"><div class="sld-5-a">We Automate</div></a>
								   <a href="#"><div class="sld-5-a">We Customize</div></a>
								   <a href="#"><div class="sld-5-a">We Implement</div></a>
								   <a href="#"><div class="sld-5-a">We Support</div></a>
								</div>								
							</div>                           
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <section id="tw-features" class="tw-features-area">
      <div class="container">
         <div class="row tw-mb-65 hide">
            <div class="col-md-5" data-wow-duration="1s" data-aos="fade-left" data-aos-once="false">
               <h2 class="column-title text-md-right text-sm-center">Our Most Valued<br/> Services</h2>
            </div>
            <div class="col-md-6 ml-md-auto wow fadeInRight" data-wow-duration="1s">
               <p class="features-text">We offer you a wide range of services to make your operations and activities more convenient and efficient in a secure. productive and user friendly manner.</p>
            </div>
         </div>
         <div class="row">
			<div class="col-lg-4 col-md-12"  data-aos="fade-up" data-aos-once="false" data-aos-duration="1s" data-aos-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/business-solutions.png');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('products');?>"><h3>Business Solutions</h3></a>
                  <a href="<?php echo base_url('products');?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>
			<div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-once="false" data-wow-duration="1s" data-wow-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/software-development.jpg');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('what-we-do/tailored-application-software-development');?>"><h3>Software Development</h3></a>
                  <a href="<?php echo base_url('what-we-do/tailored-application-software-development');?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>
			<div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-once="false" data-wow-duration="1s" data-wow-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/web-development.png');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('what-we-do/creative-web-design-and-development');?>"><h3>Web Design</h3></a>
                  <a href="<?php echo base_url('what-we-do/creative-web-design-and-development');?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>
			<div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-once="false" data-wow-duration="1s" data-wow-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/mobile-development.png');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('what-we-do/mobile-development');?>"><h3>Mobile Apps</h3></a>
                  <a href="<?php echo base_url('what-we-do/mobile-development');?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>
			<div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-once="false" data-wow-duration="1s" data-wow-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/cloud-development.svg');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('what-we-do/cloud-solutions');?>"><h3>Cloud Solutions</h3></a>
                  <a href="<?php echo base_url('what-we-do/cloud-solutions');?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>
			<div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-once="false" data-wow-duration="1s" data-wow-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/security-solution.png');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('what-we-do/security-solutions');?>"><h3>Security Solutions</h3></a>
                  <a href="<?php echo base_url('what-we-do/security-solutions');?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>
						
			<div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-once="false" data-wow-duration="1s" data-wow-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/payroll-bpo.png');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('what-we-do/payroll-and-bpo-services'); ?>"><h3>Payroll BPO Services</h3></a>
                  <a href="<?php echo base_url('what-we-do/payroll-and-bpo-services'); ?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>
            <div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-once="false" data-wow-duration="1.6s" data-wow-delay=".4s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/data-digitization.png');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('what-we-do/data-capturing-and-digitizing');?>"><h3>Document Digitization</h3></a>
                  <a href="<?php echo base_url('what-we-do/data-capturing-and-digitizing');?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>
            <div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-once="false" data-wow-duration="1.9s" data-wow-delay=".6s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/icon/managed-services.png');?>" alt="">
                     </div>
                  </div>
                  <a href="<?php echo base_url('what-we-do/managed-services');?>"><h3>Managed Services</h3></a>
                  <a href="<?php echo base_url('what-we-do/managed-services');?>" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
            </div>	
         </div>
      </div>
   </section>

   <section id="tw-facts" class="tw-facts">
      <div class="container">
         <div class="row">
            <div class="col-md-4 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn" data-wow-duration="1s">
                     <img src="<?php echo base_url('assets/core/images/icon/fact2.png');?>" alt="" class="img-fluid">
                  </div>
                  <div class="facts-content wow fadeInUp" data-wow-duration="1s">
                     <h4 class="facts-title">Years in Business</h4>
                     <span class="counter">30</span>
                     <sup>+</sup>
                  </div>
               </div>
            </div>
            <div class="col-md-4 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                     <img src="<?php echo base_url('assets/core/images/icon/fact1.png');?>" alt="" class="img-fluid">
                  </div>
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Employees</h4>
                     <span class="counter">100</span>
                     <sup>+</sup>
                  </div>
               </div>
            </div>
            <div class="col-md-4 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                     <img src="<?php echo base_url('assets/core/images/icon/fact3.png');?>" alt="" class="img-fluid">
                  </div>
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Man-Year Experience</h4>
                     <span class="counter">450</span>
                     <sup>+</sup>
                  </div>
               </div>
            </div>
            <div class="col-md-3 text-center" hidden>
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                     <img src="<?php echo base_url('assets/core/images/icon/fact4.png');?>" alt="" class="img-fluid">
                  </div>
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Awards</h4>
                     <span class="counter">50</span>
                     <sup>+</sup>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>   

   <section>
      <div class="container">	  
		<div class="row wow fadeInUp">
            <div class="col text-center">
               <div class="section-heading tw-mb-80">
                  <h2>
                     Our Technologies
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/dot-net.jpg');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/android.jpg');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/xamarin.jpg');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/java.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/bootstrap.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/sqlite.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/react-native.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/nativescript.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/apache.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/source-tree.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/bit-bucket.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/github.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/swift.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/node.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/python.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/mongo-db.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/oracle-db.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/web.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/sql-server.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/angular.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/mysql.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/visual-studio.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/jira.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/sap-bus.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/share-point.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/send-grid.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/oracle.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/office-365.png');?>" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>      
		</div>
   </section>   
      
   <?php
	$this->load->view('inc/footer');	
   ?>
</body>
</html>