<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Trusted Partners | DMS Software Engineering";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);		
   ?>
   <style>.tw-final-result{margin-bottom:50px}.partner{position:absolute;height:47px;width:200px;top:121px;left:128px}.partner-1{height:191px;width:197px;top:51px;left:138px}.partner-2{height:70px;top:111px}.partner-6{left:68px;width:300px}@media (min-width:320px) and (max-width:548px){.result-bg{display:none}.tw-final-result{margin-bottom:0}}</style>
</head>

<body>
	<?php $this->load->view('inc/header'); ?>

   <div id="banner-area" class="banner-area bg-overlay case-bg-overlay" style="background-image:url(<?php echo base_url('assets/core/images/banner/baner.jpg'); ?>">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading case-banner-heading">
                  <h1 class="banner-case-title">Our Trusted<br/> <span>Partners</span></h1>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!--<section id="main-container" class="main-container" hidden>
      <div class="container">
         <div class="row">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>The Beginning</small>
                     Challenge & <span>Solution</span>
                  </h2>
                  <span class="animate-border ml-auto mr-auto tw-mt-20 tw-mb-40"></span>
                  <p>
                     Start working with an company that can provide everything you need to generate awareness, drive traffic, connect with . massa
                     quis enim. Donec pede justo. Dringilla vel, aliquet nec, vulputate eget, arcu Donec quam felis, ultricies
                     nec, pellentesque eu, pretium. quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla
                     vel, aliquet nec, vulputate eget, arcu
                  </p>
               </div>
            </div>
         </div>
      </div>
   </section>-->

   <section class="tw-final-result">
      <div class="container">
         <div class="row">
            <div class="col-md-6" data-aos="fade-left" data-aos-once="false">
               <div class="section-heading">
                  <h2>
                     3I Infotech
                     <span class="animate-border tw-mt-20 tw-mb-35"></span>
                  </h2>
               </div>
               <div class="tw-results-content">
                  <p>3I Infotech has over 5000+ employees in 24+ offices across 12 countries and over 1200+ customers in more than 50 countries across 4 continents. With a comprehensive set of IP based software solutions and a wide range of IT services, 3i Infotech has successfully streamlined business operations of customers globally.</p>

                  <div class="results-trafiic tw-mt-40">
                     <div class="traffic-info">
                        <img src="<?php echo base_url('assets/core/images/icon/final_icon2.png');?>" alt="" class="img-fluid">
                        <span>More at <a href="https://www.3i-infotech.com/about-us/" target="_blank">3i Infotech</a></span>
                     </div>
                  </div>
               </div>
            </div>
			
            <div class="col-md-6" data-aos="fade-right" data-aos-once="false">
               <div class="result-bg">
                  <img src="<?php echo base_url('assets/core/images/cases/final_result.png');?>" alt="">
				  <div class="partner partner-1" style="background:url('<?php echo base_url('assets/images/partners/3i.jpg');?>')"></div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="tw-final-result">
      <div class="container">
         <div class="row">			
            <div class="col-md-6" data-aos="fade-right" data-aos-once="false">
               <div class="result-bg">
                  <img src="<?php echo base_url('assets/core/images/cases/final_result.png');?>" alt="">
				  <div class="partner partner-2" style="background:url('<?php echo base_url('assets/images/partners/aura.png');?>')"></div>
               </div>
            </div>
            <div class="col-md-6" data-aos="fade-right" data-aos-once="false">
               <div class="section-heading">
                  <h2>
                     Aura Portal
                     <span class="animate-border tw-mt-20 tw-mb-35"></span>
                  </h2>
               </div>
               <div class="tw-results-content">
                  <p>AuraPortal is an internationally renowned BPM software vendor, recognized by leading analyst firms, including Gartner and OVUM, for its extreme ease of use, quickest implementation rates, seamless integration with other systems, scalability and many other features.</p>

                  <div class="results-trafiic tw-mt-40">
                     <div class="traffic-info">
                        <img src="<?php echo base_url('assets/core/images/icon/final_icon2.png');?>" alt="" class="img-fluid">
                        <span>More at <a href="https://www.auraportal.com/our-team/company/" target="_blank">Aura Portal</a></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="tw-final-result">
      <div class="container">
         <div class="row">
            <div class="col-md-6" data-aos="fade-right" data-aos-once="false">
               <div class="section-heading">
                  <h2>
                     Microsoft
                     <span class="animate-border tw-mt-20 tw-mb-35"></span>
                  </h2>
               </div>
               <div class="tw-results-content">
                  <p>Microsoft (Nasdaq “MSFT” @microsoft) enables digital transformation for the era of an intelligent cloud and an intelligent edge. Its mission is to empower every person and every organization on the planet to achieve more.</p>

                  <div class="results-trafiic tw-mt-40">
                     <div class="traffic-info">
                        <img src="<?php echo base_url('assets/core/images/icon/final_icon2.png');?>" alt="" class="img-fluid">
                        <span>More at <a href="https://www.microsoft.com/en-us/about" target="_blank">Microsoft</a></span>
                     </div>
                  </div>
               </div>
            </div>
			
            <div class="col-md-6" data-aos="fade-right" data-aos-once="false">
               <div class="result-bg">
                  <img src="<?php echo base_url('assets/core/images/cases/final_result.png');?>" alt="">
				  <div class="partner" style="background:url('<?php echo base_url('assets/images/partners/microsoft.jpg');?>')"></div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="tw-final-result">
      <div class="container">
         <div class="row">
			
            <div class="col-md-6" data-aos="fade-right" data-aos-once="false">
               <div class="result-bg">
                  <img src="<?php echo base_url('assets/core/images/cases/final_result.png');?>" alt="">
				  <div class="partner" style="background:url('<?php echo base_url('assets/images/partners/kaspersky.jpg');?>')"></div>
               </div>
            </div>
            <div class="col-md-6" data-aos="fade-left" data-aos-once="false">
               <div class="section-heading">
                  <h2>
                     Kaspersky
                     <span class="animate-border tw-mt-20 tw-mb-35"></span>
                  </h2>
               </div>
               <div class="tw-results-content">
                  <p>Kaspersky Lab is a global cybersecurity company founded in 1997. Kaspersky Lab’s deep threat intelligence and security expertise is constantly transforming into security solutions and services to protect businesses, critical infrastructure, governments and consumers around the globe. The company’s comprehensive security portfolio includes leading endpoint protection and a number of specialized security solutions and services to fight sophisticated and evolving digital threats.</p>

                  <div class="results-trafiic tw-mt-40">
                     <div class="traffic-info">
                        <img src="<?php echo base_url('assets/core/images/icon/final_icon2.png');?>" alt="" class="img-fluid">
                        <span>More at <a href="https://www.kaspersky.co.in/about" target="_blank">Kaspersky</a></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="tw-final-result">
      <div class="container">
         <div class="row">
            <div class="col-md-6" data-aos="fade-left" data-aos-once="false">
               <div class="section-heading">
                  <h2>
                     Sophos
                     <span class="animate-border tw-mt-20 tw-mb-35"></span>
                  </h2>
               </div>
               <div class="tw-results-content">
                  <div class="results-trafiic tw-mt-40">
                     <div class="traffic-info">
                        <img src="<?php echo base_url('assets/core/images/icon/final_icon2.png');?>" alt="" class="img-fluid">
                        <span>More at <a href="#" target="_blank">Sophos</a></span>
                     </div>
                  </div>
               </div>
            </div>			
			
            <div class="col-md-6" data-aos="fade-right" data-aos-once="false">
               <div class="result-bg">
                  <img src="<?php echo base_url('assets/core/images/cases/final_result.png');?>" alt="">
				  <div class="partner" style="background:url('<?php echo base_url('assets/images/partners/sophos.jpg');?>')"></div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--<section class="tw-final-result" hidden>
      <div class="container">
         <div class="row wow fadeInLeft">
			
            <div class="col-md-6">
               <div class="result-bg">
                  <img src="<?php echo base_url('assets/core/images/cases/final_result.png');?>" alt="">
				  <div class="partner partner-6" style="background:url('<?php echo base_url('assets/images/partners/connex.png');?>')"></div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="section-heading">
                  <h2>
                     Connex
                     <span class="animate-border tw-mt-20 tw-mb-35"></span>
                  </h2>
               </div>
               <div class="tw-results-content">
                  <p>Connex is an innovative value-added IT distributor focused on technology areas of every part of the business IT infrastructure through system integrators, resellers and consultants.</p>

                  <div class="results-trafiic tw-mt-40">
                     <div class="traffic-info">
                        <img src="<?php echo base_url('assets/core/images/icon/final_icon2.png');?>" alt="" class="img-fluid">
                        <span>More at <a href="http://www.connexit.biz/about-us/" target="_blank">Connex</a></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>-->

   <?php
	$this->load->view('inc/footer');		
   ?>
</body>
</html>