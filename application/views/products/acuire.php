<!doctype html>
<html lang="en">
	<head>
		<?php 
			$data["title"] = "DMS – ACUIRE | DMS Software Engineering";
			$data["description"] = "";
			$data["keywords"] = "";
			$this->load->view('inc/head', $data);		
	    ?>
		<style>
		.pc-feature>div:nth-child(2) div,.pcf-title{margin-left:15px}.pc-feature,.pcf-desc{clear:left}.pc-slider{height:350px;position:relative;width:950px}.pc-features,.pc-monitor,.pc-pointer,.pc-pointer div{position:absolute}.pc-monitor{background:url("<?php echo base_url('assets/core/images/products/monitor.png');?>") no-repeat;background-size:100%;height:334px;padding:6px 0 0 6px;width:446px}.pcm-img{height:238px;width:435px}.pc-pointer{height:20px;left:441px;top:13px;width:51px;z-index:1}.pc-pointer div:nth-child(1){background:#2f2c2c;height:2px;top:9px;width:64px;z-index:1}.pc-pointer div:nth-child(2){background:#2f2c2c;height:20px;left:52px;width:20px}.pc-features{left:490px;margin-top:10px;width:460px}.pc-feature>div:nth-child(1) div{float:left}.pcf-box{background:#fff;border:2px solid #0070c0;cursor:pointer;height:26px;width:26px}.pcf-title{color:#2f2c2c;cursor:pointer;font-weight:700;font-size:18px;padding-bottom:10px}.pcf-title:hover{color:#0070c0}.pcf-desc{display:none;line-height:18px;margin:0 0 20px 40px}.pcf-desc-active{display:block}.top-header .pc-monitor{position:relative;top:50px;z-index:0}.prod-cat{margin:0 0 10px;font-weight:700;width:170px;color:#000;z-index:1}.prod-status{margin:70px 0 20px 84px}.prod-status .download,.col-sm-12 .download{padding:8px 20px;border-radius:5px;margin-top:2px;color:#fff}.download i{padding-right:10px;color:#fff}.request-demo{border-color:red;background:red}.download-brochure{background:#f7b507;border-color:#f7b507}.download:hover{background:#2f2c2c}.prod-mains{font-weight:700}.top-header *{color:#2f2c2c}@media (min-width: 992px){.col-lg-5{max-width:100%}}@media (max-width: 992px){.pc-features{left:0!important;top:350px}.pc-pointer{display:none}.tw-client{padding-top:200px}}.top-header{padding:70px 0 20px}.advantage-img{margin-top:-30px;margin-bottom:20px;height:60px}#benefits{padding:60px 0;background:#f7b507;color:#2f2c2c}.post-media.post-video::before{height:74%}.video-icon{top:40%}.tw-final-result ul{margin-left:-40px;counter-reset:li}.tw-final-result li{list-style-type:none;font-size:13px}.tw-final-result li::before{content:"→";font-weight:700;color:#0070c0;padding-right:10px;font-size:30px;font-weight:bold}.feature-image{margin-top:141px}.tw-web-analytics-content{margin-bottom:0}#features{padding-bottom:20px}.section-heading h2 span{color:#f7b507}.ac-img{position:absolute}.ac-img:nth-child(1){top:200px;left:-110px}.ac-img:nth-child(2){right:-70px;top:328px}.tw-client{padding-top:100px}
		</style>
	</head>
	<body>	
		<?php $this->load->view('inc/header');?>		
		<section class="tw-final-result top-header">
			<img src="<?php echo base_url('assets/images/products/acuire-bg-lft.png'); ?>" class="ac-img" alt=""/>
			<img src="<?php echo base_url('assets/images/products/acuire-bg-rgt.png'); ?>" class="ac-img" alt=""/>
			<div class="container">
				<div class="row" data-aos="fade-left" data-aos-once="false">	
					<div class="col-md-1"></div>
					<div class="col-md-12 col-lg-5 col-sm-12">
						<div class="row">
							<div class="pc-monitor post-media post-video" data-aos="fade-left" data-aos-once="false">
								<img class="pcm-img" src="<?php echo base_url('assets/core/images/news/post2.jpg'); ?>" alt="Screen" />
								<a class="video-popup" href="#">
									<div class="video-icon">
									   <i class="icon icon-play"></i>
									</div>
								</a>
							</div>
						</div>
						<div class="row prod-status">
							<a href="#">
								<div class="btn btn-primary download request-demo"><i class="fa fa-hand-o-right"></i>Request a Demo Today</div>
							</a>	
						</div>
					</div>
					<div class="col-md-12 col-lg-5 col-sm-12">
					   <div class="section-heading">
						  <h2><span>DMS – ACUIRE</span></h2>
					   </div>		
					   <a href="#" class="prod-cat">Manufacturing</a>
					   <p>Inventory Management Simplified With DMS ACUIRE</p>
					   <p>Inventory management is difficult enough to raise your pulses, but it is important too. For organizations, it is necessart to make sure that they have sufficient stock to fulfill the orders. Also, they have to save themselves from becoming over stocked to avoid loss. Taking care of all such things is not possible just by writing down each detail in excel files.</p>
					   <p>DMS ACUIRE is a unique online inventory software solution that accomplishes more for you in less time, increases your inventory control and simplifies your inventory management from anywhere at anytime.</p>
					   <a href="#"><div class="btn btn-primary download download-brochure"><i class="fa fa-download"></i>Brochure</div></a>
					</div>
				</div>
			</div>
		</section>    
		
		<section id="benefits" class="tw-final-result">
		  <div class="container align-self-md-center">
			 <div class="row text-center" data-aos="fade-right" data-aos-once="false">	
				<div class="col-md-3 align-self-md-center">
					<img src="<?php echo base_url('assets/images/products/simple-use.png');?>" alt="" class="img-fluid advantage-img"><br/>
					<strong>Sophisticated, Yet Simple to Use</strong><br/>
					You needn't be a computer expert to use DMS-ACUIRE. Its menu driven functions are easy to use, and the system controls all critical functions.
				</div>
				<div class="col-md-3 align-self-md-center">
					<img src="<?php echo base_url('assets/images/products/transaction-post.png');?>" alt="" class="img-fluid advantage-img" style="margin-top:0"><br/>
					<strong>Comprehensive Control Features</strong><br/>
					The system implements all required audit functions; input sequence completion verification, input batch total authentication and digit checking.
				</div>
				<div class="col-md-3 align-self-md-center">
					<img src="<?php echo base_url('assets/images/products/multiple-currency.png');?>" alt="" class="img-fluid advantage-img"><br/>
					<strong>Multiple Currencies</strong><br/>
					Transaction can be input in multiple currencies, with conversion to the base currency of the country of operation.
				</div>
				<div class="col-md-3 align-self-md-center">
					<img src="<?php echo base_url('assets/images/products/modules.png');?>" alt="" class="img-fluid advantage-img"><br/>
					<strong>Modular System Architecture</strong><br/>
					The system architecture is designed to easily incorporate new product innovations and upgrades, protecting your initial investment.
				</div>
			 </div>
		  </div>
		</section>
		
		<section id="features" class="tw-final-result">
		  <div class="container">
			<div class="row" data-aos="fade-down" data-aos-once="false">			
				<div class="col-md-12 ml-auto align-self-center">
				   <div class="tw-web-analytics-content">
					  <i class="icon icon-target"></i>
					  <h2>Some Benefits you receieve with <span>ACUIRE</span></h2>
					  <small>Get to know all the great benefits. Why wait? <a href="#"><strong>Request a Demo Today</strong></a></small>
					  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
						<div class="row">
						  <div class="col">
							  <p><strong>General Ledger</strong></p>
							  <ul>
								<li>Data capture either automatically through sub system or independently through direct data entry functions.</li>
								<li>Standing order for standard entries.</li>
								<li>Multiple open process periods.</li>
								<li>Flexible account-no formatting.</li>
								<li>Auto reversal of provisional entries after end of reporting period.</li>
							  </ul>
						  </div>
						  <div class="col">
							  <p><strong>Accounts Receivable</strong></p>
							  <ul>
								<li>Facility to specify valid periods at system level with customer/transaction level override</li>
								<li>Multicurrency accounts for customers.</li>
								<li>Flexible customer code formatting.</li>
								<li>Facility to maintain unapplied credits.</li>
								<li>Identification of customer with salesman.</li>
							  </ul>
						  </div>
						  <div class="col">
							  <p><strong>Accounts Payable</strong></p>
							  <ul>
								<li>Selection of Invoices due for payment with facility to review.</li>
								<li>Selection of bank Accounts on which cheques are to be drawn.</li>
								<li>Printing of cheques via Payments Module..</li>
							  </ul>
						  </div>
						  <div class="col">
							  <p><strong>Inventory Control</strong></p>
							  <ul>
								<li>Multiple pricing policies for different products.</li>
								<li>Different units of measure for reporting and maintenance of stock.</li>
								<li>Facility to maintain same products in multiple locations.</li>
								<li>Product profile allow for 3-way classification of categories.</li>
							  </ul>
						  </div>
						</div>	
						<div class="row">
						  <div class="col">
							  <p><strong>Purchasing Sub System</strong></p>
							  <ul>
								<li>Generation of purchase orders when re-order level is reached.</li>
								<li>Allocation of materials.</li>
								<li>Payable system.</li>
								<li>Matching of Goods Received notes.</li>
								<li>Automatic updates of inventory and Accounts.</li>
							  </ul>
						  </div>
						  <div class="col">
							  <p><strong>Fixed Asset System</strong></p>
							  <ul>
								<li>Definition of depreciation for financial accounting and taxation.</li>
								<li>Asset Schedule and Asset Movement report.</li>
								<li>Variable percentages for financial accounting and taxation depreciation.</li>
								<li>Facility to handle disposal of fixed assets.</li>
							  </ul>
						  </div>
						  <div class="col">
							  <p><strong>Payroll</strong></p>
							  <ul>
								<li>List of bank remittances.</li>
								<li>List of other remittances.</li>
								<li>Cost centre-wise analysis.</li>
								<li>Tax returns.</li>
								<li>Coin analysis by Pay Point.</li>
								<li>Definition of earnings and deductions as per user requirements.</li>
							  </ul>
						  </div>
						  <div class="col">
							  <p><strong>Invoicing and Sales Analysis</strong></p>
							  <ul>
								<li>Online updates, online invoice of stock position.</li>
								<li>Analysis by customer.</li>
								<li>Flexible discount and free issue scheme.</li>
								<li>Target achievement percentages.</li>
								<li>Analysis by salesman or area.</li>
							  </ul>
						  </div>
						</div>	
				   </div>
				</div>
			 </div>
		  </div>
	   </section> 
	   
		<section class="tw-client">
		  <div class="container">
			<div class="row" data-aos="fade-up" data-aos-once="false">
				<div class="col text-center">
				   <div class="section-heading tw-mb-80">
					  <h2>They Use <span>ACUIRE</span></h2>
					  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
				   </div>
				</div>
			 </div>
			 <div class="row" data-aos="fade-up" data-aos-once="false">
				<div class="col-md-12">
				   <div class="clients-carousel owl-carousel">
					  <div class="client-logo-wrapper d-table">
						 <div class="client-logo d-table-cell">
							<img src="<?php echo base_url('assets/core/images/clients/client1.png');?>" alt="">
						 </div>
					  </div>
					  <div class="client-logo-wrapper d-table">
						 <div class="client-logo d-table-cell">
							<img src="<?php echo base_url('assets/core/images/clients/client2.png');?>" alt="">
						 </div>
					  </div>
					  <div class="client-logo-wrapper d-table">
						 <div class="client-logo d-table-cell">
							<img src="<?php echo base_url('assets/core/images/clients/client3.png');?>" alt="">
						 </div>
					  </div>
					  <div class="client-logo-wrapper d-table">
						 <div class="client-logo d-table-cell">
							<img src="<?php echo base_url('assets/core/images/clients/client4.png');?>" alt="">
						 </div>
					  </div>
				   </div>
				</div>
			 </div>
		  </div>
		</section>   
		<?php 
			$this->load->view('inc/footer', $data);
		?>
	</body>
</html>