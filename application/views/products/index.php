<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Products | DMS Software Engineering";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);		
	?>
	<style>
		.no-padd-bot{padding-bottom:0}
		.section-heading{
			margin-bottom:40px
		}
		.tw-service-box-bg {
			padding: 5px 8px;
			float: left;
			height: 170px;
			margin-right: 10px;
			margin-bottom: 10px;
			width: 180px;
		}
		
		.tw-service-box-list p{
			color:#fff;
			line-height:14px
		}
		.owl-item active{
			width:0
		}
		.tw-service-box-list h3{
			margin-top:15px
		}
		.tw-service-box-list h3 a{
			color:#215968
		}		
		.tw-service-box-list:hover{
			background:black
		}
		
		.bus-sol .tw-service-box-bg{
			background:#00b0f0	
		}		
		.bus-sol-part .tw-service-box-bg{
			background:#e46c0a			
		}
		.bus-sol-part .tw-service-box-list h3 a{
			color:black
		}
		
		.tea .tw-service-box-bg{
			background:green
		}
		.tea .tw-service-box-list h3 a{
			color:#fff
		}

		.div-sol .tw-service-box-bg{
			background:#00b0f0
		}
		
		.tw-service-box-bg:hover{
			box-shadow:0px -5px 7px 0px gold
		}
		
		.bus-sol-div .tw-service-box-bg{
			background:#254061
		}
		.bus-sol-div .tw-service-box-list h3 a{
			color:#fff
		}
		
		.partners{
			height:100px
		}
		
		.banner-title span{
			color:#0070c0
		}
	</style>
</head>
<body>
   <?php $this->load->view('inc/header');?>

   <div id="banner-area" class="banner-area" style="background-image:url(<?php echo base_url('assets/images/products/products-bg.jpg');?>)">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading">
				  <span><strong>We Engineer Products & Services</strong></span>
                  <h1 class="banner-title">
					<span>Business Solutions</span>
				  </h1>
               </div>
            </div>
         </div>
      </div>
   </div>

   <section id="financial" class="main-container no-padd-bot">
      <div class="container">
         <div class="row">
            <div class="col">
               <div class="section-heading">
                  <h2>
					<small>Products for the</small>
                     Banking & Financial
					 <small>Sector</small>
                  </h2>
                  <span class="animate-border tw-mt-30 tw-mb-40"></span>
               </div>
            </div>
		  </div>
		  
		  <div class="row bus-sol">
            <div class="col">
               <div class="">
                  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">BANKSYS</a></h3>
                     <p>A conventional banking solution for co-operative rural banks</p>
                  </div>                  
                  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">IMAGO</a></h3>
                     <p>Cheque Imaging and Truncation Solution</p>
                  </div>				  
				  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">CRIMS</a></h3>
                     <p>CRIB Information Management System. Best solution to minimize the CRIB reporting errors</p>
                  </div>				  
				  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">CRMS</a></h3>
                     <p>Customs Remittance Management System</p>
                  </div>
				  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">DIS</a></h3>
                     <p>Deposits Insurance Scheme</p>
                  </div>
				  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">CRRS</a></h3>
                     <p>CBO and WBO Regulatory reports submitted to CBSL</p>
                  </div>
				  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">BROKERAGE APP</a></h3>
                     <p>Pay Brokerage fee to the brokers who bring business to FX & FI Trading desk and ALM desk.</p>
                  </div>
				  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">NOP</a></h3>
                     <p>Inform the previous days FX position to CBSL</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <section id="manufacture" class="main-container no-padd-bot">
      <div class="container">
         <div class="row">
            <div class="col">
               <div class="section-heading">
                  <h2>
					<small>Products for the</small>
                     Manufacturing
					 <small>Sector</small>
                  </h2>
                  <span class="animate-border tw-mt-30 tw-mb-40"></span>
               </div>
            </div>
		  </div>
		  <div class="row tea">
            <div class="col">
               <div class="">
                  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">TEAMICS</a></h3>
                     <p>Tea Exporters Automated Management Information & Control System</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <section id="diversify" class="main-container">
      <div class="container">
         <div class="row">
            <div class="col">
               <div class="section-heading">
                  <h2>
					<small>Products for the</small>
                     Diversified
					 <small>Sectors</small>
                  </h2>
                  <span class="animate-border tw-mt-30 tw-mb-40"></span>
               </div>
            </div>
		  </div>
		  <div class="row div-sol">
            <div class="col">
               <div class="">
                  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">TAS/LEAVE</a></h3>
                     <p>Leave and Time & Attendance Solution</p>
                  </div>				  
				  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">PAYADMIN</a></h3>
                     <p>Complete Payroll Processing Solution</p>
                  </div>				  
				  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">E-PAYSLIP</a></h3>
                     <p>www.epayslip.lk Electronic Payslip on Cloud</p>
                  </div>
				  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">EMS</a></h3>
                     <p>Cloud based education portal for small and medium size universities & campus</p>
                  </div>                  
                  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">ACUIRE</a></h3>
                     <p>Mini Accounting ERP Solution for SMEs</p>
                  </div>				  
				  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">HRIS</a></h3>
                     <p>Human Resource Information System</p>
                  </div>				  
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <section class="main-container" style="padding-top:0">
      <div class="container">
         <div class="row">
            <div class="col">
               <div class="section-heading">
                  <h2>
					<small>Products Offered By</small>
                     Strategic Partners
                  </h2>
                  <span class="animate-border tw-mt-30 tw-mb-40"></span>
               </div>
            </div>
		  </div>
		  
		 <div class="row" style="margin-top:40px">			
			  <div class="col">
				<div class="section-heading">
				  <h2 style="font-size:25px">
					For Banking & Financial Institutions
				  </h2>
				</div>
			  </div>
		  </div>		  
		  <div class="row bus-sol-part">
            <div class="col">
               <div class="">
                  <div class="tw-service-box-list tw-service-box-bg partners">                     
                     <h3><a href="javascript:void()">KASTLE®</a></h3>
                     <p>Core Banking Solution</p>
                  </div>                  
                  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg partners">                     
                     <h3><a href="javascript:void()">KASTLE®</a></h3>
                     <p>Treasury Solution</p>
                  </div>				  
				  <div class="tw-service-box-list tw-service-box-bg partners">                     
                     <h3><a href="javascript:void()">KASTLE®</a></h3>
                     <p>Asset Liability Management</p>
                  </div>				  
				  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg partners">                     
                     <h3><a href="javascript:void()">AMLOCK</a></h3>
                     <p>Anti-Money Laundering</p>
                  </div>
               </div>
            </div>
         </div>
		 
		 <div class="row" style="margin-top:40px">			
			  <div class="col">
				<div class="section-heading">
				  <h2 style="font-size:25px">
					For Insurance
				  </h2>
				</div>
			  </div>
		  </div>		  
		  <div class="row bus-sol-part">
            <div class="col">
               <div class="">
                  <div class="tw-service-box-list tw-service-box-bg partners">                     
                     <h3><a href="javascript:void()">PREMIA</a></h3>
                     <p>Life Insurance</p>
                  </div>                  
                  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg partners">                     
                     <h3><a href="javascript:void()">PREMIA</a></h3>
                     <p>General Insurance</p>
                  </div>				  
				  <div class="tw-service-box-list tw-service-box-bg partners">                     
                     <h3><a href="javascript:void()">PREMIA</a></h3>
                     <p>Health Insurance</p>
                  </div>	
               </div>
            </div>
         </div>
		 
		 <div class="row" style="margin-top:40px">			
			  <div class="col">
				<div class="section-heading">
				  <h2 style="font-size:25px">
					For Diversified Market
				  </h2>
				</div>
			  </div>
		  </div>		  
		  <div class="row bus-sol-div">
            <div class="col">
               <div class="">
                  <div class="tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">DATASCAN ONLINE</a></h3>
                     <p>Enterprise Document Content Management System for the WEB</p>
                  </div>                  
                  <div class="tw-service-box-list tw-service-box-list tw-service-box-bg">                     
                     <h3><a href="javascript:void()">AURA PORTAL</a></h3>
                     <p>Intelligent Business Process Management System</p>
                  </div>	
               </div>
            </div>
         </div>
      </div>
   </section>

   <?php 
		$data['show_quote'] = '';
		$this->load->view('inc/footer', $data);
   ?>
</body>
</html>