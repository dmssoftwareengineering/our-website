<!doctype html>
<html lang="en">
	<head>	
		<?php 
			$data["title"] = "DMS – PAYADMIN | DMS Software Engineering";
			$data["description"] = "";
			$data["keywords"] = "";
			$this->load->view('inc/head', $data);		
	    ?>
		<style>.pc-feature>div:nth-child(2) div,.pcf-title{margin-left:15px}.pc-feature,.pcf-desc{clear:left}.pc-slider{height:350px;position:relative;width:950px}.pc-features,.pc-monitor,.pc-pointer,.pc-pointer div{position:absolute}.pc-monitor{background:url(<?php echo base_url(assets/core/images/products/monitor.png);?>) no-repeat;background-size:100%;height:334px;padding:6px 0 0 6px;width:446px}.pcm-img{height:238px;width:435px}.pc-pointer{height:20px;left:441px;top:13px;width:51px;z-index:1}.pc-pointer div:nth-child(1){background:#2f2c2c;height:2px;top:9px;width:64px;z-index:1}.pc-pointer div:nth-child(2){background:#2f2c2c;height:20px;left:52px;width:20px}.pc-features{left:490px;margin-top:10px;width:460px}.pc-feature>div:nth-child(1) div{float:left}.pcf-box{background:#fff;border:2px solid #0070c0;cursor:pointer;height:26px;width:26px}.pcf-title{color:#2f2c2c;cursor:pointer;font-weight:700;font-size:18px;padding-bottom:10px}.pcf-title:hover{color:#0070c0}.pcf-desc{display:none;line-height:18px;margin:0 0 20px 40px}.pcf-desc-active{display:block}.top-header .pc-monitor{position:relative;top:50px;z-index:0}.prod-cat{margin:0 0 10px;font-weight:700;width:170px;color:#000;z-index:1}.prod-status{margin:70px 0 20px 84px}.prod-status .download,.col-sm-12 .download{padding:8px 20px;border-radius:5px;margin-top:2px;color:#fff}.download i{padding-right:10px;color:#fff}.request-demo{border-color:red;background:red}.download-brochure{background:#0070c0;border-color:#0070c0}.download:hover{background:#2f2c2c}.prod-mains{font-weight:700}.top-header *{color:#2f2c2c}@media (min-width: 992px){.col-lg-5{max-width:100%}}@media (max-width: 992px){.pc-features{left:0!important;top:350px}.pc-pointer{display:none}.tw-client{padding-top:200px}}.top-header{padding:70px 0 20px}.advantage-img{margin-bottom:20px;height:60px}#benefits{padding:60px 0;background:#f6f7f6;color:#2f2c2c}.post-media.post-video::before{height:74%}.video-icon{top:40%}.tw-final-result ul{margin-left:-40px;counter-reset:li}.tw-final-result li{list-style-type:none;margin-bottom:10px}.tw-final-result li::before{content:"→";font-weight:700;color:#0070c0;padding-right:10px;font-size:30px;font-weight:bold}.feature-image{margin-top:141px}.tw-web-analytics-content{margin-bottom:0}#features{padding-bottom:20px}</style>
	</head>
	<body>	
		<?php $this->load->view('inc/header');?>		
		<section class="tw-final-result top-header">
			<div class="container">
				<div class="row" data-aos="fade-left" data-aos-once="false">	
					<div class="col-md-1"></div>
					<div class="col-md-12 col-lg-5 col-sm-12">
						<div class="row">
							<div class="pc-monitor post-media post-video" data-aos="fade-left" data-aos-once="false">
								<img class="pcm-img" src="<?php echo base_url('assets/core/images/news/post2.jpg'); ?>" alt="Screen" />
								<a class="video-popup" href="#">
									<div class="video-icon">
									   <i class="icon icon-play"></i>
									</div>
								 </a>
							</div>
						</div>
						<div class="row prod-status">
							<a href="#">
								<div class="btn btn-primary download request-demo"><i class="fa fa-hand-o-right"></i>Request a Demo Today</div>
							</a>	
						</div>
					</div>
					<div class="col-md-12 col-lg-5 col-sm-12">
					   <div class="section-heading">
						  <h2><span>DMS – PAYADMIN</span></h2>
					   </div>		
					   <a href="#" class="prod-cat">Financial</a>
					   <p>
					   This is our flagship product widely used by many customers either “in-house” or on “BPO” basis. Outsourcing your payroll function can save time and money. More and more organizations are placing their trust in <strong>DMS Software Engineering (Pvt) Ltd</strong> to deliver payroll as a bureau, outsourced, part managed or fully managed service. 
					   </p>
					   <p>
					   We provide payroll services to more than <strong>50+ clients</strong> – from smaller organizations to those with <strong>1000+ employees</strong>. Our client base extends across all market sectors in Sri Lanka.
					   </p>
					   <p>
					   <strong>Benefit from the accessibility, flexibility and control</strong><br/>of running payroll in-house – without having to allocate your staff, equipment, time and resources to manage the payroll services.
					   </p>
					   <a href="#"><div class="btn btn-primary download download-brochure"><i class="fa fa-download"></i>Brochure</div></a>
					</div>
				</div>
			</div>
		</section>    
		
		<section id="benefits" class="tw-final-result">
		  <div class="container">
			 <div class="row text-center" data-aos="fade-right" data-aos-once="false">			
				<div class="col-md-2 align-self-md-center">
					<img src="<?php echo base_url('assets/core/images/icon/feature1.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Secured access to payroll, HR and accounting data.
				</div>
				<div class="col-md-3">
					<img src="<?php echo base_url('assets/core/images/icon/process3.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Service options to meet the level demanded by your business.
				</div>
				<div class="col-md-3">
					<img src="<?php echo base_url('assets/core/images/icon/fact1.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Minimizes administration and free-up people and equipment.
				</div>
				<div class="col-md-2">
					<img src="<?php echo base_url('assets/core/images/icon/service6.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Unlimited helpdesk support.
				</div>
				<div class="col-md-2">
					<img src="<?php echo base_url('assets/core/images/icon/process2.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Automatic updates in accordance with changes in legislation.
				</div>
			 </div>
		  </div>
		</section>
		
		<section id="features" class="tw-final-result">
		  <div class="container">
				<div class="row" data-aos="fade-dowm" data-aos-once="false">
				
					<div class="col-md-7 ml-auto align-self-center">
					   <div class="tw-web-analytics-content">
						  <i class="icon icon-target"></i>
						  <h2>Some Benefits you receieve with <span>PAYADMIN</span></h2>
						  <small>Get to know all the great benefits. Why wait? <a href="#"><strong>Request a Demo Today</strong></a></small>
						  
						  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
						  <ul>
							<div class="row">
							  <div class="col">
								  <li>Employees can be categorized to define  common benefits. </li>
								  <li>Analyze the cost of the employees for  different cost center.</li>
								  <li>Provides for Hundred different types of  earnings and Nine Hundred different types of Deductions. Maintains Standard  Earnings and Deductions until they are changed.</li>
								  <li>Earnings can be defined as to whether  they are applicable for calculation of O/T, PAYEE, EPF, etc.</li>
								  <li>All the third party deduction reports  would be generated by the system such as bank remittance, bank order, and other  order remittance reports with summary.</li>
								  <li>All the required statutory reports such  as EPF, ETF, Monthly and half-yearly, tax return schedule and certificates,  etc. are generated by the system.</li>
								  <li>Salary payment method could be either  salary or wages.</li>
								  <li>The module has the facility  to run stand alone or integrated with Bar Code System and Cheque Writer.</li>
							  </div>
							  <div class="col">
								  <li>Deductions that could not be deducted  previous month will be deducted automatically from the current months Payroll.</li>
								  <li>Automatic removal of deductions on user  determined 'Priority Basis' in instances where deductions of an employee exceed  the allowed limit.</li>
								  <li>If Stamp deductions are to be recovered  from the net pay, the recoveries will be transferred automatically by the  system.</li>
								  <li>Provides 4 different methods of  adjusting coins in the net salary. E.g.:&nbsp;  Net Salary is rounded and the difference in coins will be carried  forward to easy Pay Packeting.</li>
								  <li>Current month’s payroll will be  reconciled automatically with the previous month’s payroll - item-wise.</li>
								  <li>User defined overtime and lump sum  payment schemes.</li>
								  <li>The facility to process payments made  outside the normal payroll run.</li>								  
							  </div>
							  </div>
							</ul>		
					   </div>
					</div>
					
					<div class="col-md-5">
					   <img src="<?php echo base_url('assets/core/images/services/single_service_img3.png');?>" alt="" class="img-fluid feature-image">
					</div>
				 </div>
		  </div>
	   </section>     

		<section class="tw-final-result">
		  <div class="container">
			 <div class="row data-aos="fade-left" data-aos-once="false">				
				<div class="col-md-12 text-center">
				   <div class="section-heading">
					  <h2>
						 Technology and the Environment  
						 <span class="animate-border tw-mt-20 tw-mb-30 ml-auto mr-auto"></span>
					  </h2>
				   </div>
				</div>
			  </div>
			  <div class="row" data-aos="fade-left" data-aos-once="false">		
				<div class="col-md-4">		
				   <p>We adapt to ‘open’ technology compliant to ISO/ANSI/Industry Standards, Graphical User Interface using Industry Standard Windows Objects. The operation of the package is made easy by the menu driven functions and control of sequence of operations such as end of day, end of month & end of year are made compulsory by the system itself.</p>
				</div>
				<div class="col-md-4">		
				   <p>Thus the people in the accounting profession who need not be computer experts could use the system. DMS applications are supported on multiple hardware/operating system environments, which include the latest version of windoes. The proposed system will be implemented on a Three-tier architecture where data base, application and the clients are in different levels running on latest version of windows. The database would be SQL server.</p>
				</div>
				<div class="col-md-4">		
				   <p>This would enable customer to adapt to open standard, user friendliness, and standard report formatting and versatile report writer facility in case not satisfied with the standard reports and when necessity for user defined reports. The parameterization and the modular architecture are the inherent features of all the systems.</p>
				</div>				
			  </div>
			 </div>
	   </section>   
	   
	  <section class="tw-client">
      <div class="container">
		<div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col text-center">
               <div class="section-heading tw-mb-80">
                  <h2>
                     They Use <span>PAYADMIN</span>
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client1.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client2.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client3.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client4.png');?>" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>   
		<?php 
			$this->load->view('inc/footer', $data);
		?>
	</body>
</html>