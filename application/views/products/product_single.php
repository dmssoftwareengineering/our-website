<!doctype html>
<html lang="en">
	<head>	
		<?php 
			$data["title"] = $product_name." | DMS Software Engineering";
			$data["description"] = "";
			$data["keywords"] = "";
			$this->load->view('inc/head', $data);		
	    ?>
		<style>
		.pc-feature>div:nth-child(2) div,.pcf-title{margin-left:15px}.pc-feature,.pcf-desc{clear:left}.pc-slider{height:350px;position:relative;width:950px}.pc-features,.pc-monitor,.pc-pointer,.pc-pointer div{position:absolute}.pc-monitor{background:url(<?php echo base_url('assets/core/images/products/monitor.png');?>) no-repeat;background-size:100%;height:334px;padding:6px 0 0 6px;width:446px}.pcm-img{height:238px;width:435px}.pc-pointer{height:20px;left:441px;top:13px;width:51px;z-index:1}.pc-pointer div:nth-child(1){background:#2f2c2c;height:2px;top:9px;width:64px;z-index:1}.pc-pointer div:nth-child(2){background:#2f2c2c;height:20px;left:52px;width:20px}.pc-features{left:490px;margin-top:10px;width:460px}.pc-feature>div:nth-child(1) div{float:left}.pcf-box{background:#fff;border:2px solid #0070c0;cursor:pointer;height:26px;width:26px}.pcf-title{color:#2f2c2c;cursor:pointer;font-weight:700;font-size:18px;padding-bottom:10px}.pcf-title:hover{color:#0070c0}.pcf-desc{display:none;line-height:18px;margin:0 0 20px 40px}.pcf-desc-active{display:block}.top-header .pc-monitor{position:relative;z-index:0}
		.prod-cat {
			margin: 0 0 10px;
			font-weight: bold;
			width: 170px;
			color: #000;
			z-index:1
		}
		
		.prod-status .col-md-3 {
	border-right: #cececd solid 1px;
	margin-bottom: 20px;
}
		
		.prod-status .download {
	padding: 4px 20px;
	border-radius: 5px;
	margin-top: 12px;
	color: #fff;
}
.prod-mains {
	font-weight: bold;
}
.top-header * {
	color: #2f2c2c;
}

@media (min-width: 992px) {	
	.col-lg-5 {
		max-width: 100%
	}
}

@media (max-width: 992px) {	
	.pc-features {
		left: 0 !important;
		top: 350px;
	}
	.pc-pointer{
		display:none
	}
	
	.tw-client {
		padding-top: 200px;
	}
}
		</style>
	</head>
	<body>	
		<?php $this->load->view('inc/header');?>		
		<section class="tw-final-result top-header">
			<div class="container">
				<div class="row wow fadeInLeft">	
					<div class="col-md-1"></div>
					<div class="col-md-12 col-lg-5 col-sm-12">
						<div class="pc-monitor wow fadeInLeft">
							<img class="pcm-img" src="<?php echo base_url('assets/core/images/news/post2.jpg'); ?>" alt="Screen" />
						</div>
					</div>
					<div class="col-md-12 col-lg-5 col-sm-12">
					   <div class="section-heading">
						  <h2><?php echo $product_name; ?></h2>
					   </div>		
					   <a href="#" class="prod-cat"><?php echo $prod_type; ?></a>
					   <p>DMS - IMAGO is a Cheque Imaging and Truncation Solution which promises to bring multiple benefits to customers by substantially reducing the time taken to clear the cheques as well as to the banks by enabling them to offer better customer services and increasing operational efficiency by cutting down on overheads in physical clearing.</p>
					   
					   <div class="row wow fadeInLeft prod-status">			
							<div class="col-md-3">
								<div class="prod-mains">Runs On</div>
								<div style="display:inline"><i class="fa fa-windows" title="MS Windows"></i></div>
							</div>
							<div class="col-md-3">
								<div class="prod-mains">Version</div>
								<div>10.0.0 <!--<span>Beta</span>--></div>
							</div>
							<div class="col-md-5">
								<a href="#"><div class="btn btn-primary download">
									Request a Demo				
								</div></a>	
							</div>
					   </div>
					</div>
				</div>
			</div>
		</section>    
		
		<section class="tw-final-result">
		  <div class="container">
			 <div class="row wow fadeInLeft">				
				<div class="col-md-12">
				   <div class="section-heading">
					  <h2>
						 Product Features
						 <span class="animate-border tw-mt-20 tw-mb-40"></span>
					  </h2>
				   </div>
					<div class="tw-results-content">
						<div id="slider1" class="pc-slider wow fadeInUp">
							<div class="pc-monitor wow fadeInLeft">
								<img class="pcm-img" src="<?php echo base_url('assets/core/images/news/post1.jpg'); ?>" alt="Screen" />
							</div>
							<div class="pc-pointer wow fadeInRight">
								<div></div>
								<div></div>
							</div>		
							<div class="pc-features">			
								<div class="pc-feature" data-index="0" data-img="<?php echo base_url('assets/core/images/news/post1.jpg'); ?>">
									<div>
										<div class="pcf-box"></div>
										<div class="pcf-title">Inward Clearing Module</div>
									</div>
									<div class="pcf-desc pcf-desc-active">
										Download Inward details from Clearing House  (Online /Offline)
										Delegate Inward Batches
										Process Inward Batches
										Authorized Inward Batches
									</div>
								</div>			
								<div class="pc-feature" data-index="1" data-img="<?php echo base_url('assets/core/images/news/post2.jpg'); ?>">
									<div>
										<div class="pcf-box"></div>
										<div class="pcf-title">Outward Clearing Module</div>
									</div>
									<div class="pcf-desc">Powerful inhouse built encryption, encrypt diary entries to ensure total privacy.</div>
								</div>			
								<div class="pc-feature" data-index="2" data-img="<?php echo base_url('assets/core/images/news/post3.jpg'); ?>">
									<div>
										<div class="pcf-box"></div>
										<div class="pcf-title">Postdated Cheques Module</div>
									</div>
									<div class="pcf-desc">Record diary entries in audio format using your microphone.</div>
								</div>	
								<div class="pc-feature" data-index="3" data-img="<?php echo base_url('assets/core/images/news/post2.jpg'); ?>">
									<div>
										<div class="pcf-box"></div>
										<div class="pcf-title">Other Features</div>
									</div>
									<div class="pcf-desc">Old fashion way of writing diary entries with modern WYSIWYG features.</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			 </div>
		  </div>
	   </section>     

	  <section class="tw-client">
      <div class="container">
		<div class="row wow fadeInUp">
            <div class="col text-center">
               <div class="section-heading tw-mb-80">
                  <h2>
                     They Use <span><?php echo $product_name; ?></span>
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row wow fadeInUp">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client1.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client2.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client3.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client4.png');?>" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>   
		<?php 
			$this->load->view('inc/footer', $data);
		?>
		<script>			
			function pcSlider(t){$(t+" .pcf-title,"+t+" .pcf-box").click(function(){$(t+" .pcf-desc").removeClass("pcf-desc-active");var c=$(this).parent().parent(),a=c.attr("data-index"),e=13;a>=0&&(e=38*parseInt(a)+13),$(c[0].children[1]).addClass("pcf-desc-active"),$(t+" .pcm-img").attr("src",c.attr("data-img")),$(t+" .pc-pointer").attr("style","top:"+e+"px")})}$(function(i){"use strict";pcSlider("#slider1")});
		</script>
	</body>
</html>