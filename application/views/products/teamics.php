<!doctype html>
<html lang="en">
	<head>	
		<?php 
			$data["title"] = "DMS – TEAMICS | DMS Software Engineering";
			$data["description"] = "";
			$data["keywords"] = "";
			$this->load->view('inc/head', $data);		
	    ?>
		<style>.pc-feature>div:nth-child(2) div,.pcf-title{margin-left:15px}.pc-feature,.pcf-desc{clear:left}.pc-slider{height:350px;position:relative;width:950px}.pc-features,.pc-monitor,.pc-pointer,.pc-pointer div{position:absolute}.pc-monitor{background:url("<?php echo base_url('assets/core/images/products/monitor.png');?>") no-repeat;background-size:100%;height:334px;padding:6px 0 0 6px;width:446px}.pcm-img{height:238px;width:435px}.pc-pointer{height:20px;left:441px;top:13px;width:51px;z-index:1}.pc-pointer div:nth-child(1){background:#2f2c2c;height:2px;top:9px;width:64px;z-index:1}.pc-pointer div:nth-child(2){background:#2f2c2c;height:20px;left:52px;width:20px}.pc-features{left:490px;margin-top:10px;width:460px}.pc-feature>div:nth-child(1) div{float:left}.pcf-box{background:#fff;border:2px solid #0070c0;cursor:pointer;height:26px;width:26px}.pcf-title{color:#2f2c2c;cursor:pointer;font-weight:700;font-size:18px;padding-bottom:10px}.pcf-title:hover{color:#0070c0}.pcf-desc{display:none;line-height:18px;margin:0 0 20px 40px}.pcf-desc-active{display:block}.top-header .pc-monitor{position:relative;top:20px;z-index:0}.prod-cat{margin:0 0 10px;font-weight:700;width:170px;color:#000;z-index:1}.prod-status{margin:40px 0 20px 84px}.prod-status .download,.col-sm-12 .download{padding:8px 20px;border-radius:5px;margin-top:2px;color:#fff}.download i{padding-right:10px;color:#fff}.request-demo{border-color:red;background:red}.download-brochure{background:#199c2f;border-color:#0070c0}.download:hover{background:#2f2c2c}.prod-mains{font-weight:700}.top-header *{color:#2f2c2c}@media (min-width: 992px){.col-lg-5{max-width:100%}}@media (max-width: 992px){.pc-features{left:0!important;top:350px}.pc-pointer{display:none}.tw-client{padding-top:200px}}.top-header{padding:70px 0 20px}.advantage-img{margin-bottom:20px;height:60px}#benefits{padding:60px 0;background:#199c2f;color:#fff;font-weight:700}.post-media.post-video::before{height:74%}.video-icon{top:40%}.tw-final-result ul{margin-left:-40px;counter-reset:li}.tw-final-result li{list-style-type:none;margin-bottom:10px}.tw-final-result li::before{content:"→";font-weight:700;color:#0070c0;padding-right:10px;font-size:30px;font-weight:bold}.feature-image{margin-top:141px}.tw-web-analytics-content{margin-bottom:0}#features{padding-bottom:20px}.top-header{background:url('<?php echo base_url('assets/images/products/teamics-bg.jpg'); ?>')}.section-heading h2 span{color:#199c2f}</style>
	</head>
	<body>
		<?php $this->load->view('inc/header');?>		
		<section class="tw-final-result top-header">
			<div class="container">
				<div class="row" data-aos="fade-left" data-aos-once="false">	
					<div class="col-md-1"></div>
					<div class="col-md-12 col-lg-5 col-sm-12">
						<div class="row">
							<div class="pc-monitor post-media post-video" data-aos="fade-left" data-aos-once="false">
								<img class="pcm-img" src="<?php echo base_url('assets/core/images/news/post3.jpg'); ?>" alt="Screen" />
								<a class="video-popup" href="#">
									<div class="video-icon">
									   <i class="icon icon-play"></i>
									</div>
								 </a>
							</div>
						</div>
						<div class="row prod-status">
							<a href="#">
								<div class="btn btn-primary download request-demo"><i class="fa fa-hand-o-right"></i>Request a Demo Today</div>
							</a>	
						</div>
					</div>
					<div class="col-md-12 col-lg-5 col-sm-12">
					   <div class="section-heading">
						  <h2><span>DMS – TEAMICS</span></h2>
					   </div>
					   <a href="#" class="prod-cat">Manufacturing</a>
					   <p>A solution that manages the entire operation of a Tea Exporting business from the purchasing to shipment. It handles the catalogs files, purchases from tea auctions, collection of tea purchases, manage belending operations, maintaining tea stock, manage contracts and shipments with various reports.</p>
					   <p>
					   This is the Best Selling and the Most Trusted Tea Exporters Software Solution in Sri Lanka, our more than 40 customers are the true witnesses for our Tea Solution effectiveness. 09 Out of Top 10 Tea Exporters in Sri Lanka use DMS - TEAMICS as their Tea Solution.
					   </p>
					   <a href="#"><div class="btn btn-primary download download-brochure"><i class="fa fa-download"></i>Brochure</div></a>
					</div>
				</div>
			</div>
		</section>    
		
		<section id="benefits" class="tw-final-result">
		  <div class="container">
			 <div class="row text-center" data-aos="fade-right" data-aos-once="false">			
				<div class="col-md-2 align-self-md-center">
					<img src="<?php echo base_url('assets/core/images/icon/task-list.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Pre Auction
				</div>
				<div class="col-md-2">
					<img src="<?php echo base_url('assets/core/images/icon/judgement.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Post Auction
				</div>
				<div class="col-md-2">
					<img src="<?php echo base_url('assets/core/images/icon/truck.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Collections
				</div>
				<div class="col-md-2">
					<img src="<?php echo base_url('assets/core/images/icon/tea-stock.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Tea Stock & Blending
				</div>
				<div class="col-md-2">
					<img src="<?php echo base_url('assets/core/images/icon/tea-blend.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Blending
				</div>
				<div class="col-md-2">
					<img src="<?php echo base_url('assets/core/images/icon/ship.png');?>" alt="" class="img-fluid advantage-img"><br/>
					Contract & Shipping
				</div>
			 </div>
		  </div>
		</section>
		
		<section id="features" class="tw-final-result">
		  <div class="container">
			<div class="row" data-aos="fade-down" data-aos-once="false">			
				<div class="col-md-12 ml-auto align-self-center">
				   <div class="tw-web-analytics-content">
					  <i class="icon icon-target"></i>
					  <h2>Some Benefits you receieve with <span>TEAMICS</span></h2>
					  <small>Get to know all the great benefits. Why wait? <a href="#"><strong>Request a Demo Today</strong></a></small>
					  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
						<div class="row">
						  <div class="col">
							  <p><strong>Pre Auction</strong></p>
							  <p>Purchases at the weekly tea auction in Colombo and private sales are covered in this option. The catalogue upload facility is available with the system.</p>
						  </div>
						  <div class="col">
							  <p><strong>Post Auction</strong></p>
							  <p>The Post Auction module covers the tea purchasing operations of a Tea ex-porting company. Purchases at the weekly tea auction in Colombo and private sales are covered. This module also produces information on Tea purchases required by the Tea Commissioner.</p>
						  </div>
						  <div class="col">
							  <p><strong>Collection</strong></p>
							  <p>This module enables monitoring of collection of tea purchased at the auction. This helps rationalize collection of tea so as to reduce stock holding cost and to optimize usage of storage space. Vital management information such as lines allocated to blends but not collected is available from this module.</p>
						  </div>
						</div>	
						<div class="row">
						  <div class="col">
							  <p><strong>Tea Stock & Blending</strong></p>
							  <p>This module helps monitor blending operations and tea stocks. The functions involved are creating blends, allocating to contracts allocating tea to blends, monitoring the status of a blend (in progress, completed and physically blended) and monitoring tea stocks</p>
						  </div>
						  <div class="col">
							  <p><strong>Post Auction</strong></p>
							  <p>The Post Auction module covers the tea purchasing operations of a Tea ex-porting company. Purchases at the weekly tea auction in Colombo and private sales are covered. This module also produces information on Tea purchases required by the Tea Commissioner.</p>
						  </div>
						  <div class="col">
							  <p><strong>Contract & Shipping</strong></p>
							  <p>This module keeps track of contracts with the customers and shipments made against them. The module is thoroughly integrated with the others, for instance the Blend & Tea Stock module which has information of all blends allocated to a contract.</p>
						  </div>
						</div>	
				   </div>
				</div>
			 </div>
		  </div>
	   </section>     
	   
	  <section class="tw-client">
      <div class="container">
		<div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col text-center">
               <div class="section-heading tw-mb-80">
                  <h2>
                     They Use <span>TEAMICS</span>
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client1.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client2.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client3.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/core/images/clients/client4.png');?>" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>   
		<?php 
			$this->load->view('inc/footer', $data);
		?>
	</body>
</html>