<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Data Capture and Digitizing Services";
		$data["description"] = $seo['sub_page']['desc'];
		$data["keywords"] = $seo['sub_page']['keywords'];
		$this->load->view('inc/head', $data);		
	?>
	<style>
		#main-container ul{margin-left:-40px;counter-reset:li}#main-container li{list-style-type:none;counter-increment:li;margin-bottom:10px}#main-container li::before{content:counter(li);font-weight:700;color:#0070c0;padding-right:10px;font-size:30px}.btn{padding:10px 40px;border-radius:10px;cursor:pointer}#message{max-height:250px;min-height:50px}.error-msg{font-size:12px;color:#8b0000;font-weight:700;display:none}.show-span{display:block}@media (max-width: 766px){.sub-images{display:none}}
	</style>	
</head>

<body>	
	<?php $this->load->view('inc/header');?>

   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row" data-aos="fade-down" data-aos-once="false">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>Our Services</small>
                    Data Capturing and <br/>Digitizing Services
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 mr-auto ml-auto"></span>
               </div>
            </div>
         </div>
         <div class="row" data-aos="fade-left" data-aos-once="false">
            <div class="col-md-6 align-self-md-center">
               <img src="<?php echo base_url('assets/core/images/services/single_service_img1.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 ml-auto align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-question-circle"></i>
                  <p>
                     We adopt statistically proven data editing and validation methods to achieve a high degree of accuracy in <strong>Data Capture</strong> & <strong>Image Capture</strong>. For Data Capture we use the double entry method for verification whereby one person enters and another person verifies. During verification if a mismatch is found by our software, the verifying person will have to compare the source document and the previous entry & if required correct the error. Thus 100% accuracy in data Capture is achieved.
				  </p>
				  <p>
                     In addition, we are supported by a Team of Experienced Software development staff who assist in the Transfer / Conversion of Data from one Data Format to another Data Format E.g. ASCII to EBCDIC, EBCDIC to ASCII or to ACCESS, XML or any other data format or From One Media Type to another Media Type.
                  </p>
               </div>
            </div>
         </div>
         <div class="row" data-aos="fade-right" data-aos-once="false">
            <div class="col-md-5 align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-coins-1"></i>
                  <h3>We offer you the best</h3>
                  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
                  <p>
					 Our Data Centre operates on 24 x 7 basis. Our personnel are very experienced in Data Entry, Verification and Editing. The operational staff are all trained to co-ordinate the various aspects of Data Entry/Image Capture services.
				  </p>	
				  <p>
					 In the case of Business Process Outsourcing (BPO) our personnel are thoroughly trained on the applications by the System Developers prior to the commencement of the operations. We have a large team of motivated and well trained personnel enabling us to cater to varying business requirements. Our operation managers and supervises are experienced in human resource management, production planning & control, quality assurance and batch processing etc.
				  </p>	
				  <p>
					 We ensure that all the data supplied to us is kept strictly confidentila and is only seen by the persons entering the data.
				  </p>	
               </div>
            </div>
            <div class="col-md-6 ml-auto align-self-md-center sub-images">
               <img src="<?php echo base_url('assets/core/images/services/single_service_img2.png');?>" alt="" class="img-fluid analytics-img">
            </div>
         </div>
      </div>
   </section>
   
   <div id='recaptcha' class="g-recaptcha" data-sitekey="6LeVAGwUAAAAAMB4sy-SmzMCf3qJWRSIQib5JJ7y" data-callback="onloadCallback" data-size="invisible"></div>
   
	<?php 
		$this->load->view('services/inc/service_request');
		$this->load->view('inc/footer', $data);
		$this->load->view('inc/captcha', array(
			"csrf_token" => $csrf_token,
			"type" => "Security"
		));
	?>  
</body>
</html>