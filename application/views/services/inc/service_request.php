<section id="tw-service-value" class="tw-service-value bg-offwhite wow fadeInUp">
      <div class="container">
         <div class="row wow fadeInDown">
            <div class="col-lg-5 col-md-3">
               <div class="tw-value-desc">
                  <h2 class="column-title">LET'S MAKE SOMETHING GREAT TOGETHER</h2>
                  <span class="animate-border border-offwhite tw-mt-20 tw-mb-35"></span>
                  <p>
                     Send us your requirements. Our professionals will get in touch with you to deliver the right solution.
                  </p>                  
               </div>
            </div>
            <div class="col-lg-7 col-md-9">
               <form id="req-form" class="row">
                  <div class="col-lg-6">
                     <div class="form-group">
                        <input id="com_name" class="form-control form-name" name="name" required="" placeholder="Company Name" type="text">
						<span class="error-msg">Please enter a valid company name</span>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group">
                        <input id="person_name" class="form-control form-phone" name="phone" required="" placeholder="Contact Person" type="tel">
						<span class="error-msg">Please enter valid name for contact person</span>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group">
                        <input id="email" class="form-control form-email" name="email" required="" placeholder="Email Address" type="email">
						<span class="error-msg">Please enter valid email address</span>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group">
                        <input id="contact_number" class="form-control form-subject" name="subject" required="" placeholder="Phone Number" type="tel">
						<span class="error-msg">Please enter valid phone number. ex: +94112111222</span>
                     </div>
                  </div>
                  <div class="col-lg-12">
                     <div class="form-group">
                        <textarea id="message" class="form-control form-message required-field" rows="5" required="" placeholder="Your Requirement?"></textarea>
						<span class="error-msg">Please enter valid requirement</span>
                     </div>
				   </div>
               </form>
			   <div id="btn-reqInfo" class="btn btn-primary">Request Information</div>
            </div>
         </div>
      </div>
   </section>