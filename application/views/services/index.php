<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "What We Do | DMS Software Engineering";
		$data["description"] = $seo['desc'];
		$data["keywords"] = $seo['keywords'];
		$this->load->view('inc/head', $data);		
	?>
</head>

<body>
	<?php $this->load->view('inc/header');?>

   <div id="banner-area" class="banner-area" style="background-image:url(<?php echo base_url('assets/core/images/news/blog_bg.jpg');?>)">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading">
				  <span>What We Do</span>
                  <h1 class="banner-title">
				  The Best Possible Service
				  </h1>
               </div>
            </div>
         </div>
      </div>
   </div>

   <section id="financial" class="main-container">
      <div class="container">
		  <div class="row">
            <div class="col">
               <div class="service-list-carousel owl-carousel">
                  <div class="tw-service-box-list text-center tw-service-box-bg">
                     <div class="service-list-bg service-list-bg-1 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="<?php echo base_url('assets/core/images/icon/service1.png');?>" alt="" class="img-fluid">
                        </div>
                     </div>
                     <h3><a href="<?php echo base_url('what-we-do/payroll-and-bpo-services'); ?>">Payroll and Business Process Outsourcing</a></h3>
                     <p>Our Payroll BPO Service is highly adaptable managed payroll outsourcing solution, no matter how small or large your organization is. We aim to provide a highly efficient and comprehensive managed payroll solution.</p>
                  </div>
                  
                  <div class="tw-service-box-list text-center">
                     <div class="service-list-bg service-list-bg-1 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="<?php echo base_url('assets/core/images/icon/service1.png');?>" alt="" class="img-fluid">
                        </div>
                     </div>
                     <h3><a href="<?php echo base_url('what-we-do/data-capturing-and-digitizing'); ?>">Data Capturing & Digitization</a></h3>
                     <p>The procedure of using software and scanners to transfer an image on paper to a digital image. Hard copy paper documents are fed through scanning devices that incorporate specialized document scanning to create a digital Portable Document Format (PDF).</p>
                  </div>
				  
				  <div class="tw-service-box-list text-center tw-service-box-bg">
                     <div class="service-list-bg service-list-bg-1 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="<?php echo base_url('assets/core/images/icon/service1.png');?>" alt="" class="img-fluid">
                        </div>
                     </div>
                     <h3><a href="<?php echo base_url('products/crims/3'); ?>">Man Power Outsourcing & Managed Services</a></h3>
                     <p>With ever increasing business demands companies want to stay focused on their core business activity and outsource support services to save time & energy.</p>
                  </div>
				  
				  <div class="tw-service-box-list text-center">
                     <div class="service-list-bg service-list-bg-1 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="<?php echo base_url('assets/core/images/icon/service1.png');?>" alt="" class="img-fluid">
                        </div>
                     </div>
                     <h3><a href="<?php echo base_url('what-we-do/tailored-application-software-development'); ?>">Tailored Software Development</a></h3>
                     <p>Customized Software Development services provide the IT backbone for our clients business. Our expert technical team understands your application requirements thoroughly, prepares architecture, develops, tests and finally implements it.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>

   <?php 
		$data['show_quote'] = '';
		$this->load->view('inc/footer', $data);
	?>
</body>

</html>