<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Managed Services | DMS Software Engineering";
		$data["description"] = $seo['sub_page']['desc'];
		$data["keywords"] = $seo['sub_page']['keywords'];
		$this->load->view('inc/head', $data);		
	?>
	<style>
		#main-container ul{margin-left:-40px;counter-reset:li}#main-container li{list-style-type:none;counter-increment:li;margin-bottom:10px}#main-container li::before{content:counter(li);font-weight:700;color:#0070c0;padding-right:10px;font-size:30px}.btn{padding:10px 40px;border-radius:10px;cursor:pointer}#message{max-height:250px;min-height:50px}.error-msg{font-size:12px;color:#8b0000;font-weight:700;display:none}.show-span{display:block}@media (max-width: 766px){.sub-images{display:none}}		
	</style>	
</head>

<body>	
	<?php $this->load->view('inc/header');?>

   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row wow fadeInDown">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>Our Services</small>
                     Managed Services
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 mr-auto ml-auto"></span>
               </div>
            </div>
         </div>
         <div class="row wow fadeInLeft">
            <div class="col-md-4 align-self-md-center">
               <img src="<?php echo base_url('assets/images/services/managed-services.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-question-circle"></i>
                  <p>The buzzword relating to IT Support is “Managed Services”, and every day more and more businesses are jumping on the bandwagon. But what does managed services actually mean and how can you tell if an IT Support company is not just using the word as a marketing tool, but is in fact only offering “flat rate” services packaged as “managed services”? Our Managed Services allows a business to offload IT operations to a service provider, known as a Managed Services Provider. </p>
				  <p>The managed service provider assumes an ongoing responsibility for 24-hour monitoring, managing and/or problem resolution for the IT systems within a business.</p>
               </div>
            </div>
         </div>
		 
         <div class="row" data-aos="fade-left" data-aos-once="false">
            <div class="col-md-6 align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-target"></i>
                  <h3>6 Benefits of Managed Services</h3>
                  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
                  <ul>
					<li>Increased operational efficiency</li>
					<li>Reduced operating costs</li>
					<li>Cost-effective access to enterprise-level support</li>
					<li>Minimized downtime</li>
					<li>Allows the focus to be on running the business, and not the technology</li>
					<li>Peace of mind from knowing that the network is monitored 24/7/365</li>
				  </ul>
                  
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <div id='recaptcha' class="g-recaptcha" data-sitekey="6LeVAGwUAAAAAMB4sy-SmzMCf3qJWRSIQib5JJ7y" data-callback="onloadCallback" data-size="invisible"></div>
   
	<?php 
		$this->load->view('services/inc/service_request');
		$this->load->view('inc/footer', $data);
		$this->load->view('inc/captcha', array(
			"csrf_token" => $csrf_token,
			"type" => "Managed"
		));
	?>  
</body>
</html>