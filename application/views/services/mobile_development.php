<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Mobile Application Development | DMS Software Engineering";
		$data["description"] = $seo['sub_page']['desc'];
		$data["keywords"] = $seo['sub_page']['keywords'];
		$this->load->view('inc/head', $data);		
	?>
	<style>
		#main-container ul{margin-left:-40px;counter-reset:li}#main-container li{list-style-type:none;counter-increment:li;margin-bottom:10px}#main-container li::before{content:counter(li);font-weight:700;color:#0070c0;padding-right:10px;font-size:30px}.btn{padding:10px 40px;border-radius:10px;cursor:pointer}#message{max-height:250px;min-height:50px}.error-msg{font-size:12px;color:#8b0000;font-weight:700;display:none}.show-span{display:block}@media (max-width: 766px){.sub-images{display:none}}	
	</style>	
</head>

<body>	
	<?php $this->load->view('inc/header');?>

   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row wow fadeInDown">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>Our Services</small>
                     Mobile Application <br/>Development Services
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 mr-auto ml-auto"></span>
               </div>
            </div>
         </div>
         <div class="row wow fadeInLeft">
            <div class="col-md-6 align-self-md-center">
               <img src="<?php echo base_url('assets/images/services/mobile_app.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 ml-auto align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-question-circle"></i>
                  <p>
                     Our newest service offering is cross-platform mobile development using Xamarin. This allows us to do mobile app development across all of the major mobile platforms such as, iOS (iPhone, iPad), Android and Windows. 
				  </p>
				  <p>
					We can develop a high-quality cross-platform solution using Xamarin.  
				  </p>
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <div id='recaptcha' class="g-recaptcha" data-sitekey="6LeVAGwUAAAAAMB4sy-SmzMCf3qJWRSIQib5JJ7y" data-callback="onloadCallback" data-size="invisible"></div>
     
	<?php 
		$this->load->view('services/inc/service_request');
		$this->load->view('inc/footer', $data);
		$this->load->view('inc/captcha', array(
			"csrf_token" => $csrf_token,
			"type" => "Mobile"
		));
	?>  
</body>
</html>