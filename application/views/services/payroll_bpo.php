<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Payroll and BPO Services | DMS Software Engineering";
		$data["description"] = $seo['sub_page']['desc'];
		$data["keywords"] = $seo['sub_page']['keywords'];
		$this->load->view('inc/head', $data);		
	?>
	<style>
		#main-container ul{margin-left:-40px;counter-reset:li}#main-container li{list-style-type:none;counter-increment:li;margin-bottom:10px}#main-container li::before{content:counter(li);font-weight:700;color:#0070c0;padding-right:10px;font-size:30px}.btn{padding:10px 40px;border-radius:10px;cursor:pointer}#message{max-height:250px;min-height:50px}.error-msg{font-size:12px;color:#8b0000;font-weight:700;display:none}.show-span{display:block}@media (max-width: 766px){.sub-images{display:none}}.payroll-counter,.epayslip{margin-bottom:100px;margin-top:-40px}
	</style>	
</head>

<body>	
	<?php $this->load->view('inc/header');?>

   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row wow fadeInDown">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>Our Services</small>
                     Payroll and BPO<br/>Services
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 mr-auto ml-auto"></span>
               </div>
            </div>
         </div>
         <div class="row" data-aos="fade-left" data-aos-once="false">
            <div class="col-md-6 align-self-md-center">
               <img src="<?php echo base_url('assets/core/images/services/single_service_img1.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 ml-auto align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-question-circle"></i>
                  <p>
                     Outsourcing meticulous business functions can help you to free up resources and energy to focus on your core business activities.
				  </p>
				  <p>
					Outsourcing payroll services are more and more important for start-up businesses. Whilst you look out the whole thing from product development to advertising and marketing, payroll is one function that can be effortlessly outsourced to a payroll service company. Payroll companies assist you to organize your payroll, adhere to policies and procedures, and get employees paid on time.
                  </p>
               </div>
            </div>
         </div>
		 
		 <div class="row payroll-counter" data-aos="fade-left" data-aos-once="false">
            <div class="col-md-4 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn" data-wow-duration="1s">
                  </div>
                  <div class="facts-content wow fadeInUp" data-wow-duration="1s">
                     <h4 class="facts-title">Printing & Distributing of</h4>
                     <span class="counter">200,000</span>
					 <h4 class="facts-title">Payslips per month</h4>
                  </div>
               </div>
            </div>
            <div class="col-md-4 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                  </div>
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Processing of</h4>
                     <span class="counter">60</span>
                     <sup>+</sup>
                     <h4 class="facts-title">Company Payroll on BPO</h4>
                  </div>
               </div>
            </div>
            <div class="col-md-4 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                  </div>
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Counting</h4>
                     <span class="counter">20</span>
                     <sup>+</sup>
                     <h4 class="facts-title">Years of Experience</h4>
                  </div>
               </div>
            </div>
            <div class="col-md-3 text-center" hidden>
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                     <img src="<?php echo base_url('assets/core/images/icon/fact4.png');?>" alt="" class="img-fluid">
                  </div>
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Awards</h4>
                     <span class="counter">50</span>
                     <sup>+</sup>
                  </div>
               </div>
            </div>
         </div>
		 
		 <div class="row epayslip" data-aos="fade-right" data-aos-once="false">
            <div class="col-md-12 align-self-center col text-center">
               <div>
                  <p>We have been the pioneer in providing Payroll Outsourcing Services for more than 25+ years.</p>
				  <a href="https://www.epayslip.lk" target="_blank"><img src="<?php echo base_url('assets/images/services/epayslip.png');?>" alt="" class="img-fluid"></a>
				<p>
					Introduced a Paperless Payslips<br/>
					<a href="https://www.epayslip.lk" target="_blank">www.epayslip.lk</a>
				</p>
               </div>
            </div>
         </div>
		 
         <div class="row" data-aos="fade-right" data-aos-once="false">
            <div class="col-md-5 align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-coins-1"></i>
                  <h3>Why you should outsource payroll processing? </h3>
                  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
                  <p>
                     <strong>To focus on your core business activities:</strong> Payroll processing eats up a total 8 to 10 hours of employee time at every pay period. By outsourcing payroll processing, saves valuable employee time which can be reserved for core business problems to improve productivity.
				  </p>
				  <p>
					<strong>To save payroll software cost:</strong> Eradicates the need of acquiring, installing and maintaining the payroll management software, thus it reduces the IT labor needed, resulting in several immediate cost reductions. 
				  </p>
				  <p>
					<strong>Easy scalability at minimal cost:</strong> When you outsource your payroll service to Us (<a href="<?php echo base_url('about-us'); ?>" target="_blank">DMS Software Engineering</a>), you need not to worry about adding new staff to deal with the business growth. Likewise, payroll services ensure of reassigning tasks, layoffs and unemployment insurance for contracting businesses, therefore freeing you of these responsibilities at affordable cost.
                  </p>
               </div>
            </div>
            <div class="col-md-6 ml-auto align-self-md-center sub-images">
               <img src="<?php echo base_url('assets/core/images/services/single_service_img2.png');?>" alt="" class="img-fluid analytics-img">
            </div>
         </div>
         <div class="row" data-aos="fade-left" data-aos-once="false">
            <div class="col-md-6 align-self-md-center sub-images">
               <img src="<?php echo base_url('assets/core/images/services/single_service_img3.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 ml-auto align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-target"></i>
                  <h3>With Us, You receive these benefits</h3>
                  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
                  <ul>
					<li>Do not require computer to operate the package.</li>                  
					<li>Not necessary to recruit staff to manage the payroll.</li>                  
					<li>The purchase of computer stationary, magnetic media is not required.</li>                  
					<li>The processing would be handled by DMS based on the information provide by you and reports would be available within a day.</li>                  
					<li>The generation of EPF/ETF returns, Paye deductions and T9/T10 returns, net salary diskette preparation will be done by DMS.</li>                  
					<li>If selected, payroll management service would include collection of cash from bank, packet and disburse cash at an agreed rate, hence would not require your staff to get involved on cash disbursement.</li>
				  </ul>
                  
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <div id='recaptcha' class="g-recaptcha" data-sitekey="6LeVAGwUAAAAAMB4sy-SmzMCf3qJWRSIQib5JJ7y" data-callback="onloadCallback" data-size="invisible"></div>
   
	<?php 
		$this->load->view('services/inc/service_request');
		$this->load->view('inc/footer', $data);
		$this->load->view('inc/captcha', array(
			"csrf_token" => $csrf_token,
			"type" => "Payroll"
		));
	?>   
</body>
</html>