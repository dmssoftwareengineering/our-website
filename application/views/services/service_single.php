<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Service | DMS Software Engineering";
		$data["description"] = "";
		$data["keywords"] = "";
		$this->load->view('inc/head', $data);		
	?>
	<style>#main-container ul{margin-left:-40px;counter-reset:li}#main-container li {list-style-type:none;counter-increment:li;margin-bottom:10px}#main-container li::before{content:counter(li);font-weight:bold;color:#0070c0;padding-right:10px;font-size:30px}</style>
</head>

<body>	
	<?php $this->load->view('inc/header');?>

   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>Our Services</small>
                     Payroll and BPO<br/>Services
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 mr-auto ml-auto"></span>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-6 align-self-md-center">
               <img src="<?php echo base_url('assets/core/images/services/single_service_img1.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 ml-auto align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-question-circle"></i>
                  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
                  <p>
                     Outsourcing meticulous business functions can help you to free up resources and energy to focus on your core business activities.
				  </p>
				  <p>
					Outsourcing payroll services are more and more important for start-up businesses. Whilst you look out the whole thing from product development to advertising and marketing, payroll is one function that can be effortlessly outsourced to a payroll service company. Payroll companies assist you to organize your payroll, adhere to policies and procedures, and get employees paid on time.
                  </p>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-5 align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-coins-1"></i>
                  <h3>Why you should outsource payroll processing? </h3>
                  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
                  <p>
                     <strong>To focus on your core business activities:</strong> Payroll processing eats up a total 8 to 10 hours of employee time at every pay period. By outsourcing payroll processing, saves valuable employee time which can be reserved for core business problems to improve productivity.
				  </p>
				  <p>
					<strong>To save payroll software cost:</strong> Eradicates the need of acquiring, installing and maintaining the payroll management software, thus it reduces the IT labor needed, resulting in several immediate cost reductions. 
				  </p>
				  <p>
					<strong>Easy scalability at minimal cost:</strong> When you outsource your payroll service to Us <span style="color:#0070c0">(DMS Software Engineering)</span>, you need not to worry about adding new staff to deal with the business growth. Likewise, payroll services ensure of reassigning tasks, layoffs and unemployment insurance for contracting businesses, therefore freeing you of these responsibilities at affordable cost.
                  </p>
               </div>
            </div>
            <div class="col-md-6 ml-auto align-self-md-center">
               <img src="<?php echo base_url('assets/core/images/services/single_service_img2.png');?>" alt="" class="img-fluid analytics-img">
            </div>
         </div>
         <div class="row">
            <div class="col-md-6 align-self-md-center">
               <img src="<?php echo base_url('assets/core/images/services/single_service_img3.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 ml-auto align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-target"></i>
                  <h3>With Us, You receive these benefits</h3>
                  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
                  <ul>
					<li>Do not require computer to operate the package.</li>                  
					<li>Not necessary to recruit staff to manage the payroll.</li>                  
					<li>The purchase of computer stationary, magnetic media is not required.</li>                  
					<li>The processing would be handled by DMS based on the information provide by you and reports would be available within a day.</li>                  
					<li>The generation of EPF/ETF returns, Paye deductions and T9/T10 returns, net salary diskette preparation will be done by DMS.</li>                  
					<li>If selected, payroll management service would include collection of cash from bank, packet and disburse cash at an agreed rate, hence would not require your staff to get involved on cash disbursement.</li>
				  </ul>
                  
               </div>
            </div>
         </div>
      </div>
   </section>
   
	<?php 
		$this->load->view('inc/footer', $data);
	?>
	
   <script src="<?php echo base_url('assets/core/js/easy-pie-chart.js');?>"></script>
</body>

</html>