<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Tailored Software Application Development Services";
		$data["description"] = $seo['sub_page']['desc'];
		$data["keywords"] = $seo['sub_page']['keywords'];
		$this->load->view('inc/head', $data);		
	?>
	<style>
		#main-container ul{margin-left:-40px;counter-reset:li}#main-container li{list-style-type:none;counter-increment:li;margin-bottom:10px}#main-container li::before{content:counter(li);font-weight:700;color:#0070c0;padding-right:10px;font-size:30px}.btn{padding:10px 40px;border-radius:10px;cursor:pointer}#message{max-height:250px;min-height:50px}.error-msg{font-size:12px;color:#8b0000;font-weight:700;display:none}.show-span{display:block}@media (max-width: 766px){.sub-images{display:none}}		
	</style>	
</head>

<body>	
	<?php $this->load->view('inc/header');?>

   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row wow fadeInDown">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>Our Services</small>
                     Tailored Application Software <br/>Development Services
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 mr-auto ml-auto"></span>
               </div>
            </div>
         </div>
         <div class="row wow fadeInLeft">
            <div class="col-md-6 align-self-md-center">
               <img src="<?php echo base_url('assets/images/services/software_1.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 ml-auto align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-question-circle"></i>
                  <p>
                     DMS provides a wide range of Application Software Development Outsourcing services. Our strength in Enterprise Application Development, Financial Services Industry, Commercial, Manufacturing, and Travel Industry etc. We have self motivated highly qualified IT Engineers, Industrial Engineers, Management Accountants & financial consultants who can work in many industrial domain and different technology platforms:
				  </p>				  
               </div>
            </div>
         </div>
         <div class="row wow fadeInRight">
            <div class="col-md-5 align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-coins-1"></i>
                  <h3>We offer you the best</h3>
                  <span class="bottom-border tw-mt-20 tw-mb-30"></span>
                  <p>
                     <strong>Project Management</strong> <br/>
					 Entire project management and planning is based in automated tools and the DMS Project Management Database System (PMDS). This facilitates accurate project delivery, resources allocation and to take timely action in case project deviates from the agreed project plan.
				  </p>
				  <p>
					<strong>Quality Assurance Process</strong> <br/>
					The entire project phases are quality assured according to the DMS Quest Standard and all version control and configuration management is done using Concurrent Version System (CVS).
				  </p>
				  <p hidden>
					<strong>eCommerce Application Development</strong> <br/>
					We design and develop eCommerce Solutions for different transaction processess such as Business to Business (B2B), Business to Client (B2C), and Business to Employee (B2E) etc. Using object Oriented concepts and tools.
                  </p>
				  <p>
					<strong>Bespoke Application Development Methodology</strong> <br/>
					Our application development methodology is based on the Rapid Application Development (RAD) Approach. We are using many of the world's leading Computer Aided Software Engineering (CASE) tools, to analyse, design and build phases of the software development life cycle.
                  </p>				  
               </div>
            </div>
            <div class="col-md-6 ml-auto align-self-md-center sub-images">
               <img src="<?php echo base_url('assets/images/services/software_2.png');?>" alt="" class="img-fluid analytics-img">
            </div>
         </div>
		 
		 <div class="row wow fadeInRight" hidden>
            <div class="col-md-6 align-self-md-center sub-images">
               <img src="<?php echo base_url('assets/images/services/software_3.png');?>" alt="" class="img-fluid analytics-img">
            </div>
            <div class="col-md-5 align-self-center">
               <div class="tw-web-analytics-content">
				  <p>
					<strong>Application Support and Maintenance</strong> <br/>
					We provide round the clock application support services such as help desk facilities, bug fixing, version upgrades and incorpotation of new requirements etc.
                  </p>
                  <p>
                     <strong>Data Warehouse Solutions</strong> <br/>
					 We design & build Data Warehouse solutions to meet your organizations requirements using state-of-the-art leading technologies. We undertake data migration, implementation and maintenance phases in Enterprise level Data Warehouses.
				  </p>
				  <p>
					<strong>Quality Assurance Services</strong> <br/>
					We undertake QA activities such as Testing, Preparation of QA standards, Test Plans and Test Documents etc.
				  </p>
				  <p>
					<strong>Technical Documentation</strong> <br/>
					We undertake technical documentation preparation projects.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </section>
	
   <div id='recaptcha' class="g-recaptcha" data-sitekey="6LeVAGwUAAAAAMB4sy-SmzMCf3qJWRSIQib5JJ7y" data-callback="onloadCallback" data-size="invisible"></div>
   
   <?php 
		$this->load->view('services/inc/service_request');
		$this->load->view('inc/footer', $data);
		$this->load->view('inc/captcha', array(
			"type" => "Software",
			"csrf_token" => $csrf_token
		));
	?>   
</body>
</html>