<!doctype html>
<html lang="en">

<head>
   <?php 
		$data["title"] = "Creative Web Design and Development";
		$data["description"] = $seo['sub_page']['desc'];
		$data["keywords"] = $seo['sub_page']['keywords'];
		$this->load->view('inc/head', $data);		
	?>
	<style>
		#main-container ul{margin-left:-40px;counter-reset:li}#main-container li{list-style-type:none;counter-increment:li;margin-bottom:10px}#main-container li::before{content:counter(li);font-weight:700;color:#0070c0;padding-right:10px;font-size:30px}.btn{padding:10px 40px;border-radius:10px;cursor:pointer}#message{max-height:250px;min-height:50px}.error-msg{font-size:12px;color:#8b0000;font-weight:700;display:none}.show-span{display:block}@media (max-width: 766px){.sub-images{display:none}}.tw-web-analytics-content{margin-bottom:0}.home-slide{padding:30px 0 40px 130px}.home-slide:last-child{padding-bottom:100px}.web-trends{margin-top:60px}main .section-heading{margin-bottom:0}.circle{border:1px solid #9fd3ff;border-radius:180px;height:270px;width:270px;position:relative}.circle > div:nth-child(1){position:absolute;top:120px;left:73px;font-weight:700;font-size:32px;width:130px;text-align:center}.circle_desc{margin-top:20%}.things > .content{width:50%;height:110px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative}.things > .content h1{font-family:'Arial',sans-serif;text-transform:uppercase;top:0;bottom:0;left:0;right:0;margin:auto;position:absolute;height:150px;color:#89323B}.arrow,.reverse{position:relative;width:100px;transform:rotate(346deg) rotateY(180deg) translateX(-284px);z-index:1}.reverse{transform:rotate(380deg) rotateY(6deg) translateX(-211px)}.arrow .curve{border:2px solid #BE5F4B;border-color:transparent transparent transparent #BE5F4B;height:311px;width:216px;border-radius:230px 0 0 150px}.arrow .point{position:absolute;left:35px;top:270px}.arrow .point:before,.arrow .point:after{border:1px solid #BE5F4B;height:25px;content:"";position:absolute}.arrow .point:before{top:-11px;left:-11px;transform:rotate(-74deg);-webkit-transform:rotate(-74deg);-moz-transform:rotate(-74deg);-ms-transform:rotate(-74deg)}.arrow .point:after{top:-20px;left:5px;transform:rotate(12deg);-webkit-transform:rotate(12deg);-moz-transform:rotate(12deg);-ms-transform:rotate(12deg)}.sect-1{background:#dceaf5}.cir-1{border-color:#4c8fcc;background:#4c8fcc;color:#fff}.arrow .sect-aw-1{border-left-color:#4c8fcc}.arrow .sect-awt-1:before,.arrow .sect-awt-1:after{border-color:#4c8fcc}.sect-2{background:#d7e1eb}.cir-2{border-color:#376b9d;background:#376b9d;color:#fff}.arrow .sect-aw-2{border-left-color:#376b9d}.arrow .sect-awt-2:before,.arrow .sect-awt-2:after{border-color:#376b9d}.sect-3{background:#fde2db}.cir-3{border-color:#f36f4d;background:#f36f4d;color:#fff}.arrow .sect-aw-3{border-left-color:#f36f4d}.arrow .sect-awt-3:before,.arrow .sect-awt-3:after{border-color:#f36f4d}.sect-4{background:#fff7db}.cir-4{border-color:#ffd339;background:#ffd339;color:#fff}.arrow .sect-aw-4{border-left-color:#ffd339}.arrow .sect-awt-4:before,.arrow .sect-awt-4:after{border-color:#ffd339}.sect-5{background:#f4edd5}.cir-5{border-color:#c19f2d;background:#c19f2d;color:#fff}
	</style>	
</head>

<body>	
	<?php $this->load->view('inc/header');?>
	
	<section id="main-container" class="main-container">
      <div class="container">
         <div class="row wow fadeInDown">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     Web Design
                     <small>We use “Design Thinking” as our core strategy to build web sites.</small>
                  </h2>
                  <span class="animate-border tw-mt-20 tw-mb-40 mr-auto ml-auto"></span>
               </div>
            </div>
         </div>
         <div class="row wow fadeInLeft">
            <div class="col-md-4 align-self-md-center">
               <img src="<?php echo base_url('assets/images/creative-design/creative-design.png');?>" alt="" class="img-fluid">
            </div>
            <div class="col-md-7 ml-auto align-self-center">
               <div class="tw-web-analytics-content">
                  <i class="icon icon-question-circle"></i>
                  <p>
                     Our Design Thinking Studio follows a human-centric method to problem solving and innovation. It is a strategy we focus on the customer. In order to design a website and other related products and services, we keep the behavioral traits of the customer on top of our mind.
				  </p>				  
               </div>
            </div>
         </div>
         <div class="row wow fadeInRight">
            <div class="col-md-6 align-self-center web-trends">
               <div class="tw-web-analytics-content">
                  <p>
                     We work with the modern web design technologies to build fully responsive and mobile friendly websites. We offer a full range of web designing, development services including Search Engine Optimization (SEO) & Social Media Marketing (SMM).
				  </p>			  
               </div>
            </div>
            <div class="col-md-4 ml-auto align-self-md-center sub-images">
               <img src="<?php echo base_url('assets/images/creative-design/web_trends.png');?>" alt="" class="img-fluid">
            </div>
         </div>
      </div>
   </section>	
   
	<main>	 
		<section class="main-container">
		  <div class="container">
			 <div class="row wow fadeInDown">
				<div class="col text-center">
				   <div class="section-heading">
					  <h2>
						 Our Design Thinking Modes 
					  </h2>
				   </div>
				</div>
			 </div>
		  </div>
		</section>
	
		<section class="home-slide sect-1 wow fadeInDown">
			<div class="container">
				<div class="row">
					<div class="col-3">
						<div class="circle cir-1">
							<div>Empathy</div>
						</div>
						<div class="things">
						  <div class="content">
							<div class="arrow">
							  <div class="curve sect-aw-1"></div>
							  <div class="point sect-awt-1"></div>
							</div>
						  </div> 
						</div>
					</div>
					<div class="col-6">
						<div class="circle_desc">When you feel what the other person is feeling and can mirror their expression, their opinions, and their hopes.</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="home-slide sect-2 wow fadeInDown">
			<div class="container">
				<div class="row">
					<div class="col-6">
						<div class="circle_desc">Defining the problems using a unique, concise, reframing of the problem that is grounded in user needs & insights.</div>
					</div>
					<div class="col-3">
						<div class="circle cir-2">
							<div>Definition</div>
						</div>
						<div class="things">
						  <div class="content">
							<div class="arrow reverse">
							  <div class="curve sect-aw-2"></div>
							  <div class="point sect-awt-2"></div>
							</div>
						  </div> 
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="home-slide sect-3 wow fadeInDown">
			<div class="container">
				<div class="row">
					<div class="col-3">
						<div class="circle cir-3">
							<div>Ideation</div>
						</div>
						<div class="things">
						  <div class="content">
							<div class="arrow">
							  <div class="curve sect-aw-3"></div>
							  <div class="point sect-awt-3"></div>
							</div>
						  </div> 
						</div>
					</div>
					<div class="col-6">
						<div class="circle_desc">Generating many possible solutions to a problem.</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="home-slide sect-4 wow fadeInDown">
			<div class="container">
				<div class="row">
					<div class="col-6">
						<div class="circle_desc">Is creating a concrete embodiment of a concept which becomes a way to test your hypotheses get you closer to your final solution.</div>
					</div>
					<div class="col-3">
						<div class="circle cir-4">
							<div>Prototype</div>
						</div>
						<div class="things">
						  <div class="content">
							<div class="arrow reverse">
							  <div class="curve sect-aw-4"></div>
							  <div class="point sect-awt-4"></div>
							</div>
						  </div> 
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="home-slide sect-5 wow fadeInDown">
			<div class="container">
				<div class="row">
					<div class="col-3">
						<div class="circle cir-5">
							<div>Test</div>
						</div>
					</div>
					<div class="col-6">
						<div class="circle_desc">Test your concept with users using your prototypes.</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	
	<section>
      <div class="container">	  
		<div class="row wow fadeInUp">
            <div class="col text-center">
               <div class="section-heading tw-mb-80">
                  <h2>
                     Our Technologies
                  </h2>
                  <span class="animate-border tw-mt-20 ml-auto mr-auto"></span>
               </div>
            </div>
         </div>
         <div class="row" data-aos="fade-up" data-aos-once="false">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/dot-net.jpg');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/android.jpg');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/xamarin.jpg');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/java.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/bootstrap.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/sqlite.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/react-native.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/nativescript.png');?>" alt="">
                     </div>
                  </div>
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/apache.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/source-tree.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/bit-bucket.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/github.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/swift.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/node.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/python.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/mongo-db.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/oracle-db.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/web.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/sql-server.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/angular.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/mysql.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/visual-studio.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/jira.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/sap-bus.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/share-point.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/send-grid.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/oracle.png');?>" alt="">
                     </div>
                  </div>
				  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="<?php echo base_url('assets/images/techstack/office-365.png');?>" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>      
		</div>
   </section> 
   
   <div id='recaptcha' class="g-recaptcha" data-sitekey="6LeVAGwUAAAAAMB4sy-SmzMCf3qJWRSIQib5JJ7y" data-callback="onloadCallback" data-size="invisible"></div>
   
	<?php 
		$this->load->view('services/inc/service_request');
		$this->load->view('inc/footer', $data);
		$this->load->view('inc/captcha', array(
			"csrf_token" => $csrf_token,
			"type" => "Web"
		));
	?>   
</body>
</html>