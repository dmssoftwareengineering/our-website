"use strict";
$(function ($) {   
   $(document).ready(function(){
	$('.loading-con').fadeOut();
	console.log('Welcome to DMS Software Engineering');
	checkScreenSize();
   });

   /* Menu Active link*/
   $('.nav-item ').on('click', function () {
      $('.nav-item ').removeClass('active');
      $(this).addClass('active');
   });

   /* DropDown Menu */
   $(window).on("resize", function (e) {
      checkScreenSize();
   });

   function checkScreenSize() {
      var newWindowWidth = $(window).width();
      if (newWindowWidth < 991) {
         $("li.nav-item a").on("click", function () {
            $(this).parent("li").find(".dropdown-menu").slideToggle();
            $(this).find("i").toggleClass("fa-angle-down fa-angle-up");
         });
      } else {
         $("li.nav-item a").on("click", function () {
            $(this).find("i").toggleClass("fa-angle-down fa-angle-up");
            parent.window.location.href = $(this)[0].attributes['href'].value;
         });
      }
   }
   
   $('#mega-products').click(function(){
	   $('#tw-megamenu').fadeToggle();
   });

   /*Main Slideshow*/
   $(".tw-hero-slider").owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      nav: false,
      dots: false,
      autoplayTimeout: 8000,
      autoplayHoverPause: true,
      mouseDrag: false,
      smartSpeed: 1100,
      navText: ['<i class="icon icon-left-arrow2">', '<i class="icon icon-right-arrow2">'],
   });

   /*Testimonial Slider*/
   $(".tw-testimonial-carousel").owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      nav: false,
      dots: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      mouseDrag: false,
      smartSpeed: 900,
   });

   /* Testimonial Slider */
   if ($(".testimonial-slider").length > 0) {
      $(".testimonial-slider").owlCarousel({
         items: 1,
         loop: true,
         autoplay: true,
         nav: false,
         dots: true,
         autoplayTimeout: 5000,
         autoplayHoverPause: true,
         mouseDrag: true,
         smartSpeed: 900,
      });
   };

   /* Testimonial Slider */
   if ($(".testimonial-carousel-gray").length > 0) {
      $(".testimonial-carousel-gray").owlCarousel({
         items: 2,
         margin: 20,
         loop: true,
         autoplay: true,
         nav: false,
         dots: true,
         autoplayTimeout: 5000,
         autoplayHoverPause: true,
         mouseDrag: true,
         smartSpeed: 900,
      });
   };
   /* Testimonial Box Carousel */
   if ($(".testimonial-box-carousel").length > 0) {
      $(".testimonial-box-carousel").owlCarousel({
         items: 3,
         margin: 20,
         loop: true,
         autoplay: true,
         nav: false,
         dots: true,
         autoplayTimeout: 5000,
         autoplayHoverPause: true,
         mouseDrag: true,
         responsiveClass: true,
         smartSpeed: 900,
         responsive: {
            0: {
               items: 1,
            },
            600: {
               items: 2,
            },
            1000: {
               items: 3,
            }
         }
      });
   };

   /* Counter UP */
   $(".counter").counterUp({
      delay: 10,
      time: 2000
   });

   /* Client carousel */
   $(".clients-carousel").owlCarousel({
      items: 5,
      loop: true,
      nav: false,
      dots: false,
      mouseDrag: true,
      autoplay: true,
      responsiveClass: true,
      autoplayHoverPause: true,
      mouseDrag: true,
      smartSpeed: 2000,
      responsive: {
         0: {
            items: 1,
         },
         600: {
            items: 2,
         },
         1000: {
            items: 4,
         }
      }
   });

   /* Back to top */
   $(window).scroll(function () {
      if ($(this).scrollTop()) {
         $('.back-to-top button').fadeIn();
      } else {
         $('.back-to-top button').fadeOut();
      }
   });
   $(".back-to-top button").on('click', function () {
      $("html, body").animate({
         scrollTop: 0
      }, 1000);
   });

   /* Video Popup */
   if ($('.video-popup').length > 0) {
      $('.video-popup').magnificPopup({
         disableOn: 700,
         type: 'iframe',
         mainClass: 'mfp-fade',
         removalDelay: 160,
         preloader: true,
         fixedContentPos: false
      });
   };

   /* Our Mission Carousel */
   $('.mission-carousel').owlCarousel({
      items: 1,
      loop: true,
      nav: false,
      dots: true,
      autoplay: true,
      autoplayHoverPause: true,
      mouseDrag: false,
      smartSpeed: 900,
      animateOut: 'animated slideInRight',
      animateIn: 'animated slideInRight',
   });

   /* Map */
   /*if ($('#map').length > 0) {
      var contactmap = {
         lat: 6.9169846,
         lng: 79.9185959
      };

      $('#map')
        .gmap3({
            zoom: 13,
            center: contactmap,
            scrollwheel: false,
            mapTypeId: "shadeOfGrey",
            mapTypeControlOptions: {
               mapTypeIds: [google.maps.MapTypeId.ROADMAP, "shadeOfGrey"]
            }
        })
        .styledmaptype(
            "shadeOfGrey", [
               {
                  "featureType": "administrative",
                  "elementType": "geometry.stroke",
                  "stylers": [{
                     "color": "#fefefe"
                  }, {
                     "lightness": 17
                  }, {
                     "weight": 1.2
                  }]
               },
               {
                  "featureType": "administrative",
                  "elementType": "geometry.fill",
                  "stylers": [{
                     "color": "#fefefe"
                  }, {
                     "lightness": 20
                  }]
               },
               {
                  "featureType": "transit",
                  "elementType": "geometry",
                  "stylers": [{
                     "color": "#f2f2f2"
                  }, {
                     "lightness": 19
                  }]
               },
               {
                  "featureType": "all",
                  "elementType": "labels.icon",
                  "stylers": [{
                     "visibility": "off"
                  }]
               },
               {
                  "featureType": "all",
                  "elementType": "labels.text.fill",
                  "stylers": [{
                     "saturation": 36
                  }, {
                     "color": "#333333"
                  }, {
                     "lightness": 40
                  }]
               },
               {
                  "featureType": "all",
                  "elementType": "labels.text.stroke",
                  "stylers": [{
                     "visibility": "on"
                  }, {
                     "color": "#ffffff"
                  }, {
                     "lightness": 16
                  }]
               },
               {
                  "featureType": "poi",
                  "elementType": "geometry",
                  "stylers": [{
                     "color": "#f5f5f5"
                  }, {
                     "lightness": 21
                  }]
               },
               {
                  "featureType": "road.local",
                  "elementType": "geometry",
                  "stylers": [{
                     "color": "#ffffff"
                  }, {
                     "lightness": 16
                  }]
               },
               {
                  "featureType": "road.arterial",
                  "elementType": "geometry",
                  "stylers": [{
                     "color": "#ffffff"
                  }, {
                     "lightness": 18
                  }]
               },
               {
                  "featureType": "road.highway",
                  "elementType": "geometry.stroke",
                  "stylers": [{
                     "color": "#ffffff"
                  }, {
                     "lightness": 29
                  }, {
                     "weight": 0.2
                  }]
               },
               {
                  "featureType": "road.highway",
                  "elementType": "geometry.fill",
                  "stylers": [{
                     "color": "#ffffff"
                  }, {
                     "lightness": 17
                  }]
               },
               {
                  "featureType": "landscape",
                  "elementType": "geometry",
                  "stylers": [{
                     "color": "#f5f5f5"
                  }, {
                     "lightness": 20
                  }]
               },
               {
                  "featureType": "water",
                  "elementType": "geometry",
                  "stylers": [{
                     "color": "#e9e9e9"
                  }, {
                     "lightness": 17
                  }]
               }
            ], {
               name: "DMS Software Engineering"
            }
        )
        .marker({
            position: contactmap,
            icon: './assets/core/images/icon/map_marker.png'
        })
        .infowindow({
            position: contactmap,
            content: "No 380, Kalapaluwawa Rd, Rajagiriya 10107"
        })
        .then(function (infowindow) {
            var map = this.get(0);
            var marker = this.get(1);
            marker.addListener('click', function() {
               infowindow.open(map, marker);
            });
        });
   };*/

   /* Service List Box Slider */
   if ($(".service-list-carousel").length > 0) {
      $(".service-list-carousel").owlCarousel({
         items: 3,
         loop: true,
         margin: 10,
         autoplay: true,
         nav: true,
         navText: ['<i class="icon icon-arrow-left"></i>', '<i class="icon icon-arrow-right"></i>'],
         dots: false,
         autoplayTimeout: 5000,
         autoplayHoverPause: true,
         mouseDrag: true,
         responsiveClass: true,
         smartSpeed: 900,
         responsive: {
            0: {
               items: 1,
            },
            600: {
               items: 2,
            },
            1000: {
               items: 3,
               margin: 5,
            }
         }
      });
   };

   /* On Hover Timeline Active Changed */
   $(".timeline-wrapper .row").hover(function () {
      $(".timeline-item").find(".timeline-badge").removeClass("active");
      $(this).find(".timeline-badge").addClass("active");
   });
   $(".timeline-wrapper .row").hover(function () {
      $(".timeline-item").find(".timeline-date").removeClass("active");
      $(this).find(".timeline-date").addClass("active");
   });

   /* On Click search bar */
   $(".tw-search i, .tw-offcanvas-menu i").on('click', function () {
      $(".search-bar").addClass('active');
   });
   $(".search-bar i.fa-close").on('click', function () {
      $(".search-bar").removeClass('active');
   });

   /* Onclick offcanvas menu visible */
   $(".tw-menu-bar").on("click", function () {
      $(".offcanvas-wrapper").addClass('active');
      $(".offcanvas-menu-overlay").addClass('menu-show');
   });
   $(".menu-close-btn, .offcanvas-menu-overlay").on("click", function () {
      $(".offcanvas-wrapper").removeClass('active');
      $(".offcanvas-menu-overlay").removeClass('menu-show');
   });

   /* Wow Initialize */
   new WOW().init();

   /*AOS Initialize */
   AOS.init();
   
   
   /* Accordion */
   function toggleIcon(e) {
      $(e.target)
         .prev('.card-header')
         .find(".faq-indicator")
         .toggleClass('fa-minus fa-plus');
   }
   $('#accordion').on('hidden.bs.collapse', toggleIcon);
   $('#accordion').on('shown.bs.collapse', toggleIcon);

   /* Navbar fixed */
   $(window).on('scroll', function () {
      if ($(window).scrollTop() > 400) {
         $('.tw-head, .tw-header').addClass('navbar-fixed');
      } else {
         $('.tw-head, .tw-header').removeClass('navbar-fixed');
      }
      if ($(window).scrollTop() < 400) {
         setTimeout(() => {
            $('header').removeClass('off-canvas');
			$('#tw-megamenu').css('top', '152px');
         }, 0);
      } else {
         setTimeout(() => {
            $('header').addClass('off-canvas');
			$('#tw-megamenu').css('top', '118px');
         }, 0);
      }
   });
   
   /* Mega Menu Navigation - Click + Mouse Leave + Mouse Over */
   function show_mega_menu(parent){
	   $('.app-holder').hide();
	   $('.prod-sections').removeClass("selected-prod");
	   
	   var current = $(parent)[0];
	   $(current).addClass("selected-prod");
	   
	   var data_id = current.attributes['data-id'].value;
	   $('#'+data_id).show();
   }
   
   $('#mega-products').mouseover(function(){
	   $('#tw-megamenu').show();
   }).mouseleave(function(){
	   $('#tw-megamenu').hide();
   }); 

   $('#tw-megamenu').mouseover(function(){
	   $('#tw-megamenu').show();
   }).mouseleave(function(){
	   $('#tw-megamenu').hide();
   });    
   
   /*$('.prod-sections').mouseover(function(){
	   show_mega_menu(this);
   });*/
   $('.prod-sections').click(function(){
	   show_mega_menu(this);
   });
   
   $('.navbar-toggler').click(function(){
	   $('#tw-megamenu').hide();
	   $('.tw-indicator').find("i").toggleClass("fa-angle-down fa-angle-up");
   });
});

/* Send Text using AJAX Post */
function sendText_Ajax(dataFields, regexFields, serverPath, token, serviceType = "") {
	alertify.logPosition("bottom left");
	try {		
		var data = new Object();		
		var validRegex = new Object();
		var dataValues = new Object();
		
		var dataLength = dataFields.length;
		
		for (var i=0;i<dataLength;i++) {
			data[dataFields[i]] = $('#'+dataFields[i]);
			validRegex[dataFields[i]] = regexFields[i];
			
			if (validRegex[dataFields[i]] == "file_uplaod") {
				dataValues[dataFields[i]] = data[dataFields[i]];
				continue;
			}
			
			dataValues[dataFields[i]] = data[dataFields[i]].val();
		}
		
		function validateData() {
			$('.error-msg').css('display', 'none');

			function validate_file(file, div) {
				file = file[0];
				if (file.files.length == 0){
					div.css('display', 'block');
					div.html("Please Select a File");
					return false;
				}
				
				var allowed_extensions = new Array("pdf","doc","docx");
				var file_extension = file.files[0].name.split('.').pop().toLowerCase();	
				var allowed_ext_len = allowed_extensions.length;	
				var status = [false, false];
				
				status[0] = allowed_extensions.indexOf(file_extension) > -1;//Validate File Extension
				if (!status[0]) {
					div.css('display', 'block');
					div.html("Only PDF, Doc or Docx File Allowed");
					return false;
				}
				
				status[1] = file.files[0].size / 1024 / 1024; //Validate File Size
				status[1] = status[1] > 2 ? false : true; //Check if File > 2 MB
				
				if (!status[1]) {
					div.css('display', 'block');
					div.html("File Must be less than 2MB");
					return false;
				}

				return true;
			}
			
			for (var k in data) {
				var parent = $(data[k]).parent().children('.error-msg');
				
				if (validRegex[k] == "file_uplaod"){
					if (!validate_file(dataValues[k], parent)) return false;
				} else {				
					if (!validRegex[k].test(dataValues[k])) {						
						parent.css('display', 'block');
						return false;
					}					
				}
			}
			
			return true;
		}
				
		if (validateData()) {
			dataValues['token'] = token;
			dataValues['service_type'] = serviceType == "" ? "Other" : serviceType;
			
			
			$.post(serverPath, dataValues,
				function(response, status) {
					if (response.status == "true" && response.response_code == "200") {
						alertify.success(response.message);
					}
				}, "json").fail(function(response, status) {
				response = response.responseJSON;
				
				if (response.response_code == 404) {
					var responseData = response.data[0];
					
					for (var k in responseData) {
					   var field = $("#"+k).parent().children('.error-msg');
					   field.css('display', 'block');
					}
					
					alertify.error("Please Enter Real Data into the Fields");
				} else {
					alertify.error(response.message);
				}
			}, "json");
		}
	} catch (error) {
		alertify.error(error);
	}
}

function sendFile_Ajax(dataFields, regexFields, serverPath, token, serviceType = "") {
	alertify.logPosition("bottom left");
	try {		
		var data = new Object();		
		var validRegex = new Object();
		var dataValues = new Object();
		
		var dataLength = dataFields.length;
		
		for (var i=0;i<dataLength;i++) {
			data[dataFields[i]] = $('#'+dataFields[i]);
			validRegex[dataFields[i]] = regexFields[i];
			
			if (validRegex[dataFields[i]] == "file_uplaod") {
				dataValues[dataFields[i]] = data[dataFields[i]];
				continue;
			}
			
			dataValues[dataFields[i]] = data[dataFields[i]].val();
		}
		
		function validateData() {
			$('.error-msg').css('display', 'none');

			function validate_file(file, div) {
				file = file[0];
				if (file.files.length == 0){
					div.css('display', 'block');
					div.html("Please Select a File");
					return false;
				}
				
				var allowed_extensions = new Array("pdf","doc","docx");
				var file_extension = file.files[0].name.split('.').pop().toLowerCase();	
				var allowed_ext_len = allowed_extensions.length;	
				var status = [false, false];
				
				status[0] = allowed_extensions.indexOf(file_extension) > -1;//Validate File Extension
				if (!status[0]) {
					div.css('display', 'block');
					div.html("Only PDF, Doc or Docx File Allowed");
					return false;
				}
				
				status[1] = file.files[0].size / 1024 / 1024; //Validate File Size
				status[1] = status[1] > 2 ? false : true; //Check if File > 2 MB
				
				if (!status[1]) {
					div.css('display', 'block');
					div.html("File Must be less than 2MB");
					return false;
				}

				return true;
			}
			
			for (var k in data) {
				var parent = $(data[k]).parent().children('.error-msg');
				
				if (validRegex[k] == "file_uplaod"){
					if (!validate_file(dataValues[k], parent)) return false;
				} else {				
					if (!validRegex[k].test(dataValues[k])) {						
						parent.css('display', 'block');
						return false;
					}					
				}
			}
			
			return true;
		}
				
		if (validateData()) {
			dataValues['token'] = token;
			dataValues['service_type'] = serviceType == "" ? "Other" : serviceType;
			
			var formData = new FormData();   
			
			for (var i=0;i<dataLength;i++) {
				if (dataFields[i] == 'file') {
					formData.append('file', $('#file')[0].files[0]);		
					continue;
				}	
				
				formData.append(dataFields[i], dataValues[dataFields[i]]);		
			}	
			
			$.ajax({
				url: serverPath,
				method: 'post',
				processData: false,
				contentType: false,
				cache: false,
				data: formData,
				enctype: 'multipart/form-data',
				success: function(response){					
					if (response.status == true && response.response_code == "200") {
						alertify.success("We Received Your Resume, We'll Get Back to You As Soon As Possible.");
						
						for (var i=0;i<dataLength;i++) {
							$("#"+dataFields[i]).val("");
						}
					}
				},error: function(error){
					alertify.error(error);
				}
			});
		}
	} catch (error) {
		alertify.error(error);
	}
}